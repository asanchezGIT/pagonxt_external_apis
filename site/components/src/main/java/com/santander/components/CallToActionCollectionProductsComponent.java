package com.santander.components;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.hippoecm.repository.util.JcrUtils;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Strings;
import com.santander.beans.CallToActionButton;
import com.santander.beans.CallToActionCollection;
import com.santander.beans.CallToActionCollectionProducts;
import com.santander.beans.CallToActionItem;
import com.santander.beans.CallToActionItemProducts;
import com.santander.components.model.CallToActionButtonDTO;
import com.santander.components.model.CallToActionItemDTO;
import com.santander.components.model.CallToActionItemProductDTO;
import com.santander.dto.GenericDAOResource;
import com.santander.utils.Constants;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;

import static com.santander.utils.Constants.COMPONENT_CTA_BUTTON;

@ParametersInfo(type = CallToActionCollectionProductsComponentInfo.class)
public class CallToActionCollectionProductsComponent extends CommonComponent {

  private static Logger logger = LoggerFactory.getLogger(CallToActionCollectionProductsComponentInfo.class);
  private static String errores = "Error executing query: ";

  @Override
  public void doBeforeRender(HstRequest request, HstResponse response) {
    try {

     

      super.doBeforeRender(request, response);
      InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
      GenericDAOResource genericDAOResource = new GenericDAOResource();

     
      
      CallToActionCollectionProductsComponentInfo callToActionCollectionProductsComponentInfo =
          getComponentParametersInfo(request);

      String callToActionCollectionProducts =
    		  callToActionCollectionProductsComponentInfo.getCallToActionCollectionProducts();
    

      if (!Strings.isNullOrEmpty(callToActionCollectionProducts)) {
    	 

    	  CallToActionCollectionProducts object1 = (CallToActionCollectionProducts) request.getRequestContext().getObjectBeanManager().getObject(callToActionCollectionProducts);

        //Necesario para preview
        request.setModel("document", object1);
        
       

        
        request.setModel("title", object1.getTitle());
        
       
        
        List<CallToActionItemProductDTO> CallToAtionItemList= new ArrayList<CallToActionItemProductDTO>();

        for (CallToActionItemProducts item : object1.getCallToActionItemProducts()) {
        	
        	processCallToActionItemProduct(request, CallToAtionItemList, item,  internalLinkProcessor, genericDAOResource);
        	

        }
        request.setModel("CallToActionItemProducts",CallToAtionItemList);

      }

    } catch (ObjectBeanManagerException e) {
    	logger.error("",e);
    }
  }
  /*
  private void generateComponent(HstRequest request, CallToActionButton ctaButton, InternalLinkProcessor internalLinkProcessor,GenericDAOResource genericDAOResource) {

    try {
      if (ctaButton != null && ctaButton.getInternalLink() != null) {
        Map<String, String> urlAttrs = HippoUtils.getDocumentAttrsByURL(request, ctaButton.getInternalLink());
        NodeType primaryType = JcrUtils.getPrimaryNodeType(ctaButton.getInternalLink().getNode());
        String parentProductId = "";
        String version = "";
        if (primaryType.getName().contains(Constants.DOC_TYPE_API_CONTAINER_ITEM)) {
          parentProductId = getParentProductId(request, ctaButton, parentProductId);
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder().name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(ctaButton.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .tab(ctaButton.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(ctaButton.getInternalLink()),primaryType.getName(), ctaButton.getInternalLink().getCanonicalUUID()))
                          .build());

        } else if(primaryType.getName().contains(Constants.DOC_TYPE_API_ITEM)){
          parentProductId = getApiContainerId(request, ctaButton, parentProductId);
          version = InternalLinkProcessor.getVersion(ctaButton.getInternalLink().getCanonicalUUID(), request.getRequestContext());
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder().name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(ctaButton.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .version(version).channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .tab(ctaButton.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(ctaButton.getInternalLink()),primaryType.getName(), ctaButton.getInternalLink().getCanonicalUUID()))
                          .build());
        } else {
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder().name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(ctaButton.getInternalLink().getCanonicalUUID()))
                          .channel(urlAttrs.get("channel"))
                          .locale(urlAttrs.get("locale"))
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .tab(ctaButton.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(ctaButton.getInternalLink()),primaryType.getName(), ctaButton.getInternalLink().getCanonicalUUID()))
                          .build());
        }
      } else {
        if(ctaButton != null) {
          request.setModel(COMPONENT_CTA_BUTTON,
                  CallToActionButtonDTO.builder()
                          .labelButton(ctaButton.getLabelButton())
                          .colorButton(ctaButton.getColorButton())
                          .externalLink(ctaButton.getExternalLink())
                          .tab(ctaButton.getTab())
                          .build());
        }
      }
    } catch (RepositoryException | QueryException e) {
      logger.error("Error: ", e);
    }
  }
*/
  private void processCallToActionItemProduct(HstRequest request,  List<CallToActionItemProductDTO> CallToAtionItemList,
		  CallToActionItemProducts item, InternalLinkProcessor internalLinkProcessor,GenericDAOResource genericDAOResource) {
    try {
    	logger.info("--------processCallToActionItemProduct******");
    	
     

      if (item.getInternalLink() != null) {
    	logger.info("AAAA");

        Map<String, String> urlAttrs =
                HippoUtils.getDocumentAttrsByURL(request, item.getInternalLink());

        NodeType primaryType = JcrUtils.getPrimaryNodeType(item.getInternalLink().getNode());

        // NUEVO.APIS
        String parentProductId = "";
        String version = "";
        String channell = "";
        String locales = "";
        channell = urlAttrs.get("channel");
        locales = urlAttrs.get("locale");

        if (primaryType.getName().contains(Constants.DOC_TYPE_API_CONTAINER_ITEM)) { //APiContainer

          parentProductId = getParentProductIdCTAItem(request, item, parentProductId);

          CallToAtionItemList.add(
                  CallToActionItemProductDTO.builder()
                          .description((new GenericDAOResource()).rewriteHstRichContent(
                                 item.getDescription().getContent(), item.getDescription().getNode(),
                                  request.getRequestContext().getResolvedMount().getMount()))
                          .title(item.getTitle()).image(item.getImage()).name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(item.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .channel(channell)
                          .locale(locales)
                          .productList(item.getProductList())
                          .tab(item.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
                          .build());

        } else if(primaryType.getName().contains(Constants.DOC_TYPE_API_ITEM)) { //ApiItem

          parentProductId = getApiContainerIdCTAItem(request, item, parentProductId);
          version = InternalLinkProcessor.getVersion(item.getInternalLink().getCanonicalUUID(), request.getRequestContext());
          CallToAtionItemList.add( 
                  CallToActionItemProductDTO.builder()
                          .description((new GenericDAOResource()).rewriteHstRichContent(
                                 item.getDescription().getContent(), item.getDescription().getNode(),
                                 request.getRequestContext().getResolvedMount().getMount()))
                          .title(item.getTitle()).image(item.getImage()).name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(item.getInternalLink().getCanonicalUUID()))
                          .parentProductId(InternalLinkProcessor.getAliasByUuid(parentProductId))
                          .version(version)
                          .channel(channell)
                          .locale(locales)
                          .productList(item.getProductList())
                          .tab(item.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
                          .build());

        }else{
        	 logger.info("item.getInternalLink()" , item.getInternalLink());
        	 logger.info("item.getCanonicalUUID()" , item.getCanonicalUUID());
        	 logger.info("generateHstLink(item.getInternalLink())", genericDAOResource.generateHstLink(item.getInternalLink()));
        	        	
          CallToAtionItemList.add(
        		  CallToActionItemProductDTO.builder()
                          .description((new GenericDAOResource()).rewriteHstRichContent(
                               item.getDescription().getContent(), item.getDescription().getNode(),
                               request.getRequestContext().getResolvedMount().getMount()))
                          .title(item.getTitle()).image(item.getImage()).name(primaryType.getName())
                          .internalLinkId(InternalLinkProcessor.getAliasByUuid(item.getInternalLink().getCanonicalUUID()))
                          .channel(channell)
                          .locale(locales)
                          .productList(item.getProductList())
                          .tab(item.getTab())
                          .url(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
                          .build());
        }

      } else {
    	  
    	logger.info("BBBB");
        CallToAtionItemList.add(
        		CallToActionItemProductDTO.builder()
                        .description((new GenericDAOResource()).rewriteHstRichContent(
                                 item.getDescription().getContent(), item.getDescription().getNode(),
                                 request.getRequestContext().getResolvedMount().getMount()))
                        .title(item.getTitle()).image(item.getImage())
                        .externalLink(item.getExternalLink())
                        .productList(item.getProductList())
                        .tab(item.getTab())
                        .build());
      }

    } catch (RepositoryException | QueryException e) {
      logger.error("",e);
    }
  }

  private String getParentProductId(HstRequest request, CallToActionButton ctaButton, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiProductRelatedDocuments(ctaButton.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error(errores, e);
    }
    return parentProductId;
  }

  private String getApiContainerId(HstRequest request, CallToActionButton ctaButton, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiItemRelatedDocuments(ctaButton.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error(errores, e);
    }
    return parentProductId;
  }

  private String getParentProductIdCTAItem(HstRequest request, CallToActionItemProducts item, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiProductRelatedDocuments(item.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error(errores, e);
    }
    return parentProductId;
  }

  private String getApiContainerIdCTAItem(HstRequest request, CallToActionItemProducts item, String parentProductId) {
    try {
      parentProductId = HippoUtils.getApiItemRelatedDocuments(item.getInternalLink()
              .getCanonicalUUID(), request.getRequestContext());
    } catch (QueryException e) {
      logger.error(errores, e);
    }
    return parentProductId;
  }

}
