package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface CardCollectionComponentInfo {

    @Parameter(name = "cardCollection", displayName = "Card Collection")
    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:CardCollection"})
    String getCardCollection();
}

