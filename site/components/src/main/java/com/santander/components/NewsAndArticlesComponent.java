package com.santander.components;

import com.google.common.base.Strings;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = NewsAndArticlesComponentInfo.class)
public class NewsAndArticlesComponent extends CommonComponent {

	private static Logger logger = LoggerFactory.getLogger(NewsAndArticlesComponent.class);

	@Override
	public void doBeforeRender(HstRequest request, HstResponse response) {
		
		try {
			super.doBeforeRender(request, response);
			NewsAndArticlesComponentInfo newsAndArticlesComponentinfo = getComponentParametersInfo(request);
			String newsAndArticleDocument = newsAndArticlesComponentinfo.getResourceDocument();
			if (!Strings.isNullOrEmpty(newsAndArticleDocument)) {
				Object object1 = request.getRequestContext().getObjectBeanManager().getObject(newsAndArticleDocument);
				//Necesario Preview
				request.setModel("document", object1);
				
				
				
			}
		} catch (ObjectBeanManagerException obme) {
			logger.error("Error: {}", obme.getMessage());
		}
	}

}
