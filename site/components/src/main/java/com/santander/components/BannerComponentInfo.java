package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface BannerComponentInfo {

  @Parameter(name = "bannerComponentDocument", displayName = "Banner Component Document")
  @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:bannerdocument"})
  String getBanner();

}


