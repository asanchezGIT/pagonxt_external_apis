package com.santander.components;

import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.base.Strings;
import com.santander.beans.GeneralDocument;


@ParametersInfo(type = GeneralDocumentComponentInfo.class)
public class GeneralDocumentComponent extends CommonComponent {

  private static Logger logger = LoggerFactory.getLogger(GeneralDocumentComponent.class);

  @Override
  public void doBeforeRender(HstRequest request, HstResponse response) {
    try {
      super.doBeforeRender(request, response);
      GeneralDocumentComponentInfo generalDocumentComponentInfo = getComponentParametersInfo(request);
      String generalDocument = generalDocumentComponentInfo.getGeneralDocument();
      if (!Strings.isNullOrEmpty(generalDocument)) {
        GeneralDocument object1 = (GeneralDocument) request.getRequestContext().getObjectBeanManager()
        		.getObject(generalDocument);
        
        //necesario para preview
        request.setModel("document", object1);
        
      
      }
    } catch (ObjectBeanManagerException e) {
      logger.error("exception generalDocument {}", e.getMessage());
    }
  }

}
