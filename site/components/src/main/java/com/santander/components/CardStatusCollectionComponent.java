package com.santander.components;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import com.google.common.base.Strings;
import com.santander.beans.CardStatusCollection;
import com.santander.beans.CardStatusItem;
import com.santander.dto.GenericDAOResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = CardStatusCollectionComponentInfo.class)
public class CardStatusCollectionComponent extends CommonComponent {

  private static Logger logger = LoggerFactory.getLogger(CardStatusCollectionComponent.class);

  @Override
  public void doBeforeRender(HstRequest request, HstResponse response) {
    try {
      super.doBeforeRender(request, response);
      CardStatusCollectionComponentInfo cardCollectionStatusComponentInfo = getComponentParametersInfo(request);
      String cardStatusCollection = cardCollectionStatusComponentInfo.getCardStatusCollection();
      if (!Strings.isNullOrEmpty(cardStatusCollection)) {
        CardStatusCollection object1 = (CardStatusCollection) request.getRequestContext()
        		.getObjectBeanManager().getObject(cardStatusCollection);
        
        
        //necesario para preview
        request.setModel("document", object1);
        
        //request.setModel("cardStatusCollection", object1);
       // int cont = 0;
         /*   for (CardStatusItem item : object1.getCardStatusItem()) {
      String fixDescription = (new GenericDAOResource()).rewriteHstRichContent(item.getDescription().getContent()
        		  , item.getDescription().getNode(), RequestContextProvider.get().getResolvedMount().getMount());
          request.setModel("description" + cont, fixDescription);
          cont++;
        }*/
      }
    } catch (ObjectBeanManagerException e) {
      logger.error("exception {}", e.getMessage());
    }
  }

}
