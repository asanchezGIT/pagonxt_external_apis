package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface ReleaseNoteComponentInfo {

    @Parameter(name = "releaseNoteComponentDocument", displayName = "Release Note Component Document")
    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:ReleaseNote"})
    String getReleaseNote();

}
