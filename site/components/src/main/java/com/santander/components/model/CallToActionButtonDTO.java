package com.santander.components.model;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Produces({MediaType.APPLICATION_JSON})
public class CallToActionButtonDTO {

	
    private String name;
    private String internalLinkId;
    private String externalLink;
    private String channel;
    private String locale;
    private String labelButton;
    private String colorButton;
    private String tab;
    private String parentProductId;
    private String version;
    private String url;

}
