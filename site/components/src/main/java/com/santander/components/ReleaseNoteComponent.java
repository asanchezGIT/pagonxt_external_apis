package com.santander.components;

import com.santander.beans.*;
import com.santander.components.model.NoteDTO;
import com.santander.components.model.ReleaseNoteLevelOneDTO;
import com.santander.components.model.ReleaseNoteLevelTwoDTO;
import com.santander.utils.InternalLinkProcessor;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import com.google.common.base.Strings;
import com.santander.dto.GenericDAOResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.xml.ws.BindingProvider;
import java.util.ArrayList;
import java.util.List;

@ParametersInfo(type = ReleaseNoteComponentInfo.class)
public class ReleaseNoteComponent extends CommonComponent {

    private static Logger logger = LoggerFactory.getLogger(ReleaseNoteComponent.class);

    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) {

        try {
            super.doBeforeRender(request, response);
            InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
            GenericDAOResource genericDAOResource = new GenericDAOResource();
            ReleaseNoteComponentInfo releaseNoteComponentInfo = getComponentParametersInfo(request);
            String releaseNoteDocument = releaseNoteComponentInfo.getReleaseNote();
            if (!Strings.isNullOrEmpty(releaseNoteDocument)) {
                ReleaseNote object1 = (ReleaseNote) request.getRequestContext().getObjectBeanManager().getObject(releaseNoteDocument);

                //necesario para preview
                request.setModel("document", object1);
                //request.setModel("releaseNote", object1.getLevelOne());
                

                //List<ReleaseNoteLevelOneDTO> releaseNoteLevelOneDTOList= new ArrayList<ReleaseNoteLevelOneDTO>();
                //ProcesReleaseNote(object1.getLevelOne(),releaseNoteLevelOneDTOList);
                
                /*
                for (ReleaseNoteLevelOne item : object1.getLevelOne()) {
                    processLinksAndDownloads(request, releaseNoteLevelOneDTOList, item);

                }
                */
                //request.setModel("release", releaseNoteLevelOneDTOList);


            }
        } catch (ObjectBeanManagerException obme) {
            logger.error("Error: ", obme);
          }
    }


    /*private void processLinksAndDownloads(List<LinksAndDownloadsDTO> linksLista,
                                          LinksAndDownloads item, InternalLinkProcessor internalLinkProcessor,GenericDAOResource genericDAOResource) {
        try {

            if (item.getInternalLink() != null) {

                NodeType primaryType = JcrUtils.getPrimaryNodeType(item.getInternalLink().getNode());

                linksLista.add(
                        LinksAndDownloadsDTO.builder()
                                .externalLink(item.getExternalLink())
                                .title(item.getTitle())
                                .tab(item.getTab())
                                .internalLink(internalLinkProcessor.generateInternalLink(genericDAOResource.generateHstLink(item.getInternalLink()),primaryType.getName(), item.getInternalLink().getCanonicalUUID()))
                                .build());



            } else {

                linksLista.add(
                        LinksAndDownloadsDTO.builder()
                                .externalLink(item.getExternalLink())
                                .title(item.getTitle())
                                .tab(item.getTab())
                                .internalLink(null)
                                .build());
            }

        } catch (RepositoryException | QueryException e) {
            logger.error("",e);
        }
    }*/

    private void processLinksAndDownloads(HstRequest request, List<ReleaseNoteLevelOneDTO> releaseNoteLevelOneDTOList, ReleaseNoteLevelOne item) throws RepositoryException {

        List<ReleaseNoteLevelTwoDTO> sections = new ArrayList<ReleaseNoteLevelTwoDTO>();
        ReleaseNoteLevelTwo item2 = new ReleaseNoteLevelTwo();

        List<NoteDTO> noteList = new ArrayList<NoteDTO>();
        Note notes = new Note();

        //sections = getListReleaseNoteLevelTwoDTO(item);
        /*noteList.add(
                NoteDTO.builder()
                        .title(notes.getTitle())
                        .description((new GenericDAOResource()).rewriteHstRichContent(
                                notes.getDescription().getContent(), notes.getDescription().getNode(),
                                request.getRequestContext().getResolvedMount().getMount()))
                        .icon(notes.getIcon())
                        .build());


         */


        sections.add(
                ReleaseNoteLevelTwoDTO.builder()

                        .title(item2.getTitle())
                        .note(noteList)

                .build());

        releaseNoteLevelOneDTOList.add(

                ReleaseNoteLevelOneDTO.builder()

                        .title(item.getTitle())
                        .level(sections)
                        .build());


    }


    //esto es lo que intentamos hacer ayer Alfonso y yo pero no salía en el model
    /*private List<ReleaseNoteLevelTwoDTO> getListReleaseNoteLevelTwoDTO(ReleaseNoteLevelOne item) throws RepositoryException {

        List<ReleaseNoteLevelTwoDTO> sections = new ArrayList<>();
        for(int i = 0; i<=item.getLevel().size(); i++){
            //List<ReleaseNoteLevelTwoDTO> notes = new ArrayList<ReleaseNoteLevelTwoDTO>(); cambiar tipo
            Node levelTwo = item.getLevel().get(i).getNode();
            //notes = getListNotesDTO(levelTwo);
            sections.add(
                    ReleaseNoteLevelTwoDTO.builder()
                            //.title(levelTwo.getProperty("santanderbrxm:title").getString())
                            //.note(notes)
                            .build());
        }


        return sections;

    */

    /*private List<ReleaseNoteLevelTwoDTO> getListNotesDTO(ReleaseNoteLevelTwo levelTwo) {
    }*/

    /*private void processSectionLevels(HstRequestContext requestContext,List<ReleaseNoteLevelOneDTO> releaseNoteLevelOneDTOList, ReleaseNoteLevelOne level1) {
        ReleaseNoteLevelOneDTO page = new ReleaseNoteLevelOneDTO();
        page.setTitle(level1.getTitle());
        page.setRichContent(
                Objects.nonNull(level1.getDescription()) ? rewriteHstRichContent(level1
                                .getDescription().getContent(), level1.getDescription().getNode(),
                        requestContext.getResolvedMount().getMount()) : StringUtils.EMPTY);
        List<SectionDTO> sections = new ArrayList<>();
        for (SectionLevelTwo level2 : level1.getSections()) {
            SectionDTO section = new SectionDTO();
            section.setTitle(level2.getTitle());
            section.setRichContent(
                    Objects.nonNull(level2.getDescription())
                            ? rewriteHstRichContent(level2.getDescription()
                            .getContent(), level2.getDescription()
                            .getNode(), requestContext.getResolvedMount()
                            .getMount()) : StringUtils.EMPTY);
            List<SectionDTO> sections2 = new ArrayList<>();
            for (SectionLevelThree level3 : level2.getSections()) {
                SectionDTO section2 = new SectionDTO();
                section2.setTitle(level3.getText());
                section2.setRichContent(
                        Objects.nonNull(level3.getHtml()) ? rewriteHstRichContent(level3.getHtml()
                                .getContent(), level3.getHtml().getNode(), requestContext
                                .getResolvedMount().getMount()) : StringUtils.EMPTY);
                sections2.add(section2);
            }
            section.setSections(sections2);
            sections.add(section);
        }
        page.setSections(sections);
        pages.add(page);
    }*/
}
