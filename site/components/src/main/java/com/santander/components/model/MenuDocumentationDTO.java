package com.santander.components.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.santander.beans.MenuDocumentation;
import com.santander.beans.SubMenuDocumentationLevelOne;
import com.santander.dto.GenericDAOResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MenuDocumentationDTO extends GenericDAOResource{
 
  private List<SubMenuDocumentationDTO> subMenusDocumentation;
 
  
  private static Logger logger = LoggerFactory.getLogger(MenuDocumentationDTO.class);
  
  public MenuDocumentationDTO(MenuDocumentation menuDocumentation)  {
	  List<SubMenuDocumentationDTO> submenusDocumentationsDTO = new ArrayList<>(); 
	  
	  if (Objects.nonNull(menuDocumentation.getDocumentsGroups())) { 
	     for (SubMenuDocumentationLevelOne submenu :menuDocumentation.getDocumentsGroups()) {
	    	  
	    	  if(Objects.nonNull(submenu.getTitle())){
	       		  SubMenuDocumentationDTO submenuDTO=new SubMenuDocumentationDTO(submenu);
	    		  submenusDocumentationsDTO.add(submenuDTO);
	    	  
	    	  }
	    }
	 }
    
	 this.subMenusDocumentation=submenusDocumentationsDTO;
		 
  }

}
	 
	 
