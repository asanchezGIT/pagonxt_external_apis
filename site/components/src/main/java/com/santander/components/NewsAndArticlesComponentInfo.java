package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface NewsAndArticlesComponentInfo {

	@Parameter(name = "resourceDocument", displayName = "Resource Document")
	@JcrPath(pickerSelectableNodeTypes = { "santanderbrxm:NewsAndArticles" })
	String getResourceDocument();

}
