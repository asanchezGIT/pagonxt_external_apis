package com.santander.components;

import com.google.common.base.Strings;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = ProductsComponentInfo.class)
public class ProductsComponent extends CommonComponent {

    private static Logger logger = LoggerFactory.getLogger(ProductsComponent.class);

    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) {
       
        try {
            super.doBeforeRender(request, response);
            ProductsComponentInfo productsComponentInfo = getComponentParametersInfo(request);
            String productDescription = productsComponentInfo.getProductBannerDescription();
            String product1path = productsComponentInfo.getProduct1();
            String product2path = productsComponentInfo.getProduct2();
            String product3path = productsComponentInfo.getProduct3();
            if (!Strings.isNullOrEmpty(productDescription)) {
                Object description = request.getRequestContext().getObjectBeanManager().getObject(productDescription);
                request.setModel("productBannerDescription", description);
            }
            if (!Strings.isNullOrEmpty(product1path)) {
                Object object1 = request.getRequestContext().getObjectBeanManager().getObject(product1path);
                request.setModel("document1", object1);
            }
            if (!Strings.isNullOrEmpty(product2path)) {
                Object object2 = request.getRequestContext().getObjectBeanManager().getObject(product2path);
                request.setModel("document2", object2);
            }
            if (!Strings.isNullOrEmpty(product3path)) {
                Object object3 = request.getRequestContext().getObjectBeanManager().getObject(product3path);
                request.setModel("document3", object3);
            }
        } catch (ObjectBeanManagerException obme) {
            logger.error("Error: {}", obme.getMessage());
        }
    }

}
