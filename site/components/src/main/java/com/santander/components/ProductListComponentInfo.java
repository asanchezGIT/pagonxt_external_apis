package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface ProductListComponentInfo {

    @Parameter(name = "productList", displayName = "Product List")
    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:ProductList"})
    String getProductList();
}

