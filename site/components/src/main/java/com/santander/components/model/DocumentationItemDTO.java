package com.santander.components.model;

import java.util.Objects;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.core.request.HstRequestContext;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.santander.beans.Documentation;
import com.santander.dto.ApiContainerDTO;
import com.santander.dto.DescriptionDTO;
import com.santander.dto.GenericDAOResource;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Produces({MediaType.APPLICATION_JSON})

public class DocumentationItemDTO extends GenericDAOResource {
	
private String title;
private String href; 	
	
public DocumentationItemDTO(Documentation documentation) {
	
	//title
	if(Objects.nonNull(documentation.getTitle())){
		this.title=documentation.getTitle();
	}
	else {
		  this.title=StringUtils.EMPTY;
	}
	
	//href
	try	{

		HstRequestContext requestContext = RequestContextProvider.get(); 
		InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
		
		String url =requestContext.getHstLinkCreator().create(documentation, requestContext)
		        .toUrlForm(requestContext, true);
		String uuid=HippoUtils.getNodeId(documentation.getNode().getPath(), requestContext);
	
		String type = documentation.getNode().getPrimaryNodeType().getName();
		 
		this.href= internalLinkProcessor.generateInternalLink(url, type,uuid);
	}
	catch (Exception e) {
		this.href="";
	}

	

	
}

@Override public String toString() {
    return "DocumentationDTO [href=" + href +",title=" + title+ "]";
}

}


