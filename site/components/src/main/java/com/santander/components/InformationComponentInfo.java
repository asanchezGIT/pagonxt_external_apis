package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

public interface InformationComponentInfo {

    @Parameter(name = "information1", displayName = "Information 1")
    @JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:informationItem"})
    String getInformation1();

    @Parameter(name = "information2", displayName = "Information 2")
    @JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:informationItem"})
    String getInformation2();

    @Parameter(name = "information3", displayName = "Information 3")
    @JcrPath( pickerSelectableNodeTypes = {"santanderbrxm:informationItem"})
    String getInformation3();

}
