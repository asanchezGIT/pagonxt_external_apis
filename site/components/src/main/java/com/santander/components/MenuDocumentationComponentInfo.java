package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface MenuDocumentationComponentInfo {

    @Parameter(name = "MenuDocumentation", displayName = "Menu Documentation")
    @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:MenuDocumentation"})
    String getMenuDocumentation();
}

