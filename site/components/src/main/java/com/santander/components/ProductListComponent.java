package com.santander.components;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import com.google.common.base.Strings;
import com.santander.beans.CardStatusCollection;
import com.santander.beans.CardStatusItem;
import com.santander.beans.Documentation;
import com.santander.beans.ProductContainerItem;
import com.santander.beans.ProductList;
import com.santander.components.model.DocumentationItemDTO;
import com.santander.components.model.ProductContainerItemDTO;
import com.santander.dto.GenericDAOResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = ProductListComponentInfo.class)
public class ProductListComponent extends CommonComponent {

  private static Logger logger = LoggerFactory.getLogger(ProductListComponent.class);

  @Override
  public void doBeforeRender(HstRequest request, HstResponse response) {
    try {
       super.doBeforeRender(request, response);
      ProductListComponentInfo productListComponentInfo = getComponentParametersInfo(request);
      String productListString = productListComponentInfo.getProductList();
      
      if (!Strings.isNullOrEmpty(productListString)) {
        ProductList productList = (ProductList) request.getRequestContext()
        		.getObjectBeanManager().getObject(productListString);
        
        
        // Recoge los ProductContainerItems de la lista
        Iterator<HippoBean> productsIterator=productList.getProductos().iterator();
        
        
        List <ProductContainerItemDTO> productContainerItemDTOList=new ArrayList<>();
        ProductContainerItem productContainerItem=null;
        ProductContainerItemDTO productContainerItemDTO=null;

        HippoHtml descOverview;
        while (productsIterator.hasNext()) {
        	productContainerItem = (ProductContainerItem) productsIterator.next();
        	
        	descOverview=productContainerItem.getDescriptionOverview();
        	productContainerItemDTO=new ProductContainerItemDTO(productContainerItem);
        	productContainerItemDTOList.add(productContainerItemDTO);
           
        }

        //añado el productItemDTOList en el model
        
        request.setModel("ProductsList",productContainerItemDTOList);
       
        
        //añado todo el document al model para que front pueda hacer el preview
        request.setModel("document", productList);
        
      }
    } catch (ObjectBeanManagerException e) {
      logger.error("exception {}", e.getMessage());
    }
  }

}
