package com.santander.components.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import com.santander.beans.Documentation;
import com.santander.beans.DocumentsGroup;
import com.santander.dto.GenericDAOResource;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DocumentsGroupDTO extends GenericDAOResource{
 
	private String title;
	private List<DocumentationItemDTO> documentations; 	
		
	public DocumentsGroupDTO(DocumentsGroup documentsGroup) {
		
		//title
		if(Objects.nonNull(documentsGroup.getTitle())){
			this.title=documentsGroup.getTitle();
		}
		else {
			  this.title=StringUtils.EMPTY;
		}
		
		//documentations. Para cada uno de los documento utiliza el DocumentationItemDTO
		
		
		 //documentos destacados del producto
		List<DocumentationItemDTO> documentationsDTOList = new ArrayList<>();
		if (Objects.nonNull(documentsGroup.getDocuments())) { 
			
			 
			 List <HippoBean> documentations= documentsGroup.getDocuments();
			 
			 
			 Documentation documentation=null;
			 DocumentationItemDTO documentationItemDTO =null;
			 
			 
		     int i=0;
		     while (i<documentations.size()) {
		    	 documentation= (Documentation)documentations.get(i);
		    	 documentationItemDTO= new DocumentationItemDTO(documentation);
		    	 documentationsDTOList.add(documentationItemDTO);
			       i++;	 
		     }
		}
		this.documentations=documentationsDTOList;
		
	}


}


 