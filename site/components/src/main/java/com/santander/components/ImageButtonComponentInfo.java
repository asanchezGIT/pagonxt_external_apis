package com.santander.components;

import org.hippoecm.hst.core.parameters.JcrPath;
import org.hippoecm.hst.core.parameters.Parameter;

@FunctionalInterface
public interface ImageButtonComponentInfo {

  @Parameter(name = "document", displayName = "Image Button Document")
  @JcrPath(pickerSelectableNodeTypes = {"santanderbrxm:ImageButton"})
  String getResourceDocument();

}
