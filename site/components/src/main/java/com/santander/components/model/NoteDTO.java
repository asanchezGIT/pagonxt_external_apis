package com.santander.components.model;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.santander.beans.ReleaseNoteLevelTwo;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@Produces({MediaType.APPLICATION_JSON})
public class NoteDTO {

    public String title;
    public String description;
    public HippoGalleryImageSet icon;
}
