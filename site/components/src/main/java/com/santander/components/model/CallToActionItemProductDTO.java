package com.santander.components.model;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;

import com.santander.beans.ProductListCompound;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Produces({MediaType.APPLICATION_JSON})
public class CallToActionItemProductDTO {
	
	String name;
	String title;
	String description;
	HippoGalleryImageSet image;
	
	String internalLinkId;
	String externalLink;
	String tab;
	
	String parentProductId;
	String version;
	String url;
	
	String channel;
	String locale;
	ProductListCompound productList;
	
	
	
	
	
	
	  
	
	
	
}
