package com.santander.components;

import com.google.common.base.Strings;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = ResourceBannerImageComponentInfo.class)
public class ResourceBannerImageComponent extends CommonComponent {

    private static Logger logger = LoggerFactory.getLogger(ResourceBannerImageComponent.class);

    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) {
        try {
            super.doBeforeRender(request, response);
            ResourceBannerImageComponentInfo resourceBannerImageInfo = getComponentParametersInfo(request);
            String resourceBannerImage1path = resourceBannerImageInfo.getResourceBannerImage();
            if (!Strings.isNullOrEmpty(resourceBannerImage1path)) {
                Object object1 = request.getRequestContext().getObjectBeanManager().getObject(resourceBannerImage1path);
                request.setModel("document", object1);
            }
        } catch (ObjectBeanManagerException e) {
            logger.error("Error: {}", e.getMessage());
        }
    }

}