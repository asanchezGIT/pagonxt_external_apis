package com.santander.components;

import com.google.common.base.Strings;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.component.HstResponse;
import org.hippoecm.hst.core.parameters.ParametersInfo;
import org.onehippo.cms7.essentials.components.CommonComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ParametersInfo(type = FAQBannerComponentInfo.class)
public class FAQBannerComponent extends CommonComponent {

    private static Logger logger = LoggerFactory.getLogger(FAQBannerComponent.class);

    @Override
    public void doBeforeRender(HstRequest request, HstResponse response) {
        try {
            super.doBeforeRender(request, response);
            FAQBannerComponentInfo faqBannerComponentInfo = getComponentParametersInfo(request);
            String faqBannerPath = faqBannerComponentInfo.getFAQBanner();

            if (!Strings.isNullOrEmpty(faqBannerPath)) {
                Object object1 = request.getRequestContext().getObjectBeanManager().getObject(faqBannerPath);
               //necesario para preview
                request.setModel("document1", object1);
            }

        } catch (ObjectBeanManagerException e) {
            logger.error("exception {}", e.getMessage());
        }
    }

}