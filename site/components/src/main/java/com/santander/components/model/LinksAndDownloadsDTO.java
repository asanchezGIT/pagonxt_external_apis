package com.santander.components.model;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@Produces({MediaType.APPLICATION_JSON})
public class LinksAndDownloadsDTO {


    private String externalLink;
    private String title;
    private String tab;
    private String internalLink;

}