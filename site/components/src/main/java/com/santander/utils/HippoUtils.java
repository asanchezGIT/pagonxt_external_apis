package com.santander.utils;


import com.santander.beans.*;


import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.content.beans.query.filter.Filter;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.content.beans.standard.HippoDocumentBean;
import org.hippoecm.hst.core.component.HstRequest;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.util.ContentBeanUtils;
import org.hippoecm.hst.util.PathUtils;
import org.hippoecm.repository.HippoStdNodeType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.Map;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;


public class HippoUtils {

    private HippoUtils() {}


    private static Logger logger = LoggerFactory.getLogger(HippoUtils.class);
    private static HstRequestContext hstRequestContext = RequestContextProvider.get();

    private static String channell = "channel";
    private static String locales = "locale";
    private static String channnel = "channel: {}";
    private static String localle = "locale: {}";





    public static String getApiProductRelatedDocuments(String uuid, HstRequestContext hstRequestContext) 
    		throws QueryException, NullPointerException, IllegalArgumentException {
        ProductContainerItem productBean = null;
        final HippoDocumentBean hippoDocumentBean = hstRequestContext.getSiteContentBaseBean()
        		.getBeanByUUID(uuid, ApiContainerItem.class);
        HstQuery hstQuery = ContentBeanUtils.createIncomingBeansQuery(hippoDocumentBean, 
        		hstRequestContext.getSiteContentBaseBean(), "santanderbrxm:apiContainers/@hippo:docbase",
                ProductContainerItem.class, false);
        HstQueryResult products = hstQuery.execute();
        HippoBeanIterator iterator = products.getHippoBeans();
        if(iterator.hasNext()) {
            productBean = (ProductContainerItem) iterator.nextHippoBean();
            logger.debug("Parent Product found is : {}" , productBean.getIdentifier());
        }
        return productBean != null ? productBean.getIdentifier() : StringUtils.EMPTY;
    }


  
    

    public static String getApiItemRelatedDocuments(String uuid, HstRequestContext hstRequestContext) 
    		throws QueryException, NullPointerException, IllegalArgumentException {
        ApiContainerItem apiContainerBean = null;
        final HippoDocumentBean hippoDocumentBean = hstRequestContext.getSiteContentBaseBean()
        		.getBeanByUUID(uuid, ApiItem.class);
        HstQuery hstQuery = ContentBeanUtils.createIncomingBeansQuery(hippoDocumentBean, 
        		hstRequestContext.getSiteContentBaseBean(), "santanderbrxm:api/@hippo:docbase",
        		ApiContainerItem.class, false);
        HstQueryResult products = hstQuery.execute();
        HippoBeanIterator iterator = products.getHippoBeans();
        if(iterator.hasNext()) {
            apiContainerBean = (ApiContainerItem) iterator.nextHippoBean();
            
        }
        return apiContainerBean != null ? apiContainerBean.getIdentifier() : StringUtils.EMPTY;
    }

    //TODO test me
    public static String getRelatedDocument(Node node, Class<BaseDocument> aClass, Class<?> parentClass, 
    		String documentType) throws QueryException, RepositoryException {
        if(parentClass.equals(Product.class)) {
            Product document = null;
            final HippoDocumentBean hippoDocumentBean = hstRequestContext.getSiteContentBaseBean()
            		.getBeanByUUID(node.getIdentifier(), aClass);
            HstQuery hstQuery = ContentBeanUtils.createIncomingBeansQuery(hippoDocumentBean, hstRequestContext
            		.getSiteContentBaseBean(), documentType.toLowerCase() + "/@hippo:docbase", Product.class, false);
            HstQueryResult beans = hstQuery.execute();
            HippoBeanIterator iterator = beans.getHippoBeans();
            if(iterator.hasNext()) {
                document = (Product) iterator.nextHippoBean();
                logger.debug("Parent Product found is : {}",  document.getTitle());
            }
            return document != null ? document.getIdentifier() : StringUtils.EMPTY;
        }
        return "";
    }


    public static String getNodeId(String contentPath, HstRequestContext hstRequestContext) throws RepositoryException {
        return hstRequestContext.getSession().getNode(contentPath).getIdentifier();
    }

    public static String getPublishedVariantNodeId(Node node) throws RepositoryException {
        String publishedNodeId = "";
        for(NodeIterator variantsIterator = node.getNodes(node.getName());variantsIterator.hasNext();) {
            Node variant = variantsIterator.nextNode();
            String state = variant.getProperty(HippoStdNodeType.HIPPOSTD_STATE).getString();
            if(HippoStdNodeType.PUBLISHED.equals(state)) {
                publishedNodeId = variant.getIdentifier();
            }
        }
        return publishedNodeId;
    }

    public static String generateHstLink(HippoBean bean) {
        return hstRequestContext.getHstLinkCreator().create(bean, hstRequestContext)
                .toUrlForm(hstRequestContext, true);
    }

    public static Map<String, String> getDocumentAttrsByURL(HstRequest request, HippoBean bean) {
        String url = request.getRequestContext().getHstLinkCreator().create(bean, request.getRequestContext())
        		.toUrlForm(request.getRequestContext(), true);
        logger.debug("url document: {}" , url);
        String channel = "";
        String locale = "";



        String[] splitSite = url.split("/site/");
        if (splitSite.length > 1) {
            String afterSite = splitSite[1];
            String[] urlContexts = afterSite.split("/");
            if (urlContexts.length > 1) {
                channel = urlContexts[0];
                locale = urlContexts[1];
            }
        }
        HashMap<String, String> attrs = new HashMap<>();
        attrs.put(channell, channel);
        attrs.put(locales, locale);
        logger.debug(channnel , channel);
        logger.debug(localle , locale);
        return attrs;
    }
    
    public static Map<String, String> getDocumentAttrsByURLCustom(HippoBean bean){
    	logger.debug("****INICIO getDocumentAttrsByURLCustom");
    	
    	String channel = "";
        String locale = "";
    	String url=bean.getPath();
      	logger.debug("**** url:"+url);	
    	
    	String[] splitSite =url.split(Constants.PROJECT_ROOT_PATH);

	
		//ejem: content/documents/santander/santander-superdigital/en_gb/content/producercontainers........
    	//splitSite[0]=content/documents/santander/santander-
    	//splitSite[1]=superdigital/en_gb/content/producercontainers		
    			
	    if (splitSite.length > 1) {
	    	channel=splitSite[1].substring(0,splitSite[1].indexOf("/"));  //superdigital
	    	logger.debug("*****channel:"+channel);
	    	String aux=splitSite[1].substring(splitSite[1].indexOf("/")+1); //en_gb/content/producercontainers
	    	locale=aux.substring(0,aux.indexOf("/"));            //en_gb
	    	logger.debug("*****locale :"+locale);
	    }
	    HashMap<String, String> attrs = new HashMap<>();
        attrs.put(channell, channel);
        attrs.put(locales, locale);
        logger.debug(channnel , channel);
        logger.debug(localle , locale);
        return attrs;
	   
	    
    
    }
    

    public static Map<String, String> getDocumentAttrsByURLCustom(HstRequestContext requestContext, HippoBean bean) {
    	
    	
    	
        String url = requestContext.getHstLinkCreator().create(bean, requestContext).toUrlForm(requestContext, true);
        logger.debug("url document: {}" , url);
        String channel = "";
        String locale = "";
        String[] splitSite = url.split("/customapi/");
        if (splitSite.length > 1) {
            String afterSite = splitSite[1];
            String[] urlContexts = afterSite.split("/");
            if (urlContexts.length > 1) {
                channel = urlContexts[0];
                locale = urlContexts[1];
            }
        } else {
            String[] splitSiteMarketplace = url.split("/site/");
            if (splitSiteMarketplace.length > 1) {
                String afterSite = splitSiteMarketplace[1];
                String[] urlContexts = afterSite.split("/");
                if (urlContexts.length > 1) {
                    channel = urlContexts[0];
                    locale = urlContexts[1];
                }
            }
        }
        HashMap<String, String> attrs = new HashMap<>();
        attrs.put(channell, channel);
        attrs.put(locales, locale);
        logger.debug(channnel , channel);
        logger.debug(localle , locale);
        return attrs;
    }
    
    public static HippoBeanIterator getObjectTypeDefaultLanguaje(HstRequestContext requestContext,String typeObject,String defaultLocale,String uuid,HstQueryManager hstQueryManager) {
    
    	HippoBeanIterator iterator=null; 
    	try {
    		
    		
		    	//Pongo el punto de montaje del lenguaje por defecto	
		   	 String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
		   	 String mountContentDefaultLocalePath=(mountContentPath.substring(0, mountContentPath.lastIndexOf("/")+1)).concat(defaultLocale.toLowerCase());
		   	 Node mountContentNodeDefaultLocale = requestContext.getSession().getRootNode().getNode(PathUtils.normalizePath(mountContentDefaultLocalePath));
		   	logger.debug("*****mountContentNodeDefaultLocale:"+mountContentNodeDefaultLocale.getPath());
		   	 
		   	 		HstQuery hstQuery = hstQueryManager.createQuery(mountContentNodeDefaultLocale);

		   	 		if ("ProductContainerItem".equals(typeObject)){
		   	 			 hstQuery = hstQueryManager.createQuery(mountContentNodeDefaultLocale,ProductContainerItem.class);
		   	 			
		   	 		}
		   	 		
		   	 		else if ("ApiContainerItem".equals(typeObject)){
		   	 			hstQuery = hstQueryManager.createQuery(mountContentNodeDefaultLocale,ApiContainerItem.class);
	   	 			
		   	 		}
		   	 	
		   	 		else if ("Documentation".equals(typeObject)){
		   	 			hstQuery = hstQueryManager.createQuery(mountContentNodeDefaultLocale,Documentation.class);
   	 			
		   	 		}
		   	 
		   	 		else if ("GettingStartingPage".equals(typeObject)){
		   	 			hstQuery = hstQueryManager.createQuery(mountContentNodeDefaultLocale,GettingStartingPage.class);
  	 			
		   	 		}
		   	 		else if ("NewsAndArticles".equals(typeObject)){
		   	 			hstQuery = hstQueryManager.createQuery(mountContentNodeDefaultLocale,NewsAndArticles.class);
	 			
		   	 		}
	   	 		
		   	 		
		   		 	
		       		Filter filter = hstQuery.createFilter();
		       		filter.addEqualTo("santanderbrxm:alias", uuid);
		         	hstQuery.setFilter(filter);
		         	
		         	HstQueryResult resultDefault = hstQuery.execute();
		            iterator = resultDefault.getHippoBeans();
		            
			            
	 }
		   	 
		catch (Exception e){
			 logger.error("getObjectTypeDefaultLanguaje ERROR:"+e.getMessage());	 
		}
    	
    	return iterator;
		   
    }
    
    
    public static String getSiteName(String  scopePath) {
    	
    	String siteName="";
	    String[] splitSite =scopePath.split(Constants.PROJECT_ROOT_PATH);
	    if (splitSite.length > 1) {
	    	siteName=splitSite[1].substring(0,splitSite[1].indexOf("/"));
	    }
	     return siteName;
    
    }
    
    
            
    
}
