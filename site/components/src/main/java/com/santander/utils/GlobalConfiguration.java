package com.santander.utils;

import com.santander.beans.SantanderGlobalConfiguration;

import javax.jcr.Node;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.query.filter.Filter;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.util.PathUtils;
import org.onehippo.cms7.essentials.components.rest.BaseRestResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlobalConfiguration extends BaseRestResource {

    private static Logger logger = LoggerFactory.getLogger(GlobalConfiguration.class);
    //private HstRequestContext hstRequestContext = RequestContextProvider.get();

    public  String getConfigurationById(String id, HstRequestContext requestContext) {

        String value = "";

        try {
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            Node mountContentNode = requestContext.getSession().getRootNode().getNode(PathUtils.normalizePath(Constants.CONTENT_CONFIGURATION_PATH));
       	    HstQuery hstQuery = hstQueryManager.createQuery(mountContentNode, SantanderGlobalConfiguration.class);
       	    
 
            if (!id.isEmpty()) {
                Filter filter = hstQuery.createFilter();
                filter.addEqualTo("santanderbrxm:id", id);
                hstQuery.setFilter(filter);
            }

            HstQueryResult result = hstQuery.execute();

            HippoBeanIterator iterator = result.getHippoBeans();
            SantanderGlobalConfiguration conf=null;
            while (iterator.hasNext()){
                conf = (SantanderGlobalConfiguration) iterator.nextHippoBean();
                value = conf.getValue();
            }

        } catch (Exception e) {
            logger.error("Error get configuration by ID: {}", e);
        }

        return value;


    }
    
    public  String getConfiguration(HstRequestContext requestContext) {

    	
    	logger.debug("*****Inicio getConfiguration");
        String value = "en_GB";

        try {
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            HippoBean scope = getMountContentBaseBean(requestContext);
            //Punto de montaje superior. Sin idioma
            String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
        	String mountContentSitePath=(mountContentPath.substring(0, mountContentPath.lastIndexOf("/")));
       	 
       	 	logger.debug("*****mountContentSitePath:"+mountContentSitePath);
       	 
       	 	Node mountContentNode = requestContext.getSession().getRootNode().getNode(PathUtils.normalizePath(mountContentSitePath));
       	    HstQuery hstQuery = hstQueryManager.createQuery(mountContentNode, SantanderGlobalConfiguration.class);
          
            HstQueryResult result = hstQuery.execute();

            HippoBeanIterator iterator = result.getHippoBeans();
            SantanderGlobalConfiguration conf=null;
            while (iterator.hasNext()){
            	logger.debug("encontrado el SantanderGlobalConfiguration");
                conf = (SantanderGlobalConfiguration) iterator.nextHippoBean();
                value = conf.getValue();
            }

        } catch (Exception e) {
            logger.error("Error get configuration by ID: {}", e);
        }

        return value;


    }
    
    
    
}
