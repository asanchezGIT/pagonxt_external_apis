package com.santander.links;

import com.santander.utils.Constants;
import com.santander.utils.InternalLinkProcessor;
import lombok.SneakyThrows;

import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.core.linking.HstLink;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.linking.HstLinkProcessorTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;

public class DevmktLinksProcessor extends HstLinkProcessorTemplate {

	
	public static final String RESOURCEAPI_NAME = Constants.RESOURCEAPI_NAME;
	public static final String CUSTOMAPI_NAME = Constants.CUSTOMAPI_NAME;
	public static final String SITE_CUSTOMAPI = Constants.SITE_CUSTOMAPI;
	
	private static Logger logger = LoggerFactory.getLogger(DevmktLinksProcessor.class);
    
                                                   

    @SneakyThrows
    @Override
    protected HstLink doPostProcess(HstLink link) {
    	
    	//mountContentPath instead of DOCUMENTS_FOLDER
    	
    	HstRequestContext requestContext = RequestContextProvider.get();
    	String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
    	
       	//en los campos de textos enriquecidos (campos hrml) , la url a los enlaces a contenidos internos se modifican a enlaces a los servicios custom para servir ese contenido.
    	//Esto es para que Front pueda saber la url de invocación para recuperar el contenido.
    	if(link.getPath().contains(".html") && !link.getPath().contains("pages") && !link.getMount().getParent().getMountPath().contains("customapi")) {
    		
    		//html que no sean de un customapi, ni sean de una página directa de bloomreach( nosotros no usamos páginas directas)
    	    logger.debug("Link post process {}", link.getPath());
            logger.debug("bean id {}", link.getMount().getMountPath());
     
            //HstRequestContext requestContext = RequestContextProvider.get();
            InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
            String pathInternalLink = link.getPath();
            String pathSite = link.getMount().getMountPath();
            String urlFormatt = "";
            String linkFormatt = "";

            //logger.info("******pathSite:"+pathSite);
            if(pathSite.contains(RESOURCEAPI_NAME) ){
            	pathSite = pathSite.replace(RESOURCEAPI_NAME, "");
                urlFormatt = SITE_CUSTOMAPI + pathSite + pathInternalLink;
           
	           //}
	            
	            //logger.info("******urlFormatt:"+urlFormatt);
	           
	            String contentUUID = internalLinkProcessor.getInternalLinkUUID(internalLinkProcessor.translateInternalLinkURLToContentPath(urlFormatt));
	            //logger.info("******contentUUID:"+contentUUID);
	            
	            
	            String contentType = internalLinkProcessor.getInternalLinkDocumentType(contentUUID);
	            //logger.info("******contentType:"+contentType);
	            
	            linkFormatt = internalLinkProcessor.generateInternalLink(urlFormatt, contentType, contentUUID);
	            //logger.info("linkFormatt:  {}", linkFormatt);
	
	            //Node newApiContainerNode = null; comentado pq no se usa posteriormente para nada
	            try {
	                //newApiContainerNode = requestContext.getSession().getNode(mountContentPath+"/" + link.getPath().replaceAll(".html", ""));
	                link.setPath(linkFormatt);
	            } catch (Exception e) {
	                logger.error("error in doPostProcess ", e);
	            }
	            
            } // Añadido aqui en lugar de en la 57
        }
    	return link;
    }

    @Override
    protected HstLink doPreProcess(HstLink link) {
    	
    	/*HstRequestContext requestContext = RequestContextProvider.get();
    	logger.debug(" Link pre process {}", link.getPath());
    	logger.debug("bean name {}",link.getMount().getName());*/
        return link;
    }
}