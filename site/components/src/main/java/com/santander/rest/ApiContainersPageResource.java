package com.santander.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.query.exceptions.FilterException;
import org.hippoecm.hst.content.beans.query.filter.Filter;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.onehippo.cms7.essentials.components.rest.BaseRestResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.santander.beans.ApiContainerItem;
import com.santander.dto.HALLinkDTO;
import com.santander.utils.Constants;
import com.santander.utils.GlobalConfiguration;
import com.santander.utils.HippoUtils;
import com.santander.utils.Locale;
import com.santander.dto.ApiContainerDTO;
import com.santander.dto.ApiContainerDetailDTO;
import com.santander.dto.ApiContainerExpandDTO;
import com.santander.dto.ApiContainersDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;
import javax.validation.constraints.NotNull;



@Path("/api_containers")
@Api(value = "Api Containers", description = "ApiContainers REST service")
public class ApiContainersPageResource extends BaseRestResource {

    private static final String OFFSET = "&_offset=";
	private static final String LIMIT = "_limit=";
	private static final String HIPPOTAXONOMY_KEYS = "hippotaxonomy:keys";
	private static Logger logger = LoggerFactory.getLogger(ApiContainersPageResource.class);
    private static final Integer URL_INDEX = 3;

    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM,
            MediaType.APPLICATION_FORM_URLENCODED})
    @ApiOperation(value = "Returns  Api Containers Detail",
            response = ApiContainerDetailDTO.class, authorizations = @Authorization(value = "clientCredentials",
            scopes = @AuthorizationScope(scope = "read_apicontainers", description = "")))
    @ApiResponses({@ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "The specified resource could not be found."),
            @ApiResponse(code = 500,
                    message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value",
                    response = ApiContainerDetailDTO.class),

    })
    @GET
    @Path("/{uuid}/")

    public Map<String, ApiContainerDetailDTO> getApiContainersByID( @Context UriInfo uriInfo,
																	@NotNull @PathParam("uuid") String uuid) {

        Map<String, ApiContainerDetailDTO> content = new HashMap<>();

        try {

            HstRequestContext requestContext = RequestContextProvider.get();
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            HippoBean scope = getMountContentBaseBean(requestContext);
            HstQuery hstQuery = hstQueryManager.createQuery(scope, ApiContainerItem.class, true);
            
            //Nombre del Site
			String scopePath=scope.getPath();
            String[] splitSite =scopePath.split(Constants.PROJECT_ROOT_PATH);
			
			String siteName="";
            if (splitSite.length > 1) {
            	siteName=splitSite[1].substring(0,splitSite[1].indexOf("/"));
            	logger.debug("*****siteName:"+siteName);
            }
            
            
            ApiContainerItem apiContainer=null;
            ApiContainerExpandDTO apiContainerExpandDTO=null;
            
            if (!uuid.isEmpty()) {
                Filter filter = hstQuery.createFilter();
                Filter filterAlias = hstQuery.createFilter(); 
            	Filter filterUuid = hstQuery.createFilter();
            	filterAlias.addEqualTo("santanderbrxm:alias", uuid);
            	filterUuid.addEqualTo("jcr:uuid", uuid);
            	filter.addOrFilter(filterAlias);
            	filter.addOrFilter(filterUuid);
                hstQuery.setFilter(filter);
            


            HstQueryResult result = hstQuery.execute();

            HippoBeanIterator iterator = result.getHippoBeans();
           
            if (iterator.getSize()>0) {
                logger.debug("encontrado ApiContainer");
                apiContainer = (ApiContainerItem) iterator.next();
                apiContainerExpandDTO=new ApiContainerExpandDTO(apiContainer,siteName);

            }
            
            
            else { //No se ha encontrado en ese idioma. Se busca el de por defecto. 
            	//si el idioma por defecto coincide con el del scope, pues no hay contenido a devolver
            	//Si no coincide->se devuelve el contenido del idioma por defecto
            	            	
            	//locale
            	Locale locale=new Locale();
            	String localeContext=locale.getLocaleContext(requestContext,"api_containers");
              
                
                //Recupero el contenido SantanderGlobalConfiguration
                GlobalConfiguration globalConfiguration=new GlobalConfiguration();
                String defaultLocale=globalConfiguration.getConfigurationById("DEFAULT_LANGUAGE_"+siteName,requestContext);
                logger.debug("*****VALORRRRR defaultLocale:"+defaultLocale);
                
                
	            if (!localeContext.equals(defaultLocale)) {
	             	HippoBeanIterator apisContainersIterator = HippoUtils.getObjectTypeDefaultLanguaje(requestContext,"ApiContainerItem",defaultLocale,uuid,hstQueryManager);	  
     	            if (apisContainersIterator!=null && apisContainersIterator.getSize()>0) { //encontrado en ese idioma
         	                logger.debug("*****encontrado ApiContainer del generico en el idioma por defecto");
         	                apiContainer = (ApiContainerItem) apisContainersIterator.next();
         	                apiContainerExpandDTO=new ApiContainerExpandDTO(apiContainer, siteName);
         	
         	        }
         	            
         	        else {
         	            	logger.debug("*****NO encontrado ApiContainer en el idioma por defecto");
         	        }
               }
            } 	
          }
            
            ApiContainerDetailDTO apiContainerDetail = new ApiContainerDetailDTO();
            apiContainerDetail.setApiContainer(apiContainerExpandDTO);


            //Get Security Content for all ApisItems of all ApiContainers
           /* String htmlSecurity="";
            htmlSecurity = setHtmlSecurity(requestContext, scope, htmlSecurity);
            apiContainerDetail.setSecurityApis(htmlSecurity);*/

            content.put("content",apiContainerDetail);
            return content;


        }
        catch (Exception e) {
        	logger.error("ERROR ApiContainerPage:"+e.getMessage());
        	e.printStackTrace();
            throw new WebApplicationException(e);
        }
       
    }


	/*private String setHtmlSecurity(HstRequestContext requestContext, HippoBean scope, String htmlSecurity) {
		try {
		    HstQueryManager hstQueryManager2 = getHstQueryManager(requestContext);
		    HstQuery hstQuerySecurity = hstQueryManager2.createQuery(scope, Security.class, true);

		    HstQueryResult resultSecurity = hstQuerySecurity.execute();
		    HippoBeanIterator securities = resultSecurity.getHippoBeans();
		    GenericDAOResource genericDAOresource=new GenericDAOResource();

		    Security security=null;
		    if (securities.getSize()>0) {
		        security = (Security) securities.next();
		        htmlSecurity=genericDAOresource.rewriteHstRichContent(security.getHtml().getContent(),
		        		security.getHtml().getNode(),requestContext.getResolvedMount().getMount());
		    }

		}catch (Exception e) {
		    logger.error("ERROR:",e);

		}
		return htmlSecurity;
	}*/


    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM,
            MediaType.APPLICATION_FORM_URLENCODED})

    @ApiOperation(value = "Returns the list of Apis Containers of the information page.",
            response = ApiContainerDTO.class, authorizations = @Authorization(value = "clientCredentials",
            scopes = @AuthorizationScope(scope = "read_apicontainers", description = "")))
    @ApiResponses({@ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "The specified resource could not be found."),
            @ApiResponse(code = 500,
                    message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value",
                    response = ApiContainerDTO.class),

    })
    @GET
    public Map<String,ApiContainersDTO> getApiContainers(@Context UriInfo uriInfo) {

        //Devuelve un conjunto de ApiContainers "No Deprecados"
        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        Map<String, ApiContainersDTO> content = new HashMap<>();

        List<String> types = queryParams.get("type");
        String type = Objects.nonNull(types) && !types.isEmpty() ? types.get(0) : null;

        List<String> businessAreas = queryParams.get("businessArea");
        String businessArea = Objects.nonNull(businessAreas) && !businessAreas.isEmpty() ? businessAreas.get(0) : null;

        String businessDomain = extractBusinessDomain(queryParams);

        String serviceDomain = extractServiceDomains(queryParams);

        List<String> entities = queryParams.get("entity");
        String entity = Objects.nonNull(entities) && !entities.isEmpty() ? entities.get(0) : null;

        List<String> limits = queryParams.get("_limit");
        int limit = Objects.nonNull(limits) && !limits.isEmpty() ? Integer.parseInt(limits.get(0)) : 0;

        int offset = extractOffsets(queryParams);

        String site = extractSites(queryParams);

        logger.debug("Invoking /api_containers with type: {}" , type);
        logger.debug("Invoking /api_containers with businessArea: {}" , businessArea);
        logger.debug("Invoking /api_containers with businessDomain: {}" , businessDomain);
        logger.debug("Invoking /api_containers with serviceDomain: {}" , serviceDomain);
        logger.debug("Invoking /api_containers with entity: {}" , entity);
        logger.debug("Invoking /api_containers with limit: {}", limit);
        logger.debug("Invoking /api_containers with offset: {}", offset);
        logger.debug("Invoking /api_containers with site: {}", site);

        addApiContainersInfo(content, type, businessArea, businessDomain, serviceDomain, entity, limit, offset);
        return content;
    }


	private int extractOffsets(MultivaluedMap<String, String> queryParams) {
		List<String> offsets = queryParams.get("_offset");
        int offset =
                Objects.nonNull(offsets) && !offsets.isEmpty() ? Integer.parseInt(offsets.get(0)) : -1;
		return offset;
	}


	private String extractSites(MultivaluedMap<String, String> queryParams) {
		List<String> sites = queryParams.get("site");
        String site = Objects.nonNull(sites) && !sites.isEmpty() ? sites.get(0) : null;
		return site;
	}


	private String extractBusinessDomain(MultivaluedMap<String, String> queryParams) {
		List<String> businessDomains = queryParams.get("businessDomain");
        String businessDomain = Objects.nonNull(businessDomains) && !businessDomains.isEmpty() 
        		? businessDomains.get(0) : null;
		return businessDomain;
	}


	private String extractServiceDomains(MultivaluedMap<String, String> queryParams) {
		List<String> serviceDomains = queryParams.get("serviceDomain");
        String serviceDomain = Objects.nonNull(serviceDomains) && !serviceDomains.isEmpty() 
        		? serviceDomains.get(0) : null;
		return serviceDomain;
	}


	protected void addApiContainersInfo(Map<String, ApiContainersDTO> content, String type, String businessArea,
										String businessDomain, String serviceDomain, String entity, int limit, int offset) {
		try {

            HstRequestContext requestContext = RequestContextProvider.get();

            String contextPath = requestContext.getBaseURL().getContextPath();
            String requestpath = requestContext.getBaseURL().getRequestPath();
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            HippoBean scope = getMountContentBaseBean(requestContext);
            
          //Nombre del Site
			String scopePath=scope.getPath();
            String[] splitSite =scopePath.split(Constants.PROJECT_ROOT_PATH);
			
			String siteName="";
            if (splitSite.length > 1) {
            	siteName=splitSite[1].substring(0,splitSite[1].indexOf("/"));
            	logger.debug("*****siteName:"+siteName);
            }
            
            

            logger.debug("scope path: {}" , scope.getPath());

				HstQuery hstQuery=hstQueryManager.createQuery(scope, ApiContainerItem.class,true);

            Filter filter = hstQuery.createFilter();
            filter.addEqualTo("santanderbrxm:deprecated","No");

            addToFilter(type, businessArea, businessDomain, serviceDomain, entity, filter);


            hstQuery.setFilter(filter);


            //Número total antes de filtrar por páginacion
            HstQueryResult result = hstQuery.execute();

            int count = result.getTotalSize();
            logger.debug("count: {}", count);


            if (limit >= 1 && offset >= 0) { //si hay que filtrar por páginacion, se ejecuta de nuevo la query
                hstQuery.setLimit(limit);
                hstQuery.setOffset(offset);
                result = hstQuery.execute();
            }

            HippoBeanIterator iterator = result.getHippoBeans();
            List<ApiContainerDTO> apiContainers = new ArrayList<>();
            ApiContainerItem apiContainer=null;
            while (iterator.hasNext()) {
                apiContainer = (ApiContainerItem) iterator.nextHippoBean();
                apiContainers.add(new ApiContainerDTO(apiContainer,siteName));

            }

            List<HALLinkDTO> links = new ArrayList<>();


            addLinks(type, businessArea, businessDomain, serviceDomain, limit, offset, requestContext, contextPath,
					requestpath, result, links);

            content.put("content",ApiContainersDTO.builder()._embedded(apiContainers)._count(count)
            		._links(links).build());
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
	}


	private void addToFilter(String type, String businessArea, String businessDomain, String serviceDomain,
			String entity, Filter filter) throws FilterException {
		if (Objects.nonNull(type) && !type.isEmpty()) {
		    filter.addEqualTo("santanderbrxm:type", type);

		}
		addEntityKeys(businessArea, filter);
		addEntityKeys(businessDomain, filter);
		addEntityKeys(serviceDomain, filter);
		addEntityKeys(entity, filter);
	}


	private void addEntityKeys(String entity, Filter filter) throws FilterException {
		if (Objects.nonNull(entity) && !entity.isEmpty()) {
		    //Taxonomia
		    filter.addContains(HIPPOTAXONOMY_KEYS,entity );
		}
	}


	private void addLinks(String type, String businessArea, String businessDomain, String serviceDomain, int limit,
			int offset, HstRequestContext requestContext, String contextPath, String requestpath, HstQueryResult result,
			List<HALLinkDTO> links) {
		if (offset >= 0 && offset <= result.getTotalSize() && limit > 0) 
		{
		    String requestURL =
		            getRequestURL(requestContext.getServletRequest().getRequestURL().toString());
		    String localPath = requestURL + contextPath + requestpath;


		    localPath = setLocalPath(type, businessArea, businessDomain, serviceDomain, localPath);


		    // First
		    links.add(HALLinkDTO.builder().rel("First")
		            .href(localPath + LIMIT + limit + "&_offset=0").build());
		    // Previous
		    if (offset != 0 && (offset - limit) >= 0) {
		        links.add(HALLinkDTO.builder().rel("Previous")
		                .href(localPath + LIMIT + limit + OFFSET + (offset - limit)).build());
		    } else if (offset - limit < 0 && offset != 0) {
		        links.add(HALLinkDTO.builder().rel("Previous")
		                .href(localPath + LIMIT + offset + "&_offset=0").build());
		    } else
		    {
		    	//do nothing
		    }

		    // Current
		    links.add(HALLinkDTO.builder().rel("Current")
		            .href(localPath + LIMIT + limit + OFFSET + offset).build());
		    // Next
		    if (offset + limit < result.getTotalSize()) {
		        links.add(HALLinkDTO.builder().rel("Next")
		                .href(localPath + LIMIT + limit + OFFSET + (offset + limit)).build());
		    }
		    // Last
		    links.add(HALLinkDTO.builder().rel("Last")
		            .href(localPath + LIMIT + limit + OFFSET + (result.getTotalSize() - 1))
		            .build());

		}
	}


	private String setLocalPath(String type, String businessArea, String businessDomain, String serviceDomain,
			String localPath) {
		if (Objects.nonNull(type) && !type.isEmpty()) {
		    localPath = localPath + "?type=" + type + "&";
		}

		else {
		    localPath = localPath + "?";
		}

		if (Objects.nonNull(businessArea) && !businessArea.isEmpty()) {
		    localPath = localPath + "businessArea=" + businessArea + "&";
		}
		if (Objects.nonNull(businessDomain) && !businessDomain.isEmpty()) {
		    localPath = localPath + "businessDomain=" + businessDomain + "&";
		}
		if (Objects.nonNull(serviceDomain) && !serviceDomain.isEmpty()) {
		    localPath = localPath + "serviceDomain=" + serviceDomain + "&";
		}
		return localPath;
	}


    private static String getRequestURL(String url) {
        String aux = url;
        int index = 0;
        int cont = 0;
        for (int i = 0; i < URL_INDEX; i++) {
            index = aux.indexOf('/');
            aux = aux.substring(index + 1, aux.length());
            cont = cont + index + 1;
        }
        return url.substring(0, cont - 1);
    }


}
