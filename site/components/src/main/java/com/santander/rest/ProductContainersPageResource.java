package com.santander.rest;

import java.util.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.santander.beans.*;
import com.santander.dto.*;
import com.santander.utils.*;
import com.santander.utils.Locale;
import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.query.filter.Filter;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.onehippo.cms7.essentials.components.rest.BaseRestResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;
import javax.validation.constraints.NotNull;

import org.hippoecm.hst.util.PathUtils;
import javax.jcr.Node;

import static com.santander.utils.Constants.*;

@Path("/product_containers")
@Api(value = "product Containers", description = "ProductContainers REST service")
public class ProductContainersPageResource extends BaseRestResource {

    private static Logger logger = LoggerFactory.getLogger(ProductContainersPageResource.class);

    @Produces({
            MediaType.APPLICATION_JSON
    })
    @Consumes({
            MediaType.APPLICATION_JSON,
            MediaType.APPLICATION_OCTET_STREAM,
            MediaType.APPLICATION_FORM_URLENCODED
    })
    @ApiOperation(value = "Returns Product Containers Detail",
            response = ProductContainerDetailDTO.class, authorizations = @Authorization(value = "clientCredentials",
            scopes = @AuthorizationScope(scope = "read_productcontainers", description = "")))
    @ApiResponses({
            @ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "nThe specified resource could not be found."),
            @ApiResponse(code = 500,
                    message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value",
                    response = ProductContainerDetailDTO.class),



    })
    @GET
    @Path("/{uuid}/")
    public Map < String, ProductContainerDetailDTO > getProductContainersByID(@Context HttpServletRequest servletRequest,
                                                                              @Context HttpServletResponse servletResponse,
                                                                              @Context UriInfo uriInfo,
                                                                              @NotNull @PathParam("uuid") String uuid) {

        logger.debug("Invoking /product_containers/{uuid} : {}", uuid);
        Map < String, ProductContainerDetailDTO > content = new HashMap < > ();
        try {
            HstRequestContext requestContext = RequestContextProvider.get();
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            HippoBean scope = getMountContentBaseBean(requestContext);
            HstQuery hstQuery = hstQueryManager.createQuery(scope, ProductContainerItem.class, true);


            //Nombre del Site
            String scopePath = scope.getPath();
            String[] splitSite = scopePath.split(Constants.PROJECT_ROOT_PATH);

            String siteName = "";
            if (splitSite.length > 1) {
                siteName = splitSite[1].substring(0, splitSite[1].indexOf("/"));
                logger.debug("*****siteName:" + siteName);
            }

            // Se va a buscar el ProductContainer con ese UUID o alias en el idioma del site (scope).
            //Si no existe en ese idioma, se busca en el idioma por defecto, que está metido en el tipo de contenido "SantanderGlobalConfiguration"
            ProductContainerItem productContainerItem = null;
            ProductContainerExpandDTO productContainerExpandDTO = null;

            if (!uuid.isEmpty()) {
                logger.debug("*****uuid:" + uuid);
                Filter filter = hstQuery.createFilter();
                Filter filterAlias = hstQuery.createFilter();
                Filter filterUuid = hstQuery.createFilter();
                filterAlias.addEqualTo("santanderbrxm:alias", uuid);
                filterUuid.addEqualTo("jcr:uuid", uuid);
                filter.addOrFilter(filterAlias);
                filter.addOrFilter(filterUuid);
                hstQuery.setFilter(filter);

                HstQueryResult result = hstQuery.execute();
                HippoBeanIterator iterator = result.getHippoBeans();

                if (iterator.getSize() > 0) { //encontrado en ese idioma
                    logger.debug("*****PC encontrado ProductContainer");
                    productContainerItem = (ProductContainerItem) iterator.next();
                    productContainerExpandDTO = new ProductContainerExpandDTO(productContainerItem, siteName);

                } else { //No se ha encontrado en ese idioma. Se busca el de por defecto.
                    //si el idioma por defecto coincide con el del scope, pues no hay contenido a devolver
                    //Si no coincide->se devuelve el contenido del idioma por defecto
                    logger.debug("*****PC NO encontrado ProductContainer en ese scope");

                    //locale
                    Locale locale = new Locale();
                    String localeContext = locale.getLocaleContext(requestContext, "product_containers");
                    logger.debug("*****localeContext:" + localeContext);


                    //Recupero el contenido SantanderGlobalConfiguration
                    GlobalConfiguration globalConfiguration = new GlobalConfiguration();
                    String defaultLocale = globalConfiguration.getConfigurationById("DEFAULT_LANGUAGE_" + siteName, requestContext);
                    logger.debug("*****defaultLocale:" + defaultLocale);


                    if (!localeContext.equals(defaultLocale)) {
                        logger.debug("*****Busco el contenido en el idioma por defecto:" + defaultLocale);
                        HippoBeanIterator productsContainersIterator = HippoUtils.getObjectTypeDefaultLanguaje(requestContext, "ProductContainerItem", defaultLocale, uuid, hstQueryManager);
                        if (productsContainersIterator != null && productsContainersIterator.getSize() > 0) { //encontrado en ese idioma
                            logger.debug("*****encontrado ProductContainer del generico en el idioma por defecto");
                            productContainerItem = (ProductContainerItem) productsContainersIterator.next();
                            productContainerExpandDTO = new ProductContainerExpandDTO(productContainerItem, siteName);
                        } else {
                            logger.debug("*****NO encontrado ProductContainer en el idioma por defecto");
                        }
                    }
                }
            }


            ProductContainerDetailDTO productContainerDetail = new ProductContainerDetailDTO();
            productContainerDetail.setProductContainer(productContainerExpandDTO);
            content.put("content", productContainerDetail);

            return content;

        } catch (Exception e) {
            logger.error("ERROR ProductContainerPage:" + e.getMessage());
            e.printStackTrace();
            throw new WebApplicationException(e);
        }

    }

    @Produces({
            MediaType.APPLICATION_JSON
    })
    @Consumes({
            MediaType.APPLICATION_JSON,
            MediaType.APPLICATION_OCTET_STREAM,
            MediaType.APPLICATION_FORM_URLENCODED
    })
    @ApiOperation(value = "Returns Product Containers",
            response = ProductContainerInfoBasicDTO.class, authorizations = @Authorization(value = "clientCredentials",
            scopes = @AuthorizationScope(scope = "read_productcontainers", description = "")))
    @ApiResponses({
            @ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "The specified resource could not be found."),
            @ApiResponse(code = 500,
                    message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value",
                    response = ProductContainerInfoBasicDTO.class),
    })

    @GET
    public Map < String, ProductContainersInfoBasicDTO > getProductContainers(@Context HttpServletRequest servletRequest,
                                                                              @Context HttpServletResponse servletResponse,
                                                                              @Context UriInfo uriInfo) {


        //Devuelve un conjunto de ProductContainers "No Deprecados" y un listado de Documentos de tipo GetStarted
        logger.debug("******Invoking /product_containers");
        MultivaluedMap < String, String > queryParams = uriInfo.getQueryParameters();
        Map < String, ProductContainersInfoBasicDTO > content = new HashMap < > ();

        try {

            HstRequestContext requestContext = RequestContextProvider.get();
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            HippoBean scope = getMountContentBaseBean(requestContext);

            HstQuery hstQuery = hstQueryManager.createQuery(scope, ProductContainerItem.class, true);


            Filter filter = hstQuery.createFilter();
            filter.addEqualTo("santanderbrxm:deprecated", "No");
            hstQuery.setFilter(filter);

            HstQueryResult result = hstQuery.execute();


            HippoBeanIterator iterator = result.getHippoBeans();
            List<ProductContainerInfoBasicDTO> productContainers = new ArrayList<>();
            ProductContainerItem productContainer = null;
            while (iterator.hasNext()) {
                productContainer = (ProductContainerItem) iterator.nextHippoBean();
                productContainers.add(new ProductContainerInfoBasicDTO(productContainer));
            }

            //NUEVO. RECUPERO LOS DOCUMENTOS DE TIPO GUIDE
            String requestpath = requestContext.getBaseURL().getRequestPath();
            String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
            Node mountContentNode = requestContext.getSession().getRootNode().getNode(PathUtils.normalizePath(mountContentPath));

            // IDIOMA
            String[] splitSite = requestpath.split("/product_containers");
            logger.debug("splitSite0 : {}", splitSite[0]);
            logger.debug("splitSiteLenght : {}", splitSite.length);

            String locale = setLocale(splitSite);
            if (locale.contains("es")) {
                locale = "es";
            }




            HstQuery hstQueryDocuments = hstQueryManager.createQuery(mountContentNode, Documentation.class);
            Filter filterDocument = hstQueryDocuments.createFilter();
            filterDocument.addEqualTo(HIPPOTRANSLATION_LOCALE_PROPERTY, locale);
            filterDocument.addEqualTo(PROPERTY_SANTANDERBRXM_TYPE, GUIDE_TYPE);
            hstQueryDocuments.setFilter(filterDocument);

            HstQueryResult resultDocument = hstQueryDocuments.execute();

            HippoBeanIterator iterator2 = resultDocument.getHippoBeans();

            List<DocumentationProductDTO> listDocumento = new ArrayList<>();
            while (iterator2.hasNext()) {
                Documentation documentation = (Documentation) iterator2.nextHippoBean();

                DocumentationProductDTO documentationDTO = new DocumentationProductDTO();
                documentationDTO.setId(InternalLinkProcessor.getAliasByUuid(documentation.getCanonicalUUID()));
                documentationDTO.setTitle(documentation.getTitle());
                Map<String, String> urlAttrs =
                        HippoUtils.getDocumentAttrsByURLCustom(documentation);

                documentationDTO.setChannel(urlAttrs.get("channel"));
                documentationDTO.setLanguage(urlAttrs.get("locale"));

                listDocumento.add(documentationDTO);
            }

            //AÑADO LOS PRODUCT Y LOS DOCUMENTOS AL OBJETO DE SALIDA
            content.put("content", ProductContainersInfoBasicDTO.builder().productContainers(productContainers).guides(listDocumento).build());

        } catch (Exception e) {

            throw new WebApplicationException(e);
        }

        return content;

    }

    private String setLocale(String[] splitSite) {
        String locale = "";

        if (splitSite.length >= 1) {
            String beforeSite = splitSite[0];
            locale = beforeSite.substring(beforeSite.lastIndexOf('/') + 1); // COGE DESDE LA ULTIMA BARRA
            // HASTA EL FINAL
            locale = locale.toLowerCase().replace("_", "-");
            logger.debug("locale : {}", locale);
        }
        return locale;
    }

}