package com.santander.rest;

import static com.santander.utils.Constants.HIPPOTRANSLATION_LOCALE_PROPERTY;
import static com.santander.utils.Constants.PROPERTY_SANTANDERBRXM_DATE;
import static com.santander.utils.Constants.PROPERTY_SANTANDERBRXM_TYPE;
import static com.santander.utils.Constants.PROPERTY_SANTANDERBRXM_TYPE_ARTICLE;
import static com.santander.utils.Constants.PROPERTY_SANTANDERBRXM_TYPE_WEBINAR;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;

import com.santander.dto.*;
import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.query.exceptions.QueryException;
import org.hippoecm.hst.content.beans.query.filter.Filter;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.hst.jaxrs.services.AbstractResource;
import org.hippoecm.hst.util.PathUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.santander.beans.Documentation;
import com.santander.beans.NewsAndArticles;
import com.santander.beans.SectionLevelFour;
import com.santander.beans.SectionLevelOne;
import com.santander.beans.SectionLevelThree;
import com.santander.beans.SectionLevelTwo;
import com.santander.utils.Constants;
import com.santander.utils.GlobalConfiguration;
import com.santander.utils.HippoUtils;
import com.santander.utils.InternalLinkProcessor;
import com.santander.utils.Locale;

import io.swagger.annotations.Api;

@Produces({MediaType.APPLICATION_JSON})
@Path("/documentations/")
@Api(value = "Documentation", description = "Documentation Page REST Service")
public class DocumentationPageResource extends AbstractResource {

    private static Logger logger = LoggerFactory.getLogger(DocumentationPageResource.class);
    
    //NUEVO METODO QUE DEVUELVA LOS DOCUMENTATIONS DE UN TIPO (GetStarted,Guides,Documentation) ( CON EL ID Y EL ALIAS Y Title TENDRIA QUE SER SUFICIENTE)
   
    
    @GET
    public Map<String, DocumentationsDTO> getDocumentations(@Context UriInfo uriInfo) {

        MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
        Map<String, DocumentationsDTO> content = new HashMap<>();

        String type = setTypes(queryParams);
        String site = setSite(queryParams);

        logger.debug("Invoking /informations with type: {}", type);
        logger.debug("Invoking /informations with site: {}", site);

        try {

           HstRequestContext requestContext = RequestContextProvider.get();

            String contextPath = requestContext.getBaseURL().getContextPath();
            String requestpath = requestContext.getBaseURL().getRequestPath();
            String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
            Node mountContentNode = requestContext.getSession().getRootNode().getNode(PathUtils.normalizePath(mountContentPath));
          
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext.getSession(), requestContext);
         
           

            // IDIOMA
            String[] splitSite = requestpath.split("/documentations");
            logger.debug("splitSite0 : {}", splitSite[0]);
            logger.debug("splitSiteLenght : {}", splitSite.length);
            String locale = setLocale(splitSite);



            
            HstQuery hstQuery = hstQueryManager.createQuery(mountContentNode, Documentation.class);
            Filter filter = hstQuery.createFilter();
            filter.addEqualTo(HIPPOTRANSLATION_LOCALE_PROPERTY, locale);
            filter.addEqualTo(PROPERTY_SANTANDERBRXM_TYPE, type);
            hstQuery.setFilter(filter);

            
            HstQueryResult result = hstQuery.execute();
           
           
            HippoBeanIterator iterator = result.getHippoBeans();
            DocumentationDTO documentationDTO = new DocumentationDTO();
            List<DocumentationDTO> documentations = new ArrayList<>();
            while (iterator.hasNext()) {
            	Documentation documentation = (Documentation) iterator.nextHippoBean();
                documentationDTO.setId(InternalLinkProcessor.getAliasByUuid(documentation.getCanonicalUUID()));
                documentationDTO.setTitle(documentation.getTitle());
                documentations.add(documentationDTO);
            }

            content.put("content",
                    DocumentationsDTO.builder().documentations(documentations).build());
        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
        return content;
    }
    
    

    @GET
    @Path("/{uuid}")
    public Map<String, DocumentationDTO> getDocumentationPage(@Context HttpServletRequest servletRequest
    		, @Context HttpServletResponse servletResponse, @Context UriInfo ufiInfo, 
    		@NotNull @PathParam("uuid") String uuid) {
        Map<String, DocumentationDTO> content = new HashMap<>();

        try {
        	GenericDAOResource genericDAOResource = new GenericDAOResource();
        	
	        HstRequestContext requestContext = RequestContextProvider.get();
        	HstQueryManager hstQueryManager = getHstQueryManager(requestContext.getSession(), requestContext);
        	String mountContentPath = requestContext.getResolvedMount().getMount().getContentPath();
            Node mountContentNode = requestContext.getSession().getRootNode().getNode(PathUtils.normalizePath(mountContentPath));
            HstQuery hstQuery = hstQueryManager.createQuery(mountContentNode, Documentation.class);
            
          //Nombre del Site
            HippoBean scope = getMountContentBaseBean(requestContext);
			String scopePath=scope.getPath();
            String[] splitSite =scopePath.split(Constants.PROJECT_ROOT_PATH);
			
			String siteName="";
            if (splitSite.length > 1) {
            	siteName=splitSite[1].substring(0,splitSite[1].indexOf("/"));
            	logger.debug("*****siteName:"+siteName);
            }
            
            DocumentationDTO documentationDTO = new DocumentationDTO();

            if (!uuid.isEmpty()) {
                Filter filter = hstQuery.createFilter();
                Filter filterAlias = hstQuery.createFilter(); 
                Filter filterUuid = hstQuery.createFilter();
                filterAlias.addEqualTo("santanderbrxm:alias", uuid);
                filterUuid.addEqualTo("jcr:uuid", uuid);
                filter.addOrFilter(filterAlias);
                filter.addOrFilter(filterUuid);
                hstQuery.setFilter(filter);
            	
            	HstQueryResult result = hstQuery.execute();
		        
		        HippoBeanIterator iterator = result.getHippoBeans();
		        if (iterator.getSize()>0) { //encontrado en ese idioma
		                Documentation documentation = (Documentation) iterator.nextHippoBean();
		                documentationDTO.setId(InternalLinkProcessor.getAliasByUuid(uuid));
		                documentationDTO.setTitle(documentation.getTitle());
		                documentationDTO.setDescription
		                	(Objects.nonNull(documentation.getDescription()) ? 
		                			genericDAOResource.rewriteHstRichContent(documentation.getDescription().getContent(), 
		                					documentation.getDescription().getNode(), 
		                					requestContext.getResolvedMount().getMount())
		                        : StringUtils.EMPTY);
						documentationDTO.setType(documentation.getType());
		                documentationDTO.setLinks(genericDAOResource.processlinksAndDownloads(documentation.getLinks()));

						if(Objects.nonNull(documentation.getType()) && !documentation.getType().isEmpty() && !documentation.getType().contains(Constants.DOCUMENTATION_TYPE)){
							//Obtenemos el menudDocumentation
							MenuDocumentationDTO menuDocumentation = getDocumentationMenu(requestContext, documentation);

							documentationDTO.setMenuDocumentation(menuDocumentation);
						}

		                if (Objects.nonNull(documentation.getPage())) {
		                    List<PageDTO> pages = getDocumentationPages(requestContext, genericDAOResource, documentation,requestContext.getResolvedMount().getMount());
		                    documentationDTO.setPages(pages);
		                }
		         }
		        
		        else { //No se ha encontrado en ese idioma. Se busca el de por defecto. 
	            	//si el idioma por defecto coincide con el del scope, pues no hay contenido a devolver
	            	//Si no coincide->se devuelve el contenido del idioma por defecto
	            	logger.debug("*****NO encontrado ProductContainer en ese scope");
	            	
	            	//locale
	            	Locale locale=new Locale();
	            	String localeContext=locale.getLocaleContext(requestContext,"documentations");
	               
	                //Recupero el contenido SantanderGlobalConfiguration
	                GlobalConfiguration globalConfiguration=new GlobalConfiguration();
	                String defaultLocale=globalConfiguration.getConfigurationById("DEFAULT_LANGUAGE_"+siteName,requestContext);
	
	                if (!localeContext.equals(defaultLocale)) {
		            	HippoBeanIterator documentationIterator = HippoUtils.getObjectTypeDefaultLanguaje(requestContext,"Documentation",defaultLocale,uuid,hstQueryManager);	  
         	            if (documentationIterator!=null && documentationIterator.getSize()>0) { //encontrado en ese idioma
         	            	Documentation documentation = (Documentation) documentationIterator.nextHippoBean();
         	            	documentationDTO.setId(InternalLinkProcessor.getAliasByUuid(uuid));
    		                documentationDTO.setTitle(documentation.getTitle());
    		                
    		                Mount mountPoint=requestContext.getResolvedMount().getMount();
    		                try {
    		                	Map<String, String> urlAttrs =
    		                              HippoUtils.getDocumentAttrsByURLCustom(requestContext, documentation);
    		                	
    		     		        String localeContent=documentation.getLocaleString().replace("-", "_");
    		     		        logger.debug("*****locale Contenido:"+localeContent);
    		     		       
    		     		        String mountRequestPath = requestContext.getResolvedMount().getMount().getContentPath();
    		     		        String localeMountRequestPath=mountRequestPath.substring(mountRequestPath.lastIndexOf("/")+1);
    		     		        logger.debug("*****locale request:"+localeMountRequestPath);
    		     		         
    		     		       
    		     		        if (!localeContent.equals(localeMountRequestPath)){ //Si el lenguaje del contenido no se corresponde con el de la petición, busco el Mount del contenido
    		     		          	 mountPoint=requestContext.getMount(urlAttrs.get("channel")+"-"+localeContent); //coge el Mount con el atributo alias=superdiginal-es_es
    		     		         	logger.debug("*****mountPoint:"+mountPoint);
    		     		         	 
    		     		         }
    		     		        
    		                	
    		                	documentationDTO.setDescription
    		                	(Objects.nonNull(documentation.getDescription()) ? 
    		                			genericDAOResource.rewriteHstRichContent(documentation.getDescription().getContent(), 
    		                					documentation.getDescription().getNode(), 
    		                					mountPoint)
    		                        : StringUtils.EMPTY);
    		                }
    		                catch(Exception e)
    		                {
    		                	documentationDTO.setDescription(StringUtils.EMPTY);
    		                }
    		                
    		                documentationDTO.setLinks(genericDAOResource.processlinksAndDownloads(documentation.getLinks()));
    		
    		                if (Objects.nonNull(documentation.getPage())) {
    		                    List<PageDTO> pages = getDocumentationPages(requestContext, genericDAOResource, documentation,mountPoint);
    		                    documentationDTO.setPages(pages);
    		                }
	         	
	         	        }
	         	            
	         	        else {
	         	            	logger.debug("*****N encontrado documentation en el idioma por defecto");
	         	        }
	               }
	            } 	
		        
        }       
		      


        content.put("content", documentationDTO);
        return content;
    }
    catch (Exception e) {
    	logger.error("ERROR DocumentationPage:"+e.getMessage());
    	e.printStackTrace();
        throw new WebApplicationException(e);
    }
   
}

	private MenuDocumentationDTO getDocumentationMenu(HstRequestContext requestContext, Documentation documentation) {
		MenuDocumentationDTO menuDocumentationDTO = new MenuDocumentationDTO();

		try {
			List<SubMenuDocumentionLevelOneDTO> subMenuDocumentionLevelOneDTOList = new ArrayList<>();
			SubMenuDocumentionLevelOneDTO subMenuDocumentionLevelOneDTO = new SubMenuDocumentionLevelOneDTO();
			
			List<DocumentationLinkDTO> documentationLinkDTOList = new ArrayList<>();

			subMenuDocumentionLevelOneDTO.setTitle(documentation.getType());
			subMenuDocumentionLevelOneDTO.setDescription("");

			//DOCUMENTS
			Map<String, String> urlAttrs =
					HippoUtils.getDocumentAttrsByURLCustom(requestContext, documentation);

			Query query = requestContext.getSession().getWorkspace().getQueryManager().createQuery(
					"/jcr:root/content/documents/santander/" +
							"santander-"+urlAttrs.get("channel")+"/"+urlAttrs.get("locale").toLowerCase()+"/content//element(*, santanderbrxm:documentation) "
							+ "[@santanderbrxm:type ='" + documentation.getType() + "' ] ", Query.XPATH);

			QueryResult queryResult = query.execute();

			for (NodeIterator nodeIterator = queryResult.getNodes(); nodeIterator.hasNext(); )
			{
				InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
				DocumentationLinkDTO documentationLinkDTO = new DocumentationLinkDTO();
				Node node = nodeIterator.nextNode();

				//TITLE
				String title = node.getProperty(Constants.SANTANDERBX_TITLE).getString();
				documentationLinkDTO.setTitle(title);

				//INTERNAL LINK
				String url =requestContext.getHstLinkCreator().create(node, requestContext)
						.toUrlForm(requestContext, true);
				String uuid=HippoUtils.getNodeId(node.getPath(), requestContext);
				String type = node.getPrimaryNodeType().getName();
				documentationLinkDTO.setInternalLink(internalLinkProcessor.generateInternalLink( url, type, uuid));

				documentationLinkDTOList.add(documentationLinkDTO);
			}

			subMenuDocumentionLevelOneDTO.setDocuments(documentationLinkDTOList);

			subMenuDocumentionLevelOneDTOList.add(subMenuDocumentionLevelOneDTO);

			menuDocumentationDTO.setDocumentsGroups(subMenuDocumentionLevelOneDTOList);

		} catch (RepositoryException | QueryException e) {
			logger.error("Error getDocumentationMenu", e);
		}


		return menuDocumentationDTO;
	}

	@org.jetbrains.annotations.NotNull
    private List<PageDTO> getDocumentationPages(HstRequestContext requestContext, 
    		GenericDAOResource genericDAOResource, Documentation documentation, Mount mountPoint) {
        List<PageDTO> pages = new ArrayList<>();
        for (SectionLevelOne level1 : documentation.getPage()) {
            PageDTO page = new PageDTO();
            page.setTitle(level1.getTitle());
            page.setRichContent(
                    Objects.nonNull(level1.getDescription()) 
                    ? genericDAOResource.rewriteHstRichContent(level1
                    		.getDescription().getContent()
                    		,level1.getDescription().getNode()
                    		,mountPoint) 
                    		: StringUtils.EMPTY);
            setSectionDocuments(page, level1.getDocumentation(), requestContext);
            List<SectionDTO> sections = new ArrayList<>();
            for (SectionLevelTwo level2 : level1.getSections()) {
                SectionDTO section = new SectionDTO();
                section.setTitle(level2.getTitle());
                section.setRichContent(Objects.nonNull(level2.getDescription()) ? 
                		genericDAOResource.rewriteHstRichContent(level2.getDescription()
                				.getContent(), level2.getDescription()
                				.getNode(), mountPoint) 
                		: StringUtils.EMPTY);
                setSectionDocuments(section, level2.getDocumentation(), requestContext);
                List<SectionDTO> sections2 = new ArrayList<>();
                /*for (SectionLevelThree level3 : level2.getSections()) {
                    populateSectionLevelThreeList(requestContext, genericDAOResource, sections2, level3, mountPoint);
                }+*/
                
                //NUEVO
                for (SectionLevelThree level3 : level2.getSections()) {
                    SectionDTO section2 = new SectionDTO();
                    section2.setTitle(level3.getText());
                    section2.setRichContent(Objects.nonNull(level3.getHtml()) ? 
                    		genericDAOResource.rewriteHstRichContent(level3.getHtml()
                    				.getContent(), level3.getHtml()
                    				.getNode(), mountPoint) 
                    		: StringUtils.EMPTY);
                    setSectionDocuments(section2, level3.getDocumentation(), requestContext);
                    List<SectionDTO> sections3 = new ArrayList<>();
                    for (SectionLevelFour level4 : level3.getSections()) {
                    	populateSectionLevelFourList(requestContext, genericDAOResource, sections3, level4, mountPoint);
                    }
                    section2.setSections(sections3);
                    sections2.add(section2);
                }
                //FIN NUEVO
                
               
                
                section.setSections(sections2);
                sections.add(section);
            }
            page.setSections(sections);
            pages.add(page);

        }
        return pages;
    }

    private void populateSectionLevelThreeList(HstRequestContext requestContext
    		, GenericDAOResource genericDAOResource, List<SectionDTO> sections2, SectionLevelThree level3, Mount mountPoint) {
        SectionDTO section2 = new SectionDTO();
        section2.setTitle(level3.getText());
        section2.setRichContent(
                Objects.nonNull(level3.getHtml()) ? genericDAOResource.rewriteHstRichContent(level3.getHtml()
                		.getContent(), level3.getHtml().getNode(),mountPoint) 
                		: StringUtils.EMPTY);
        setSectionDocuments(section2, level3.getDocumentation(), requestContext);
        sections2.add(section2);
    }

    private void populateSectionLevelFourList(HstRequestContext requestContext
    		, GenericDAOResource genericDAOResource, List<SectionDTO> sections3, SectionLevelFour level4, Mount mountPoint) {
        SectionDTO section3 = new SectionDTO();
        section3.setTitle(level4.getTutle());
        section3.setRichContent(
                Objects.nonNull(level4.getDescription()) ? genericDAOResource.rewriteHstRichContent(level4.getDescription()
                		.getContent(), level4.getDescription().getNode(),mountPoint) 
                		: StringUtils.EMPTY);
        setSectionDocuments(section3, level4.getDocumentation(), requestContext);
        sections3.add(section3);
    }

    
	private static void setSectionDocuments (SectionDTO section,List<HippoBean> documents,HstRequestContext requestContext)
	{
		String santanderTitle = "";
		String santanderTutle = "";

		santanderTitle = "santanderbrxm:title";
		santanderTutle = "santanderbrxm:tutle";

		if (documents==null || documents.isEmpty())
		{
			return;
		}
		
		logger.debug("MACF Setting {} documents to section {}",documents.size(),section.getTitle());
		
		
		try
		{
			List<DocumentationLinkDTO> documentsRelation = new ArrayList<DocumentationLinkDTO>();
			InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
			for (HippoBean document : documents)
			{ 
				String title=null;
				if (document.getNode()!=null && document.getNode().getProperty(santanderTitle)!=null)
				{
					title=document.getNode().getProperty(santanderTitle).getString();
				
				} else if (document.getNode()!=null && document.getNode().getProperty(santanderTutle)!=null)
				{
					title=document.getNode().getProperty(santanderTutle).getString();
				} else
				{
					//nothing
				}
				
				
				String url =requestContext.getHstLinkCreator().create(document, requestContext)
		                .toUrlForm(requestContext, true);
				String uuid=HippoUtils.getNodeId(document.getNode().getPath(), requestContext);
				
				String type = document.getNode().getPrimaryNodeType().getName();


				DocumentationLinkDTO documentationLinkDTO = new DocumentationLinkDTO();
				documentationLinkDTO.setTitle(title);
				documentationLinkDTO.setInternalLink(internalLinkProcessor.generateInternalLink(url, type, uuid));

				documentsRelation.add(documentationLinkDTO);
			}
			
			section.setDocumentsRelation(documentsRelation);
		} catch (Exception e)
		{
			logger.error("Exception retrieving documents",e);
		}
	}

	private static void setSectionDocuments (PageDTO section,List<HippoBean> documents,HstRequestContext requestContext)
	{
		String santanderTitle = "";
		String santanderTutle = "";

		santanderTitle = "santanderbrxm:title";
		santanderTutle = "santanderbrxm:tutle";

		if (documents==null || documents.isEmpty())
		{
			return;
		}
		
		logger.debug("MACF Setting {} documents to section {}",documents.size(),section.getTitle());
		
		
		try
		{
			List<DocumentationLinkDTO> documentsRelation = new ArrayList<DocumentationLinkDTO>();
			InternalLinkProcessor internalLinkProcessor = new InternalLinkProcessor();
			for (HippoBean document : documents)
			{ 
				String title=null;
				if (document.getNode()!=null && document.getNode().getProperty(santanderTitle)!=null)
				{
					title=document.getNode().getProperty(santanderTitle).getString();
				
				} else if (document.getNode()!=null && document.getNode().getProperty(santanderTutle)!=null)
				{
					title=document.getNode().getProperty(santanderTutle).getString();
				} else
				{
					//nothing
				}
				
				
				String url =requestContext.getHstLinkCreator().create(document, requestContext)
		                .toUrlForm(requestContext, true);
				String uuid=HippoUtils.getNodeId(document.getNode().getPath(), requestContext);
				
				String type = document.getNode().getPrimaryNodeType().getName();

				DocumentationLinkDTO documentationLinkDTO = new DocumentationLinkDTO();
				documentationLinkDTO.setTitle(title);
				documentationLinkDTO.setInternalLink(internalLinkProcessor.generateInternalLink(url, type, uuid));

				documentsRelation.add(documentationLinkDTO);
			}
			
			section.setDocumentsRelation(documentsRelation);
		} catch (Exception e)
		{
			logger.error("Exception retrieving documents",e);
		}
	}
    
	private String setLocale(String[] splitSite) {
		String locale = "";

		if (splitSite.length >= 1) {
		    String beforeSite = splitSite[0];
		    locale = beforeSite.substring(beforeSite.lastIndexOf('/') + 1);// COGE DESDE LA ULTIMA BARRA
		    // HASTA EL FINAL
		    locale = locale.toLowerCase().replace("_", "-");
		    logger.debug("locale : {}", locale);
		}
		return locale;
	}

	private String setSite(MultivaluedMap<String, String> queryParams) {
		List<String> sites = queryParams.get("site");
        String site = Objects.nonNull(sites) && !sites.isEmpty() ? sites.get(0) : null;
		return site;
	}

	private String setTypes(MultivaluedMap<String, String> queryParams) {
		List<String> types = queryParams.get("type");
        String type = Objects.nonNull(types) && !types.isEmpty() ? types.get(0) : null;
		return type;
	}

    public String generateHstLink(HippoBean bean) {
        return RequestContextProvider.get().getHstLinkCreator()
                .create(bean, RequestContextProvider.get()).toUrlForm(RequestContextProvider.get(), true);
    }
}
