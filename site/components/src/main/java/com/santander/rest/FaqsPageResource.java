package com.santander.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQuery;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.query.HstQueryResult;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.onehippo.cms7.essentials.components.rest.BaseRestResource;

import com.santander.beans.FaqPage;
import com.santander.dto.FaqPageDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;
import io.swagger.annotations.AuthorizationScope;
import io.swagger.annotations.Api;

@Path("/faqs")
@Api(value = "FAQs", description = "FAQs page REST service")
public class FaqsPageResource extends BaseRestResource {

    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_FORM_URLENCODED})
    @ApiOperation(value = "Returns the content of a FAQs sections.", response = FaqPageDTO.class, authorizations = {
            @Authorization(value = "clientCredentials", scopes = 
            		@AuthorizationScope(scope = "read_faqs", description = ""))
    })
    @ApiResponses({
            @ApiResponse(code = 401, message = "Client could not be authenticated."),
            @ApiResponse(code = 403, message = "Client is not authorized to make this request."),
            @ApiResponse(code = 404, message = "The specified resource could not be found."),
            @ApiResponse(code = 500, message = "Unable to complete the request because an unexpected error occurred."),
            @ApiResponse(code = 200, message = "Successful retrieval of param value", response = FaqPageDTO.class),

    })

    @GET
    @Path("/")
    public Map<String, List<FaqPageDTO>> getFaqPage() {
        Map<String, List<FaqPageDTO>> content = new HashMap<>();
        try {
            List<FaqPageDTO> faqPageDAOList = new ArrayList<>();
            FaqPage faqPage = null;
            HstRequestContext requestContext = RequestContextProvider.get();
            HstQueryManager hstQueryManager = getHstQueryManager(requestContext);
            HippoBean scope = getMountContentBaseBean(requestContext);
            HstQuery hstQuery = hstQueryManager.createQuery(scope, FaqPage.class, true);
            HstQueryResult result = hstQuery.execute();
            HippoBeanIterator iterator = result.getHippoBeans();
            while (iterator.hasNext()) {
                faqPage = (FaqPage) iterator.nextHippoBean();
                FaqPageDTO faqPageDAO = new FaqPageDTO(faqPage);
                faqPageDAOList.add(faqPageDAO);
            }
            content.put("content", faqPageDAOList);

        } catch (Exception e) {
            throw new WebApplicationException(e);
        }
        return content;
    }
}
