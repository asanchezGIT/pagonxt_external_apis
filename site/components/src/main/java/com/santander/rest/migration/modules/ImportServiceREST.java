package com.santander.rest.migration.modules;

import com.google.gson.Gson;
import com.santander.rest.migration.beans.DocumentationBean;
import lombok.SneakyThrows;
import org.hippoecm.hst.content.annotations.Persistable;
import org.hippoecm.hst.content.beans.ObjectBeanManagerException;
import org.hippoecm.hst.content.beans.ObjectBeanPersistenceException;
import org.hippoecm.hst.content.beans.manager.workflow.WorkflowPersistenceManager;
import org.hippoecm.hst.content.beans.standard.HippoFolderBean;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.security.RolesAllowed;
import javax.jcr.RepositoryException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

public class ImportServiceREST
        extends org.hippoecm.hst.jaxrs.services.AbstractResource {

    private static Gson gson = new Gson();
    private static Logger logger = LoggerFactory.getLogger(ImportServiceREST.class);


    @Persistable
    @POST
    @Path("/import")
    public Response methodImport(
            @Context HttpServletRequest servletRequest,
            @Context HttpServletResponse servletResponse,
            @Context UriInfo uriInfo,
            @FormParam("payload") String payload,
            @FormParam("type") String type) {

        HstRequestContext requestContext =
                getRequestContext(servletRequest);

        try {
            // NOTE:
            // AbstractResource#getPersistenceManager(requestContext)
            // simply get a JCR session by calling
            // requestContext.getSession().
            // This method is annotated by @Persistable, so HST-2 JAX-RS
            // runtime engine returns a JCR session from the writable
            // session pool.

            DocumentationBean documentationPayload = gson.fromJson(payload, DocumentationBean.class);


            logger.info("the type is {} of Object documentationPayload ",type);
            logger.info("the payload is {} of Object documentationPayload ",payload);

            logger.info("the title is {} of Object documentationPayload ",documentationPayload.getTitle());
            logger.info("the alias is {} of Object documentationPayload ",documentationPayload.getAlias());


            WorkflowPersistenceManager wpm = (WorkflowPersistenceManager)
                    getPersistenceManager(requestContext);

            HippoFolderBean contentBaseFolder =
                    getMountContentBaseBean(requestContext);
            String productFolderPath = contentBaseFolder.getPath() +
                    "/content/documentation";
            String beanPath = wpm.createAndReturn(productFolderPath,
                    "santanderbrxm:documentation",
                    "nombre_prueba", true);
            DocumentationBean documentationBean = (DocumentationBean) wpm.getObject(beanPath);

            documentationBean.setAlias(documentationPayload.getAlias().toString());
            documentationBean.setDescripcion(documentationBean.getDescripcion().toString());
            documentationBean.setTitle(documentationBean.getTitle().toString());
            documentationBean.setType(documentationBean.getType().toString());

            wpm.update(documentationBean);
            wpm.save();

        } catch (ObjectBeanPersistenceException | RepositoryException e) {
            e.printStackTrace();
        } catch (ObjectBeanManagerException e) {
            e.printStackTrace();
        }

        return Response.status(201).entity("ok response").build();
    }
}
