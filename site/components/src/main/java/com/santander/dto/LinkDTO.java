package com.santander.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LinkDTO {

	
	
    private String title;
    private String name;
    private String internalLinkId;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String parentProductId;
    private String version;
    private String url;
    private String channel;
    private String locale;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String tab;
}
