package com.santander.dto;

import java.util.List;
import lombok.Data;

@Data
public class GetStartedDTO {

    String title;
    String description;
    List<LinkDTO> links;
    List<GetStartedSectionDTO> sections;

}
