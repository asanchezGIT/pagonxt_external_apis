package com.santander.dto;

import com.santander.beans.FaqItem;
import com.santander.beans.FaqPage;
import lombok.Data;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@Data
public class FaqPageDTO extends GenericDAOResource {
    private static Logger logger = LoggerFactory.getLogger(FaqPageDTO.class);
    private String title;
    private List<FaqItemDTO> faqs = new ArrayList<>();

    public FaqPageDTO(final FaqPage faqPage) {
        this.setTitle(faqPage.getTile());
        try {
            //related articles
            for (HippoBean faqItemBean : faqPage.getRelatedDocuments()) {
                if (faqItemBean instanceof FaqItem) {
                    FaqItemDTO faqItemDTO = new FaqItemDTO((FaqItem) faqItemBean);
                    this.faqs.add(faqItemDTO);
                }
            }
        } catch (Exception e) {
            logger.error("Error: ", e);
        }


    }

}