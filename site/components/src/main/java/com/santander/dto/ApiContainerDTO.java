/*
 * Copyright 2014-2019 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.santander.dto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.santander.utils.InternalLinkProcessor;
import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.santander.beans.ApiContainerItem;
import com.santander.beans.ApiItem;
import com.santander.beans.ProducerOrganizationContainer;
import com.santander.utils.HippoUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Produces({MediaType.APPLICATION_JSON})
public class ApiContainerDTO extends GenericDAOResource{

    private static Logger logger = LoggerFactory.getLogger(ApiContainerDTO.class);
    private String id;
    private String idIL;
    private String name;
    private String version;
    private String title;
    private String type;
    private DescriptionDTO description;
    private boolean documentation;
    private String containerProduceOrg;
    private boolean deprecated;
    private String status;
    private String[] availableExposureMode;
    private String channel;
    private String language;
    private String icon;
    private String alt;
    private String high;
    private String wide;
    private String document;



    public ApiContainerDTO(ApiContainerItem apiContainer, String siteName)  {
    	
    	//logger.info("*****INICIO ApiContainerDTO");
    	HstRequestContext requestContext = RequestContextProvider.get();

    	this.id = InternalLinkProcessor.getAliasByUuid(apiContainer.getCanonicalUUID());
        //logger.info("*****id");
        
        this.idIL = Objects.nonNull(apiContainer.getIdIL())? apiContainer.getIdIL() : StringUtils.EMPTY;
        //logger.info("*****idIL");
        this.name = Objects.nonNull( apiContainer.getNameCMS()) && ! apiContainer.getNameCMS().isEmpty() 
        		?  apiContainer.getNameCMS() : apiContainer.getNameIL();
        
        //logger.info("*****name");
        this.title = Objects.nonNull( apiContainer.getTitleCMS()) && ! apiContainer.getTitleCMS().isEmpty() 
        		?  apiContainer.getTitleCMS() : apiContainer.getTitleIL();
        //logger.info("*****title");

        this.version = Objects.nonNull( apiContainer.getVersionIL()) && ! apiContainer.getVersionIL().isEmpty() 
        		?  apiContainer.getVersionIL() : apiContainer.getVersionCMS();
        //logger.info("*****version");
               
        this.type = Objects.nonNull(apiContainer.getType())? apiContainer.getType() : StringUtils.EMPTY;
        //logger.info("*****type");

       
       
        //Description
        
        Mount mountPoint=hstRequestContext.getResolvedMount().getMount();
        
        try {
		      
		        String localeContent=apiContainer.getLocaleString().replace("-", "_");
		      
		        String mountRequestPath = requestContext.getResolvedMount().getMount().getContentPath();
		       
		        String localeMountRequestPath=mountRequestPath.substring(mountRequestPath.lastIndexOf("/")+1);
		        
		       
		        if (!localeContent.equals(localeMountRequestPath)){ //Si el lenguaje del contenido no se corresponde con el de la petición, busco el Mount del contenido
		          	 mountPoint=requestContext.getMount(siteName+"-"+localeContent); //coge el Mount con el atributo alias=superdiginal-es_es
		         	
		         	 
		         }
		        if (Objects.nonNull(apiContainer.getDescription())) {
		        	
		        		        	
		          this.description = DescriptionDTO.builder()
		                    .content(rewriteHstRichContent(apiContainer.getDescription().getContent(),
		                    		apiContainer.getDescription().getNode(),
		                    		mountPoint))
		                    .build();
		          
		        }
		        else {
		        	this.description = DescriptionDTO.builder().content("").build();
		        }
        
        }
        catch(Exception e) {
		       	 logger.error("*****error",e);
		       	 
		       	 this.description = DescriptionDTO.builder().content("").build();
        }
        //logger.info("*****description");
 
      
        ProducerOrganizationContainer containerOrgContainer=(ProducerOrganizationContainer)apiContainer
        		.getProducerOrganizationContainer();

        if (Objects.nonNull(containerOrgContainer)) {
            this.containerProduceOrg= Objects.nonNull(containerOrgContainer.getIdIL()) 
            		&& ! containerOrgContainer.getIdIL().isEmpty() ?  containerOrgContainer.getIdIL() 
            				:containerOrgContainer.getIdCMS();
        } else {
            this.containerProduceOrg = StringUtils.EMPTY;
        }
        
        //logger.info("*****containerProduceOrg");
        
        
        this.availableExposureMode = Objects.nonNull( apiContainer.getAvailableExposureMode())?apiContainer.getAvailableExposureMode() :null;
        
        //logger.info("*****availableExposureMode");

        this.deprecated=Objects.nonNull(apiContainer.getDeprecated()) && !apiContainer.getDeprecated()
        		.isEmpty() && "No".contentEquals(apiContainer.getDeprecated()) ?  false: true;
        
        //logger.info("*****deprecated");
        
        
        this.status = Objects.nonNull(apiContainer.getStatus())? apiContainer.getStatus() : StringUtils.EMPTY;
        //logger.info("*****status");
   
       

        // Recoge los ApiItems del apiContainerItem
        Iterator<HippoBean> apisIterator=apiContainer.getApi().iterator();
        List <ApiItemDTO> apiList=new ArrayList<>();
        ApiItem apiItem=null;
        ApiItemDTO apiItemDTO=null;

        while (apisIterator.hasNext()) {
            apiItem = (ApiItem) apisIterator.next();
            //Si no esta deprecada
            if ("No".equals(apiItem.getDeprecated())) {
                apiItemDTO=new ApiItemDTO(apiItem);
                apiList.add(apiItemDTO);
            }
            Collections.sort(apiList, new ApiItemDTO());
        }

        boolean upperApiItemHasInfo=false;
        if (apiList.size() > 0){
            ApiItemDTO apiItemDTOMayor=apiList.get(0);
            if((Objects.nonNull(apiItemDTOMayor.getDocumentation()) && apiItemDTOMayor.getDocumentation().size()>0)||
                    (Objects.nonNull(apiItemDTOMayor.getDescription()) && !apiItemDTOMayor
                    		.getDescription().getContent().isEmpty()) ||
                    (Objects.nonNull(apiItemDTOMayor.getPostmanCollection()) 
                    		&& !apiItemDTOMayor.getPostmanCollection().getUrl().isEmpty()) ||
                    (Objects.nonNull(apiItemDTOMayor.getFeaturesOverview())
                    		&& !apiItemDTOMayor.getFeaturesOverview().isEmpty()) ||
                    (Objects.nonNull(apiItemDTOMayor.getGlobalExposure()) 
                    		&& !apiItemDTOMayor.getGlobalExposure().isEmpty()) ||
                    (Objects.nonNull(apiItemDTOMayor.getProducts()) && apiItemDTOMayor.getProducts().size()>0) ||
                    (Objects.nonNull(apiItemDTOMayor.getLocalCountries()) 
                    		&& apiItemDTOMayor.getLocalCountries().size()>0)) {
                upperApiItemHasInfo=true;

            }
        }

        this.documentation=upperApiItemHasInfo;
        logger.debug("*****documentation");
        
        //DOCUMENT
       try { 
	       if (Objects.nonNull(apiContainer.getDocument())){
				String url =requestContext.getHstLinkCreator().create(apiContainer.getDocument(), requestContext).toUrlForm(requestContext, true);
				String uuid=HippoUtils.getNodeId(apiContainer.getDocument().getNode().getPath(), requestContext);
				String type = apiContainer.getDocument().getNode().getPrimaryNodeType().getName();
				
				this.document= internalLinkProcessor.generateInternalLink(url, type, uuid);
				
			}
			else {
				this.document= StringUtils.EMPTY;
			}
       }
       catch (Exception e) {
       	
	       	logger.error("*****error en document",e);
	       	this.document= StringUtils.EMPTY;
	       	
       } 
       logger.debug("*****document");
        
        //ICON
               
        this.icon = Objects.nonNull(apiContainer.getIcon())? generateHstImageLink(apiContainer.getIcon()) : StringUtils.EMPTY;
        logger.debug("*****icon");
        
        this.alt = Objects.nonNull(apiContainer.getAlt())?apiContainer.getAlt() : StringUtils.EMPTY;
        this.high = Objects.nonNull(apiContainer.getHigh())?apiContainer.getHigh() : StringUtils.EMPTY;
        this.wide = Objects.nonNull(apiContainer.getWide())?apiContainer.getWide() : StringUtils.EMPTY;
        
        try {
     	   Map<String, String> urlAttrs=null;
 	       urlAttrs =HippoUtils.getDocumentAttrsByURLCustom(apiContainer);
 	       
 	       logger.debug("*****channel:"+urlAttrs.get("channel"));
 	       logger.debug("*****locale:"+urlAttrs.get("locale"));
 	       this.channel = urlAttrs.get("channel");
 	       this.language = urlAttrs.get("locale");
 	       
        
        }
        catch (Exception e) {
        	
 	       	logger.error("*****error en getDocumentAttrsByURLCustom",e);
 	       	this.channel = StringUtils.EMPTY;
 		    this.language =StringUtils.EMPTY;
 	       	
        }
    }
}
