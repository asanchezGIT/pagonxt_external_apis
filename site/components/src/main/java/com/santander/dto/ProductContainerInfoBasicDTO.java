package com.santander.dto;

import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;

import com.santander.beans.ProductContainerItem;
import com.santander.utils.HippoUtils;

import lombok.Data;

@Data
public class ProductContainerInfoBasicDTO extends GenericDAOResource {
    private String id;
    private String title;
    private String region;
    private String channel;
    private String language;

    public ProductContainerInfoBasicDTO(final ProductContainerItem productContainer) {
    	
    	 this.title = Objects.nonNull( productContainer.getTitleCMS()) && ! productContainer.getTitleCMS().isEmpty() 
         		?  productContainer.getTitleCMS() : productContainer.getNameIL();
         
         this.id=Objects.nonNull( productContainer.getAlias()) && ! productContainer.getAlias().isEmpty() 
          		?  productContainer.getAlias() : productContainer.getCanonicalUUID();	
         
         this.region=Objects.nonNull(productContainer.getRegion())? productContainer.getRegion() : StringUtils.EMPTY;
         
         
    	
          		
        // Map<String, String> urlAttrs = HippoUtils.getDocumentAttrsByURLCustom(hstRequestContext, productContainer);
         Map<String, String> urlAttrs = HippoUtils.getDocumentAttrsByURLCustom(productContainer);

    	
         this.channel = urlAttrs.get("channel");
         this.language = urlAttrs.get("locale");
         
        		 
        

    }

}
