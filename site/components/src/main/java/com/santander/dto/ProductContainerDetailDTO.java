package com.santander.dto;

import lombok.Data;

@Data
public class ProductContainerDetailDTO {

    ProductContainerExpandDTO productContainer;
}