package com.santander.dto;

import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.core.request.HstRequestContext;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.santander.beans.ApiContainerItem;
import com.santander.beans.CallToActionItemSimple;
import com.santander.utils.HippoUtils;

import lombok.Builder;
import lombok.Data;

@Data

public class CallToActionItemSimpleDTO extends GenericDAOResource{
    private String title;
    private String internalLink;
    private String tab;
    
    public CallToActionItemSimpleDTO(HstRequestContext requestContext,CallToActionItemSimple item)  {
    	try {
    		this.title= Objects.nonNull(item.getTitle())? item.getTitle(): StringUtils.EMPTY;
        	this.tab= Objects.nonNull(item.getTab())? item.getTab(): StringUtils.EMPTY;
        	
    		if (Objects.nonNull(item.getInternalLink())){
    			String url =requestContext.getHstLinkCreator().create(item.getInternalLink(), requestContext).toUrlForm(requestContext, true);
    			String uuid=HippoUtils.getNodeId(item.getInternalLink().getNode().getPath(), requestContext);
    			String type = item.getInternalLink().getNode().getPrimaryNodeType().getName();
    			this.internalLink= internalLinkProcessor.generateInternalLink(url, type, uuid);
    			
    			
    		}
    		else {
    			this.internalLink= StringUtils.EMPTY;
    		}
     		
    	}
    	catch( Exception e) {
    		this.title= Objects.nonNull(item.getTitle())? item.getTitle(): StringUtils.EMPTY;
        	this.tab= Objects.nonNull(item.getTab())? item.getTab(): StringUtils.EMPTY;
        	this.internalLink= StringUtils.EMPTY;
    		
    	}
     
    }
}
