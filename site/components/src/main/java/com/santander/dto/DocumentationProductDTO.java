package com.santander.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
public class DocumentationProductDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String id; //alias or UUID
    String title;
    String channel;
    String language;

    @Override public String toString() {
        return "DocumentationDTO [title=" + title + ",channel="+channel+",languaje="+language+"]";
    }

}
