package com.santander.dto;

import com.santander.beans.ProductContainer;
import lombok.Data;

@Data
public class ProductContainerDTO extends GenericDAOResource {
    private String id;
    private String icon;
    private String title;
    private DescriptionDTO description;
    private LinkDTO link;

    public ProductContainerDTO(final ProductContainer productContainer) {
        if(productContainer.getIcon()!=null) {
            this.setIcon(generateHstImageLink(productContainer.getIcon()));
        }
        this.setDescription(DescriptionDTO.builder().content(productContainer.getDescription().getContent()).build());

    }

}
