package com.santander.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
public class MenuDocumentationDTO {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    //String internalLinkId;
    String id; //UUID
    List<SubMenuDocumentionLevelOneDTO> documentsGroups;

    

    @Override public String toString() {
        //return "documentGroupDTO [internalLinkId=" + internalLinkId +",documentsGroups=" + documentsGroups + "]";
    	return " [documentsGroups=" + documentsGroups + "]";
    }

}
