package com.santander.dto;

import java.util.List;

import org.onehippo.cms7.essentials.components.rest.BaseRestResource;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DocumentationsDTO extends BaseRestResource {

      List <DocumentationDTO> documentations;
}
