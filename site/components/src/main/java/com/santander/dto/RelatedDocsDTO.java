package com.santander.dto;

import com.santander.beans.NewsAndArticles;
import lombok.Data;

@Data
public class RelatedDocsDTO{

    private String id;
    private String title;
    private DescriptionDTO description;
    private String image;
    private String author;
    private String iconAuthor;

    public RelatedDocsDTO(final NewsAndArticles articleDoc) {
        this.setId((articleDoc.getIdentifier()));
        this.setTitle(articleDoc.getTitle());
        this.setAuthor(articleDoc.getAuthor());
    }


}
