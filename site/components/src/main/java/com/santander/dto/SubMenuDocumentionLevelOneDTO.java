package com.santander.dto;

import java.util.List;
import lombok.Data;

@Data
public class SubMenuDocumentionLevelOneDTO {

    String title;
    String description;
    List<DocumentationLinkDTO> documents;
    List <SubMenuDocumentionLevelTwoDTO> documentsGroups;

}
