package com.santander.dto;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.santander.utils.InternalLinkProcessor;

import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.santander.beans.NewsAndArticles;
import com.santander.utils.HippoUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Produces({MediaType.APPLICATION_JSON})
public class InformationDTO extends GenericDAOResource {

    private static Logger logger = LoggerFactory.getLogger(InformationDTO.class);
    private String id;
    private String title;
    private String type;
    private String author;
    private String profile;
    private String[] tags;
    private DescriptionDTO description;
    private String image;
    private String alt;
    private String high;
    private String wide;
    private String date;
    private String iconAuthor;
    private DescriptionDTO shortDescription;
    private String littleIcon;
    private String webinarVideo;
    private List<RelatedDocsDTO> relatedDocuments = new ArrayList<>();
    private String channel;
    private String locale;



    public InformationDTO(NewsAndArticles articlePage) {
    	
    	HstRequestContext requestContext = RequestContextProvider.get();
        this.id = InternalLinkProcessor.getAliasByUuid(articlePage.getCanonicalUUID());
        this.title = articlePage.getTitle();
        this.author = articlePage.getAuthor();
        this.profile = articlePage.getProfile();
        this.type = articlePage.getType();
        
        Map<String, String> urlAttrs =null;
        try {
        	//urlAttrs =HippoUtils.getDocumentAttrsByURLCustom(requestContext, articlePage);
        	urlAttrs =HippoUtils.getDocumentAttrsByURLCustom(articlePage);
        }
        catch (Exception e) {
        	logger.error("*****ERROR:"+e.getMessage());
        	
        	
        }

        this.channel = urlAttrs.get("channel");
        this.locale = urlAttrs.get("locale");

        if (articlePage.getDate() != null) {
            Calendar cal = articlePage.getDate();
            SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = format1.format(cal.getTime());
            this.date = formattedDate;
        }

        if (articlePage.getImage() != null) {
            this.image = generateHstImageLink(articlePage.getImage());
        }
        
        this.alt=Objects.nonNull(articlePage.getAlt())?articlePage.getAlt() : StringUtils.EMPTY;
        this.high=Objects.nonNull(articlePage.getHigh())?articlePage.getHigh() : StringUtils.EMPTY;
        this.wide=Objects.nonNull(articlePage.getWide())?articlePage.getWide() : StringUtils.EMPTY;
        
        

        
        Mount mountPoint=hstRequestContext.getResolvedMount().getMount();
        try {
		       
		        String localeContent=articlePage.getLocaleString().replace("-", "_");
		      
		       
		        String mountRequestPath = hstRequestContext.getResolvedMount().getMount().getContentPath();
		        String localeMountRequestPath=mountRequestPath.substring(mountRequestPath.lastIndexOf("/")+1);
		       
		         
		       
		        if (!localeContent.equals(localeMountRequestPath)){ //Si el lenguaje del contenido no se corresponde con el de la petición, busco el Mount del contenido
		          	 mountPoint=hstRequestContext.getMount(urlAttrs.get("channel")+"-"+localeContent); //coge el Mount con el atributo alias=superdiginal-es_es
		         	
		         	 
		         }
		         
		        logger.debug("*****mountPoint:"+mountPoint);
        
        
       
        	 if (Objects.nonNull(articlePage.getDescription())) {
        		this.description = DescriptionDTO.builder()
                .content(rewriteHstRichContent(articlePage.getDescription().getContent(),
                        articlePage.getDescription().getNode(),
                        mountPoint))
                .build();
        	 }
        	 else {
        		 this.description = DescriptionDTO.builder().content("").build(); 
        		 
        	 }
        }
        catch( Exception e) {
        	this.description = DescriptionDTO.builder().content("").build();
        	logger.error("*****ERROR InformationDTO:"+e.getMessage());
        	e.printStackTrace();
                    
   
        }
        
		
        
        
        

        String[] tagsDTO = articlePage.getTags();
        this.tags = tagsDTO;


        if (articlePage.getIconAuthor() != null) {
            this.iconAuthor = generateHstImageLink(articlePage.getIconAuthor());
        }

        try {
	       if (Objects.nonNull(articlePage.getShortDescription())) { 
	    	   this.shortDescription = DescriptionDTO.builder()
	                .content(rewriteHstRichContent(articlePage.getShortDescription().getContent(),
	                        articlePage.getShortDescription().getNode(),
	                        mountPoint))
	                .build();
	       
	       }
	       else {
	        	this.description = DescriptionDTO.builder().content("").build();
	        }
       }
   
 
	   catch( Exception e) {
		   	this.description = DescriptionDTO.builder().content("").build();
		   	logger.error("*****ERROR InformationDTO:"+e.getMessage());
		   	e.printStackTrace();
	   }

        if (articlePage.getLittleIcon() != null) {
            this.littleIcon = generateHstImageLink(articlePage.getLittleIcon());
        }

        if (articlePage.getVideo() != null) {
            this.webinarVideo = generateHstLink(articlePage.getVideo());
        }

        try {
            // related articles
            for (HippoBean articlePageBean : articlePage.getRelatedDocuments()) {
                if (articlePageBean instanceof NewsAndArticles) {

                    RelatedDocsDTO relatedDocs = new RelatedDocsDTO((NewsAndArticles) articlePageBean);
                    relatedDocs.setIconAuthor(
                            generateHstImageLink(((NewsAndArticles) articlePageBean).getIconAuthor()));
                    relatedDocs
                            .setImage(generateHstImageLink(((NewsAndArticles) articlePageBean).getImage()));

                 
                    try {
	                    relatedDocs.setDescription(DescriptionDTO.builder()
	                            .content(rewriteHstRichContent(
	                                    ((NewsAndArticles) articlePageBean).getDescription().getContent(),
	                                    ((NewsAndArticles) articlePageBean).getDescription().getNode(),
	                                    mountPoint))
	                            .build());
	                    relatedDocs.setDescription(DescriptionDTO.builder().content(((NewsAndArticles) articlePageBean).getDescription().getContent())
	                            .build());
                    }
                    catch(Exception e) {
                    	relatedDocs.setDescription(DescriptionDTO.builder().content("").build());
                    }



                    this.relatedDocuments.add(relatedDocs);
                }
            }
        } catch (Exception e) {
            logger.error("Error: ", e);
        }

    }

}
