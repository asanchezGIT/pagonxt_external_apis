package com.santander.dto;

import com.santander.utils.Locale;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

@Data
public class ChannelsListDTO {

    private static Logger logger = LoggerFactory.getLogger(ChannelsListDTO.class);

    public ChannelsListDTO() {
		// Auto-generated constructor stub
	}
    
    private String channel;
    private List<Locale> languages = new ArrayList<>();

    @Override public String toString() {
        return "ChannelsListDTO [channel=" + channel +", languages=" + languages + "]";
    }

}
