package com.santander.dto;

import java.util.List;
import org.apache.commons.lang.StringUtils;

public class FacetItemDTO {
  
 public FacetItemDTO() {
	 //empty constructor
 }

    private String id;
    private String title;
    private List<FacetItemDTO> facets;

    public String getId() {
        return StringUtils.chomp(id);
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
      return StringUtils.chomp(title);
  }

  public void setTitle(String title) {
      this.title = title;
  }
  public List<FacetItemDTO> getFacets() {
    return this.facets;
  }

  public void setFacets(List<FacetItemDTO> facets ) {
      this.facets = facets;
  }

}
