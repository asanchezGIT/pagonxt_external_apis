package com.santander.dto;

import com.santander.beans.ProductContainer;
import com.santander.beans.ProductsContainers;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ProductContainersDTO {

    private String title;
    private List<ProductContainerDTO> items = new ArrayList<>();

    public ProductContainersDTO(final ProductsContainers productsContainers) {
        this.title = productsContainers.getTitle();
        for(ProductContainer productContainer : productsContainers.getItems()) {
            ProductContainerDTO productContainerDTO = new ProductContainerDTO(productContainer);
            this.items.add(productContainerDTO);
        }
    }

}