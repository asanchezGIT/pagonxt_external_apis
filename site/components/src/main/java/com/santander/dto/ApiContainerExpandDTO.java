package com.santander.dto;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.santander.utils.InternalLinkProcessor;
import org.apache.commons.lang.StringUtils;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoBeanIterator;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.onehippo.cms7.essentials.components.rest.BaseRestResource;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.santander.beans.ApiContainerItem;
import com.santander.beans.ApiItem;
import com.santander.beans.NewsAndArticles;
import com.santander.beans.ProducerOrganizationContainer;
import com.santander.utils.HippoUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiContainerExpandDTO extends GenericDAOResource{


    private static Logger logger = LoggerFactory.getLogger(ApiContainerExpandDTO.class);
    private String id;
    private String idIL;
    private String name;
    private String version;
    private String title;
    private String type;
    private String icon;
    private String alt;
    private String high;
    private String wide;
    private DescriptionDTO description;
    private boolean documentation;
    private String containerProduceOrg;
    private boolean deprecated;
    private String status;
    private String[] availableExposureMode;
    private String channel;
    private String language;
    List <ApiItemDTO> apis;
    private String document;


    public   ApiContainerExpandDTO(ApiContainerItem apiContainer, String siteName){

        HstRequestContext requestContext = RequestContextProvider.get();

        this.id = InternalLinkProcessor.getAliasByUuid(apiContainer.getCanonicalUUID());
        this.idIL = apiContainer.getIdIL();
        this.name = Objects.nonNull( apiContainer.getNameCMS()) && ! apiContainer.getNameCMS().isEmpty() 
        		?  apiContainer.getNameCMS() : apiContainer.getNameIL();
        this.title = Objects.nonNull( apiContainer.getTitleCMS()) && ! apiContainer.getTitleCMS().isEmpty() 
        		?  apiContainer.getTitleCMS() : apiContainer.getTitleIL();
        this.version = Objects.nonNull( apiContainer.getVersionIL()) && ! apiContainer.getVersionIL()
        		.isEmpty() ?  apiContainer.getVersionIL() : apiContainer.getVersionCMS();
        this.type = apiContainer.getType();
        
     
        
        
        
        
        Mount mountPoint=hstRequestContext.getResolvedMount().getMount();
        try {
        	
	        String localeContent=apiContainer.getLocaleString().replace("-", "_");
	        String mountRequestPath = requestContext.getResolvedMount().getMount().getContentPath();
	        String localeMountRequestPath=mountRequestPath.substring(mountRequestPath.lastIndexOf("/")+1);
	       
	        if (!localeContent.equals(localeMountRequestPath)){ //Si el lenguaje del contenido no se corresponde con el de la petición, busco el Mount del contenido
	          	 mountPoint=requestContext.getMount(siteName+"-"+localeContent); //coge el Mount con el atributo alias=superdiginal-es_es
	         	
	         	 
	         }
	        logger.debug("*****mountPoint:"+mountPoint);
	        
        	
	        if (Objects.nonNull(apiContainer.getDescription())) {
                this.description = DescriptionDTO.builder()
                    .content(rewriteHstRichContent(apiContainer.getDescription().getContent(),
                                apiContainer.getDescription().getNode(),
                                mountPoint)).build();
        	}
        	else {
        		this.description = DescriptionDTO.builder().content("").build();
	        }
        	
        	
        }
        catch( Exception e) {
        	this.description = DescriptionDTO.builder().content("").build();
        	logger.error("*****ERROR ApiContainerExpandDTO:"+e.getMessage());
        	e.printStackTrace();
                    
   
        }
       
        	
        	
        ProducerOrganizationContainer containerOrgContainer=(ProducerOrganizationContainer)apiContainer
        		.getProducerOrganizationContainer();

        if(containerOrgContainer != null) {
            this.containerProduceOrg= Objects.nonNull(containerOrgContainer.getIdIL()) 
            		&& ! containerOrgContainer.getIdIL().isEmpty() 
            		?  containerOrgContainer.getIdIL() :containerOrgContainer.getIdCMS();
        } else {
            this.containerProduceOrg = StringUtils.EMPTY;
        }

        this.availableExposureMode=apiContainer.getAvailableExposureMode();

        this.deprecated=Objects.nonNull(apiContainer.getDeprecated()) && !apiContainer.getDeprecated()
        		.isEmpty() && "No".contentEquals(apiContainer.getDeprecated()) ?  false: true;
        this.status=apiContainer.getStatus();

        

        // Recoge los ApiItems del apiContainerItem
        Iterator<HippoBean> apisIterator=apiContainer.getApi().iterator();
        List <ApiItemDTO> apiList=new ArrayList<>();
        ApiItem apiItem=null;
        ApiItemDTO apiItemDTO=null;

        while (apisIterator.hasNext()) {
            apiItem = (ApiItem) apisIterator.next();
            //Si no esta deprecada
            if ("No".equals(apiItem.getDeprecated())) {
                apiItemDTO=new ApiItemDTO(apiItem);
                apiList.add(apiItemDTO);
            }
            Collections.sort(apiList, new ApiItemDTO());
        }

        boolean upperApiItemHasInfo=false;
        if (apiList.size() > 0){
            ApiItemDTO apiItemDTOMayor=apiList.get(0);
            if((Objects.nonNull(apiItemDTOMayor.getDocumentation()) && apiItemDTOMayor.getDocumentation().size()>0)||
                    (Objects.nonNull(apiItemDTOMayor.getDescription()) && !apiItemDTOMayor
                    		.getDescription().getContent().isEmpty()) ||
                    (Objects.nonNull(apiItemDTOMayor.getPostmanCollection()) 
                    		&& !apiItemDTOMayor.getPostmanCollection().getUrl().isEmpty()) ||
                    (Objects.nonNull(apiItemDTOMayor.getFeaturesOverview()) 
                    		&& !apiItemDTOMayor.getFeaturesOverview().isEmpty()) ||
                    (Objects.nonNull(apiItemDTOMayor.getGlobalExposure()) 
                    		&& !apiItemDTOMayor.getGlobalExposure().isEmpty()) ||
                    (Objects.nonNull(apiItemDTOMayor.getProducts()) && apiItemDTOMayor.getProducts().size()>0) ||
                    (Objects.nonNull(apiItemDTOMayor.getLocalCountries()) 
                    		&& apiItemDTOMayor.getLocalCountries().size()>0)) {
                upperApiItemHasInfo=true;

            }
        }

        this.apis=apiList;
        this.documentation=upperApiItemHasInfo;
        
        
        //DOCUMENT
        try { 
 	       if (Objects.nonNull(apiContainer.getDocument())){
 				String url =requestContext.getHstLinkCreator().create(apiContainer.getDocument(), requestContext).toUrlForm(requestContext, true);
 				String uuid=HippoUtils.getNodeId(apiContainer.getDocument().getNode().getPath(), requestContext);
 				String type = apiContainer.getDocument().getNode().getPrimaryNodeType().getName();
 				
 				this.document= internalLinkProcessor.generateInternalLink(url, type, uuid);
 				
 			}
 			else {
 				this.document= StringUtils.EMPTY;
 			}
        }
        catch (Exception e) {
        	
 	       	logger.error("*****error en document",e);
 	       	this.document= StringUtils.EMPTY;
 	       	
        } 
        logger.debug("*****document");
        
        
        //ICON
        

        if (apiContainer.getIcon() != null) {
            this.icon = generateHstImageLink(apiContainer.getIcon());
        }
        
        if (apiContainer.getAlt() != null) {
            this.alt = apiContainer.getAlt();
        }
        
        if (apiContainer.getHigh() != null) {
            this.high = apiContainer.getHigh();
        }
        
        if (apiContainer.getIcon() != null) {
            this.wide = apiContainer.getWide();
        }
        
        Map<String, String> urlAttrs=null;
        try {
        	//urlAttrs =HippoUtils.getDocumentAttrsByURLCustom(requestContext, apiContainer);
        	 urlAttrs =HippoUtils.getDocumentAttrsByURLCustom(apiContainer);
        	 this.channel = urlAttrs.get("channel");
             this.language = urlAttrs.get("locale");
        }
        catch(Exception e) {
        	 this.channel = StringUtils.EMPTY;
             this.language =StringUtils.EMPTY;
        	 logger.error("*****ERROR:"+e.getMessage(),e);
        }

       
    }

}
 
