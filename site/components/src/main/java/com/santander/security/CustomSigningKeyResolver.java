package com.santander.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwsHeader;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SigningKeyResolverAdapter;
import org.hippoecm.hst.configuration.hosting.Mount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class CustomSigningKeyResolver extends SigningKeyResolverAdapter {
    private static Logger logger = LoggerFactory.getLogger(CustomSigningKeyResolver.class);
    public static final String PUBLIC_KEY_SERVER_URL = "publicKeyServerUrl";
    public static final String CERTIFICATE_PATH = "certificatePath";
    public static final String KEYSTORE_PATH = "keystorePath";
    public static final String KEYSTORE_PASSWORD = "keystorePassword";
    private static Mount mount;


    public CustomSigningKeyResolver(Mount mount) {
        this.mount = mount;
    }

    @Override
    public Key resolveSigningKey(JwsHeader jwsHeader, Claims claims) {

        try {
            String kid = jwsHeader.getKeyId();
            String alg = jwsHeader.getAlgorithm();
            if (alg == null || alg.equals("")) {
                SignatureAlgorithm.RS256.getValue();
            }

            String serverUrl = mount.getProperty(PUBLIC_KEY_SERVER_URL);
            String certificatePath = mount.getProperty(CERTIFICATE_PATH);
            String keystorePath = mount.getProperty(KEYSTORE_PATH);
            String keystorePassword = (mount.getProperty(KEYSTORE_PASSWORD) != null 
            		? mount.getProperty(KEYSTORE_PASSWORD) : "changeit");
            KeyStore keyStore = CertificateUtils.importCertificateToLocalKeystore(certificatePath, 
            		keystorePath, keystorePassword);

            //We need to adjust the code to the other algorithms
            byte[] encodedPublicKey = Base64.getDecoder().decode(PublicKeyService
            		.getPublicKeyFromServer(serverUrl, keyStore, kid));
            X509EncodedKeySpec spec = new X509EncodedKeySpec(encodedPublicKey);
            KeyFactory kf = KeyFactory.getInstance("RSA");
            return kf.generatePublic(spec);
        } catch (Exception e) {
            logger.error("Error validating JWT token:", e);
        }
        return null;
    }


}