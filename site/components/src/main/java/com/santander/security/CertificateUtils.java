package com.santander.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Objects;

public final class CertificateUtils {

    public static final String PUBLIC_KEY_CERTIFICATE = "publicKeyCertificate";

    private CertificateUtils() {
    }

    private static Logger logger = LoggerFactory.getLogger(CertificateUtils.class);

    public static KeyStore importCertificateToLocalKeystore(String certificatePath, String keystorePath
    		, String keyStorePassword) {
        logger.debug("START importCertificateToLocalKeystore()");

        logger.debug("certificatePath {}", certificatePath);
        logger.debug("keystorePath {}", keystorePath);

        try {

            KeyStore keystore = getOrCreateKeyStore(keystorePath, keyStorePassword);

            char[] password = keyStorePassword.toCharArray();

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream certstream = fullStream(certificatePath);
            Certificate certs = cf.generateCertificate(certstream);

            // Continue if the alias already exists in the keystore
            if (keystore.getCertificate(PUBLIC_KEY_CERTIFICATE) != null) {
                logger.debug("Alias already exists in the keystore");
                return keystore;
            }

            File keystoreFile = new File(keystorePath);
            // Load the keystore contents

            logger.debug("Load the keystore contents");

            //10-05-2022 FORTIFY
            /*
            FileInputStream in = new FileInputStream(keystoreFile);
            keystore.load(in, password);
            in.close();*/

            FileInputStream in = new FileInputStream(keystoreFile);

            try {
                keystore.load(in, password);

            } catch (IOException e)
            {
                logger.error("Error in FileInputStream importCertificateToLocalKeystore",e);
            } finally
            {
                if(Objects.nonNull(in)){
                    in.close();
                }
            }

            keystore.setCertificateEntry(PUBLIC_KEY_CERTIFICATE, certs);

            // Save the new keystore contents
            logger.debug("Save the new keystore contents");



            //10-05-2022 FORTIFY
            /*
            FileOutputStream out = new FileOutputStream(keystoreFile);
            keystore.store(out, password);
            out.close();*/

            FileOutputStream out = new FileOutputStream(keystoreFile);
            try {
                keystore.store(out, password);

            } catch (IOException e)
            {
                logger.error("Error in FileInputStream importCertificateToLocalKeystore",e);
            } finally
            {
                if(Objects.nonNull(out)){
                    out.close();
                }
            }

            logger.debug("END importCertificateToLocalKeystore()");
            return keystore;

        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
            logger.error("Keystore error", e);
        } catch (IOException e) {
            logger.error("File IO Exception", e);
        } catch (Exception e) {
            logger.error("Error loading certificate trust store", e);
        }
        return null;
    }

    public static KeyStore getOrCreateKeyStore(String keystorePath, String password) 
    		throws KeyStoreException, NoSuchAlgorithmException, CertificateException, IOException {

        logger.debug("START getOrCreateKeyStore()");

        File file = new File(keystorePath);
        KeyStore keyStore = KeyStore.getInstance("JKS");

        if(file.exists()){
            try (FileInputStream finput = new FileInputStream(file)) {
                keyStore.load(finput, password.toCharArray());

            } catch (IOException e) {
                logger.error("Error in getOrCreateKeyStore", e);
            }
        }else{
            try(FileOutputStream foutput = new FileOutputStream(file)){
                keyStore.load(null, null);
                keyStore.store(foutput, password.toCharArray());

            }catch (IOException e) {
                logger.error("Error in getOrCreateKeyStore files no exists",e);
            }
        }

        logger.debug("END getOrCreateKeyStore()");

        return keyStore;
    }

    private static InputStream fullStream(String fname) throws IOException {
        DataInputStream dis = null;
        byte[] bytes = null;
        try ( FileInputStream fis = new FileInputStream(fname);
               ){
        	dis = new DataInputStream(fis);
        	bytes = new byte[dis.available()];
        	dis.readFully(bytes);
        } catch (IOException e)
        {
        	logger.error(" error in fullStream ",e);
        } finally
        {
            if(Objects.nonNull(dis)){
        	    dis.close();
            }
        }
        return new ByteArrayInputStream(bytes);
    }

}