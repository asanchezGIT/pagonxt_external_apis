package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.Calendar;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import java.util.List;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ProductContainerItem")
@Node(jcrType = "santanderbrxm:ProductContainerItem")
public class ProductContainerItem extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:PK")
    public String getPK() {
        return getSingleProperty("santanderbrxm:PK");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:idIL")
    public String getIdIL() {
        return getSingleProperty("santanderbrxm:idIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:nameIL")
    public String getNameIL() {
        return getSingleProperty("santanderbrxm:nameIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:versionIL")
    public String getVersionIL() {
        return getSingleProperty("santanderbrxm:versionIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:versionCMS")
    public String getVersionCMS() {
        return getSingleProperty("santanderbrxm:versionCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleCMS")
    public String getTitleCMS() {
        return getSingleProperty("santanderbrxm:titleCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:descriptionCMS")
    public HippoHtml getDescriptionCMS() {
        return getHippoHtml("santanderbrxm:descriptionCMS");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:descriptionOverview")
	public HippoHtml getDescriptionOverview() {
	    return getHippoHtml("santanderbrxm:descriptionOverview");
	}
    

    @HippoEssentialsGenerated(internalName = "santanderbrxm:status")
    public String getStatus() {
        return getSingleProperty("santanderbrxm:status");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:region")
    public String getRegion() {
        return getSingleProperty("santanderbrxm:region");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:icon")
    public HippoGalleryImageSet getIcon() {
        return getLinkedBean("santanderbrxm:icon", HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:links")
    public List<LinksAndDownloads> getLinks() {
        return getChildBeansByName("santanderbrxm:links",
                LinksAndDownloads.class);
    }

    @HippoEssentialsGenerated(internalName = "hippostd:tags")
    public String[] getTags() {
        return getMultipleProperty("hippostd:tags");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:postmanCollectionlink")
    public HippoBean getPostmanCollection() {
        return getLinkedBean("santanderbrxm:postmanCollectionlink",
                HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:featuresOverview")
    public HippoHtml getFeaturesOverview() {
        return getHippoHtml("santanderbrxm:featuresOverview");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:menuDoc")
    public HippoBean getMenuDocumentation() {
        return getLinkedBean("santanderbrxm:menuDoc", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:apiContainers")
    public List<HippoBean> getApiContainers() {
        return getLinkedBeans("santanderbrxm:apiContainers", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:producerOrganizationContainer")
    public HippoBean getProducerOrganizationContainer() {
        return getLinkedBean("santanderbrxm:producerOrganizationContainer",
                HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:updateDate")
    public Calendar getUpdateDate() {
        return getSingleProperty("santanderbrxm:updateDate");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:deprecated")
    public String getDeprecated() {
        return getSingleProperty("santanderbrxm:deprecated");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:alias")
    public String getAlias() {
        return getSingleProperty("santanderbrxm:alias");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:integrationsProduct")
    public List<IntegrationProduct> getIntegrationsProduct() {
        return getChildBeansByName("santanderbrxm:integrationsProduct",
                IntegrationProduct.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:alt")
    public String getAlt() {
        return getSingleProperty("santanderbrxm:alt");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:high")
    public String getHigh() {
        return getSingleProperty("santanderbrxm:high");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:wide")
    public String getWide() {
        return getSingleProperty("santanderbrxm:wide");
    }
}
