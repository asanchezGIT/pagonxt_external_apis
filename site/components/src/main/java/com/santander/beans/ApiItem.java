package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.Calendar;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import java.util.List;
import com.santander.beans.LinksAndDownloads;
import com.santander.beans.ProductsApi;
import com.santander.beans.LocalCountry;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ApiItem")
@Node(jcrType = "santanderbrxm:ApiItem")
public class ApiItem extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:PK")
    public String getPK() {
        return getSingleProperty("santanderbrxm:PK");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:idIL")
    public String getIdIL() {
        return getSingleProperty("santanderbrxm:idIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:nameCMS")
    public String getNameCMS() {
        return getSingleProperty("santanderbrxm:nameCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:nameIL")
    public String getNameIL() {
        return getSingleProperty("santanderbrxm:nameIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:versionIL")
    public String getVersionIL() {
        return getSingleProperty("santanderbrxm:versionIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:versionCMS")
    public String getVersionCMS() {
        return getSingleProperty("santanderbrxm:versionCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleCMS")
    public String getTitleCMS() {
        return getSingleProperty("santanderbrxm:titleCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleIL")
    public String getTitleIL() {
        return getSingleProperty("santanderbrxm:titleIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:status")
    public String getStatus() {
        return getSingleProperty("santanderbrxm:status");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:updateDate")
    public Calendar getUpdateDate() {
        return getSingleProperty("santanderbrxm:updateDate");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:deprecated")
    public String getDeprecated() {
        return getSingleProperty("santanderbrxm:deprecated");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:descriptionIL")
    public HippoHtml getDescriptionIL() {
        return getHippoHtml("santanderbrxm:descriptionIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:descriptionCMS")
    public HippoHtml getDescriptionCMS() {
        return getHippoHtml("santanderbrxm:descriptionCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:icon")
    public HippoGalleryImageSet getIcon() {
        return getLinkedBean("santanderbrxm:icon", HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:yaml")
    public HippoBean getYaml() {
        return getLinkedBean("santanderbrxm:yaml", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:links")
    public List<LinksAndDownloads> getLinks() {
        return getChildBeansByName("santanderbrxm:links",
                LinksAndDownloads.class);
    }

    /** 
     * @HippoEssentialsGenerated(internalName = "santanderbrxm:postmanCollection")public LinksAndDownloads getPostmanCollection() { return getBean("santanderbrxm:postmanCollection", LinksAndDownloads.class); }
     */
    @HippoEssentialsGenerated(internalName = "santanderbrxm:postmanCollectionlink")
    public HippoBean getPostmanCollection() {
        return getLinkedBean("santanderbrxm:postmanCollectionlink",
                HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:featuresOverview")
    public HippoHtml getFeaturesOverview() {
        return getHippoHtml("santanderbrxm:featuresOverview");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:globalExposure")
    public HippoHtml getGlobalExposure() {
        return getHippoHtml("santanderbrxm:globalExposure");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:documentation")
    public List<HippoBean> getDocumentation() {
        return getLinkedBeans("santanderbrxm:documentation", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:products")
    public List<ProductsApi> getProducts() {
        return getChildBeansByName("santanderbrxm:products", ProductsApi.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:localCountry")
    public List<LocalCountry> getLocalCountry() {
        return getChildBeansByName("santanderbrxm:localCountry",
                LocalCountry.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:alt")
    public String getAlt() {
        return getSingleProperty("santanderbrxm:alt");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:high")
    public String getHigh() {
        return getSingleProperty("santanderbrxm:high");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:wide")
    public String getWide() {
        return getSingleProperty("santanderbrxm:wide");
    }
}
