package com.santander.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:callToActionButton")
@Node(jcrType = "santanderbrxm:callToActionButton")
public class CallToActionButton extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:externalLink")
    public String getExternalLink() {
        return getSingleProperty("santanderbrxm:externalLink");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:labelButton")
    public String getLabelButton() {
        return getSingleProperty("santanderbrxm:labelButton");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:colorButton")
    public String getColorButton() {
        return getSingleProperty("santanderbrxm:colorButton");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:internalLink")
    public HippoBean getInternalLink() {
        return getLinkedBean("santanderbrxm:internalLink", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:tab")
    public String getTab() {
        return getSingleProperty("santanderbrxm:tab");
    }
}
