package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;

@HippoEssentialsGenerated(internalName = "santanderbrxm:banner")
@Node(jcrType = "santanderbrxm:banner")
public class Banner extends HippoCompound {
	@HippoEssentialsGenerated(internalName = "santanderbrxm:title")
	public String getTitle() {
		return getSingleProperty("santanderbrxm:title");
	}

	@HippoEssentialsGenerated(internalName = "santanderbrxm:logo")
	public HippoGalleryImageSet getLogo() {
		return getLinkedBean("santanderbrxm:logo", HippoGalleryImageSet.class);
	}

	@HippoEssentialsGenerated(internalName = "santanderbrxm:backgroundImage")
	public HippoGalleryImageSet getBackgroundImage() {
		return getLinkedBean("santanderbrxm:backgroundImage",
				HippoGalleryImageSet.class);
	}

	@HippoEssentialsGenerated(internalName = "santanderbrxm:backgroundAlt")
	public String getBackgroundAlt() {
		return getSingleProperty("santanderbrxm:backgroundAlt");
	}
}
