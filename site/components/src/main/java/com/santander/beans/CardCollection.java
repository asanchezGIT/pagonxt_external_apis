package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

import java.util.List;
import com.santander.beans.CardItem;

@HippoEssentialsGenerated(internalName = "santanderbrxm:CardCollection")
@Node(jcrType = "santanderbrxm:CardCollection")
public class CardCollection extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:cardItem")
    public List<CardItem> getCardItem() {
        return getChildBeansByName("santanderbrxm:cardItem", CardItem.class);
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:lookandfeel")
    public String getLookandfeel() {
        return getSingleProperty("santanderbrxm:lookandfeel");
    }
}
