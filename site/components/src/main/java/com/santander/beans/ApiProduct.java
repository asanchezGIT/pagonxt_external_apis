package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoResourceBean;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ApiProduct")
@Node(jcrType = "santanderbrxm:ApiProduct")
public class ApiProduct extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:id")
    public String getId() {
        return getSingleProperty("santanderbrxm:id");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:version")
    public String getVersion() {
        return getSingleProperty("santanderbrxm:version");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:yamllink")
    public HippoBean getYaml() {
        return getLinkedBean("santanderbrxm:yamllink", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:hostnames")
    public String[] getHostnames() {
        return getMultipleProperty("santanderbrxm:hostnames");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:authmethods")
    public String[] getAuthmethods() {
        return getMultipleProperty("santanderbrxm:authmethods");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:regionessandbox")
    public String[] getRegionssandbox() {
        return getMultipleProperty("santanderbrxm:regionessandbox");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:regioneslive")
    public String[] getRegionslive() {
        return getMultipleProperty("santanderbrxm:regioneslive");
    }
}
