package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.Calendar;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import java.util.List;
import com.santander.beans.LinksAndDownloads;
import com.santander.beans.ProductsApi;
import com.santander.beans.LocalCountry;

@HippoEssentialsGenerated(internalName = "santanderbrxm:DocumentsGroup")
@Node(jcrType = "santanderbrxm:DocumentsGroup")
public class DocumentsGroup extends BaseDocument {
	
	@HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:documents")
    public List<HippoBean> getDocuments() {
        return getLinkedBeans("santanderbrxm:documents",
        		HippoBean.class);
    }
    

}
