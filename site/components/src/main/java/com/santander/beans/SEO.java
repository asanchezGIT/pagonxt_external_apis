package com.santander.beans;

import org.hippoecm.hst.content.beans.Node;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:SEO")
@Node(jcrType = "santanderbrxm:SEO")
public class SEO extends BaseDocument{

    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleSEO")
    public String getTitle() { return getSingleProperty("santanderbrxm:titleSEO");}
}
