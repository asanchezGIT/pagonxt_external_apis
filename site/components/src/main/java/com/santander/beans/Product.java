package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.Calendar;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.hippoecm.hst.content.beans.standard.HippoResourceBean;
import java.util.List;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import com.santander.beans.LinksAndDownloads;

@HippoEssentialsGenerated(internalName = "santanderbrxm:Product")
@Node(jcrType = "santanderbrxm:Product")
public class Product extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:team")
    public String getTeam() {
        return getSingleProperty("santanderbrxm:team");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:sandbox")
    public Boolean getSandbox() {
        return getSingleProperty("santanderbrxm:sandbox");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:live")
    public Boolean getLive() {
        return getSingleProperty("santanderbrxm:live");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:featuresOverview")
    public HippoHtml getFeaturesOverview() {
        return getHippoHtml("santanderbrxm:featuresOverview");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:postmanCollectionlink")
    public HippoBean getPostmanCollection() {
        return getLinkedBean("santanderbrxm:postmanCollectionlink", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:deprecated")
    public Boolean getDeprecated() {
        return getSingleProperty("santanderbrxm:deprecated");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:newVersion")
    public String getNewVersion() {
        return getSingleProperty("santanderbrxm:newVersion");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:apis")
    public List<ApiProduct> getApis() {
        return getLinkedBeans("santanderbrxm:apis", ApiProduct.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:links")
    public List<LinksAndDownloads> getLinksAndDownloads() {
        return getChildBeansByName("santanderbrxm:links",
                LinksAndDownloads.class);
    }

    @HippoEssentialsGenerated(internalName = "hippostd:tags")
    public String[] getTags() {
        return getMultipleProperty("hippostd:tags");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:documentation")
    public List<Documentation> getDocumentation() {
        return getLinkedBeans("santanderbrxm:documentation", Documentation.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:createdby")
    public String[] getCreatedby() {
        return getMultipleProperty("santanderbrxm:createdby");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:lastupdate")
    public Calendar getLastupdate() {
        return getSingleProperty("santanderbrxm:lastupdate");
    }

}
