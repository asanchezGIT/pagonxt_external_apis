package com.santander.beans;

import java.util.List;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:GettingStartingPage")
@Node(jcrType = "santanderbrxm:GettingStartingPage")
public class GettingStartingPage extends BaseDocument {
  @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
  public String getTitle() {
    return getSingleProperty("santanderbrxm:title");
  }

  @HippoEssentialsGenerated(internalName = "santanderbrxm:shortDescription")
  public HippoHtml getShortDescription() {
    return getHippoHtml("santanderbrxm:shortDescription");
  }

  @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
  public HippoHtml getDescription() {
    return getHippoHtml("santanderbrxm:description");
  }

  @HippoEssentialsGenerated(internalName = "santanderbrxm:sections")
  public List<SectionLevelOne> getSections() {
    return getChildBeansByName("santanderbrxm:sections", SectionLevelOne.class);
  }

  @HippoEssentialsGenerated(internalName = "santanderbrxm:links")
  public List<LinksAndDownloads> getLinks() {
    return getChildBeansByName("santanderbrxm:links", LinksAndDownloads.class);
  }

  @HippoEssentialsGenerated(internalName = "santanderbrxm:littleIcon")
  public HippoGalleryImageSet getLittleIcon() {
    return getLinkedBean("santanderbrxm:littleIcon", HippoGalleryImageSet.class);
  }
  @HippoEssentialsGenerated(internalName = "santanderbrxm:alias")
  public String getAlias() {
      return getSingleProperty("santanderbrxm:alias");
  }
}
