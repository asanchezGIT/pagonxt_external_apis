package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.onehippo.forge.beans.RelatedDocsBean;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import java.util.List;

@HippoEssentialsGenerated(internalName = "santanderbrxm:FaqPage")
@Node(jcrType = "santanderbrxm:FaqPage")
public class FaqPage extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:tile")
    public String getTile() {
        return getSingleProperty("santanderbrxm:tile");
    }

    @HippoEssentialsGenerated(internalName = "relateddocs:docs")
    public List<HippoBean> getRelatedDocuments() {
        final RelatedDocsBean documents = getBean("relateddocs:docs");
        return documents.getDocs();
    }
}
