package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

import java.util.List;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import com.santander.beans.Note;

@HippoEssentialsGenerated(internalName = "santanderbrxm:releaseNoteLevelTwo")
@Node(jcrType = "santanderbrxm:releaseNoteLevelTwo")
public class ReleaseNoteLevelTwo extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:notes")
    public List<Note> getNotes() {
        return getChildBeansByName("santanderbrxm:notes", Note.class);
    }
    
    
   
}
