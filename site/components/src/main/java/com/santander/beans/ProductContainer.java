package com.santander.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import java.util.Calendar;
import java.util.List;



@HippoEssentialsGenerated(internalName = "santanderbrxm:ProductContainer")
@Node(jcrType = "santanderbrxm:ProductContainer")
public class ProductContainer extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:PK")
    public String getPK() {
        return getSingleProperty("santanderbrxm:PK");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:idIL")
    public String getIdIL() {
        return getSingleProperty("santanderbrxm:idIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:nameCMS")
    public String getNameCMS() {
        return getSingleProperty("santanderbrxm:nameCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:nameIL")
    public String getNameIL() {
        return getSingleProperty("santanderbrxm:nameIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:versionIL")
    public String getVersionIL() {
        return getSingleProperty("santanderbrxm:versionIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:versionCMS")
    public String getVersionCMS() {
        return getSingleProperty("santanderbrxm:versionCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:newVersion")
    public String getNewVersion() {
        return getSingleProperty("santanderbrxm:newVersion");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:status")
    public String getStatus() {
        return getSingleProperty("santanderbrxm:status");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:exposure")
    public String getExposure() {
        return getSingleProperty("santanderbrxm:exposure");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:updateDate")
    public Calendar getUpdateDate() {
        return getSingleProperty("santanderbrxm:updateDate");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:deprecated")
    public String getDeprecated() {
        return getSingleProperty("santanderbrxm:deprecated");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:featuresOverview")
    public String getFeaturesOverview() {
        return getSingleProperty("santanderbrxm:featuresOverview");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:icon")
    public HippoGalleryImageSet getIcon() {
        return getLinkedBean("santanderbrxm:icon", HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:documentation")
    public HippoBean getDocumentation() {
        return getLinkedBean("santanderbrxm:documentation", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:links")
    public List<LinksAndDownloads> getLinks() {
        return getChildBeansByName("santanderbrxm:links",
                LinksAndDownloads.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:postmanCollection")
    public LinksAndDownloads getPostmanCollection() {
        return getBean("santanderbrxm:postmanCollection",
                LinksAndDownloads.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleIL")
    public String getTitleIL() {
        return getSingleProperty("santanderbrxm:titleIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:apiContainers")
    public ApiContainerItem getApiContainers() {
        return getBean("santanderbrxm:apiContainers", ApiContainerItem.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:producerOrganizationContainer")
    public HippoBean getProducerOrganizationContainer() {
        return getLinkedBean("santanderbrxm:producerOrganizationContainer",
                HippoBean.class);
    }
}
