package com.santander.beans;

import java.util.List;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:CallToActionCollectionProducts")
@Node(jcrType = "santanderbrxm:CallToActionCollectionProducts")
public class CallToActionCollectionProducts extends BaseDocument {
  
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
        
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:CallToActionItemProducts")
    public List<CallToActionItemProducts> getCallToActionItemProducts() {
        return getChildBeansByName("santanderbrxm:CallToActionItemProducts", CallToActionItemProducts.class);
    }

   
}
