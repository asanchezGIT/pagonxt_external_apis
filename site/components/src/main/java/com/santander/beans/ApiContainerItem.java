package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.Calendar;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import java.util.List;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ApiContainerItem")
@Node(jcrType = "santanderbrxm:ApiContainerItem")
public class ApiContainerItem extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:PK")
    public String getPK() {
        return getSingleProperty("santanderbrxm:PK");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:idIL")
    public String getIdIL() {
        return getSingleProperty("santanderbrxm:idIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:nameCMS")
    public String getNameCMS() {
        return getSingleProperty("santanderbrxm:nameCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:nameIL")
    public String getNameIL() {
        return getSingleProperty("santanderbrxm:nameIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:versionIL")
    public String getVersionIL() {
        return getSingleProperty("santanderbrxm:versionIL");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:versionCMS")
    public String getVersionCMS() {
        return getSingleProperty("santanderbrxm:versionCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:status")
    public String getStatus() {
        return getSingleProperty("santanderbrxm:status");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:type")
    public String getType() {
        return getSingleProperty("santanderbrxm:type");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:updateDate")
    public Calendar getUpdateDate() {
        return getSingleProperty("santanderbrxm:updateDate");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:deprecated")
    public String getDeprecated() {
        return getSingleProperty("santanderbrxm:deprecated");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:availableExposureMode")
    public String[] getAvailableExposureMode() {
        return getMultipleProperty("santanderbrxm:availableExposureMode");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:icon")
    public HippoGalleryImageSet getIcon() {
        return getLinkedBean("santanderbrxm:icon", HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:api")
    public List<HippoBean> getApi() {
        return getLinkedBeans("santanderbrxm:api", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:producerOrganizationContainer")
    public HippoBean getProducerOrganizationContainer() {
        return getLinkedBean("santanderbrxm:producerOrganizationContainer",
                HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleIL")
    public String getTitleIL() {
        return getSingleProperty("santanderbrxm:titleIL");
    }

    @HippoEssentialsGenerated(internalName = "hippotaxonomy:keys")
    public String[] getKeys() {
        return getMultipleProperty("hippotaxonomy:keys");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:titleCMS")
    public String getTitleCMS() {
        return getSingleProperty("santanderbrxm:titleCMS");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:alias")
    public String getAlias() {
        return getSingleProperty("santanderbrxm:alias");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:document")
    public HippoBean getDocument() {
        return getLinkedBean("santanderbrxm:document", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:alt")
    public String getAlt() {
        return getSingleProperty("santanderbrxm:alt");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:high")
    public String getHigh() {
        return getSingleProperty("santanderbrxm:high");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:wide")
    public String getWide() {
        return getSingleProperty("santanderbrxm:wide");
    }
}
