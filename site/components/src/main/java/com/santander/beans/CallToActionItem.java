package com.santander.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:CallToActionItem")
@Node(jcrType = "santanderbrxm:CallToActionItem")
public class CallToActionItem extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:externalLink")
    public String getExternalLink() {
        return getSingleProperty("santanderbrxm:externalLink");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:internalLink")
    public HippoBean getInternalLink() {
        return getLinkedBean("santanderbrxm:internalLink", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:icon")
    public HippoGalleryImageSet getIcon() {
        return getLinkedBean("santanderbrxm:icon", HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:tab")
    public String getTab() {
        return getSingleProperty("santanderbrxm:tab");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:alt")
    public String getAlt() {
        return getSingleProperty("santanderbrxm:alt");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:high")
    public String getHigh() {
        return getSingleProperty("santanderbrxm:high");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:wide")
    public String getWide() {
        return getSingleProperty("santanderbrxm:wide");
    }
}
