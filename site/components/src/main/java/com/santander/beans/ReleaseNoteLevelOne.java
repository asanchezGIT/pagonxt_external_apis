package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import java.util.List;
import com.santander.beans.ReleaseNoteLevelTwo;

@HippoEssentialsGenerated(internalName = "santanderbrxm:releaseNoteLevelOne")
@Node(jcrType = "santanderbrxm:releaseNoteLevelOne")
public class ReleaseNoteLevelOne extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:levels")
    public List<ReleaseNoteLevelTwo> getLevels() {
        return getChildBeansByName("santanderbrxm:levels",
                ReleaseNoteLevelTwo.class);
    }
}
