package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

import java.util.List;

@HippoEssentialsGenerated(internalName = "santanderbrxm:sectionLevelOne")
@Node(jcrType = "santanderbrxm:sectionLevelOne")
public class SectionLevelOne extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:sections")
    public List<SectionLevelTwo> getSections() {
        return getChildBeansByName("santanderbrxm:sections", SectionLevelTwo.class);
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:documentation")
    public List<HippoBean> getDocumentation() {
        return getLinkedBeans("santanderbrxm:documentation", HippoBean.class);
    }
    
}
