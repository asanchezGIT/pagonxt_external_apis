package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

import java.util.List;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

@HippoEssentialsGenerated(internalName = "santanderbrxm:sectionLevelFour")
@Node(jcrType = "santanderbrxm:sectionLevelFour")
public class SectionLevelFour extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:tutle")
    public String getTutle() {
        return getSingleProperty("santanderbrxm:tutle");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }
    
    @HippoEssentialsGenerated(internalName = "santanderbrxm:documentation")
    public List<HippoBean> getDocumentation() {
        return getLinkedBeans("santanderbrxm:documentation", HippoBean.class);
    }
    
    
}
