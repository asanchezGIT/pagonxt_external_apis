package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoHtml;

import java.util.List;

@HippoEssentialsGenerated(internalName = "santanderbrxm:BannerLinks")
@Node(jcrType = "santanderbrxm:BannerLinks")
public class BannerLinks extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }


    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }


    @HippoEssentialsGenerated(internalName = "santanderbrxm:linksAndDownloads")
    public List<LinksAndDownloads> getLinksAndDownloads() {
        return getChildBeansByName("santanderbrxm:linksAndDownloads", LinksAndDownloads.class);}
}
