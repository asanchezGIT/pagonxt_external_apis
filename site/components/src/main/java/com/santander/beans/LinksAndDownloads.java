package com.santander.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoBean;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:linksAndDownloads")
@Node(jcrType = "santanderbrxm:linksAndDownloads")
public class LinksAndDownloads extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:externalLink")
    public String getExternalLink() {
        return getSingleProperty("santanderbrxm:externalLink");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:internalLink")
    public HippoBean getInternalLink() {
        return getLinkedBean("santanderbrxm:internalLink", HippoBean.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:tab")
    public String getTab() {
        return getSingleProperty("santanderbrxm:tab");
    }
}
