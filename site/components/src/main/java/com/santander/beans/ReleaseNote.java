package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import java.util.List;
import com.santander.beans.ReleaseNoteLevelOne;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ReleaseNote")
@Node(jcrType = "santanderbrxm:ReleaseNote")
public class ReleaseNote extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:levels")
    public List<ReleaseNoteLevelOne> getLevels() {
        return getChildBeansByName("santanderbrxm:levels",
                ReleaseNoteLevelOne.class);
    }
}
