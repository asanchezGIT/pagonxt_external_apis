package com.santander.beans;

import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoDocument;

@Node(jcrType="santanderbrxm:basedocument")
public class BaseDocument extends HippoDocument {

}
