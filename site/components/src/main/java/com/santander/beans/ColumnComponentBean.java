package com.santander.beans;

import java.util.List;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoGalleryImageSet;
import org.hippoecm.hst.content.beans.standard.HippoHtml;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ColumnComponent")
@Node(jcrType = "santanderbrxm:ColumnComponent")
public class ColumnComponentBean extends BaseDocument {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:description")
    public HippoHtml getDescription() {
        return getHippoHtml("santanderbrxm:description");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:imageBackground")
    public HippoGalleryImageSet getImageBackground() {
        return getLinkedBean("santanderbrxm:imageBackground",
                HippoGalleryImageSet.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:CallToActionItem")
    public List<CallToActionItem> getCallToActionItem() {
        return getChildBeansByName("santanderbrxm:CallToActionItem",
                CallToActionItem.class);
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:alt")
    public String getAlt() {
        return getSingleProperty("santanderbrxm:alt");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:high")
    public String getHigh() {
        return getSingleProperty("santanderbrxm:high");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:wide")
    public String getWide() {
        return getSingleProperty("santanderbrxm:wide");
    }
}
