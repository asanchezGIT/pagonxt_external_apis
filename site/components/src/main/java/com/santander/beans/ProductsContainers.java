package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import java.util.List;
import com.santander.beans.ProductContainer;

@HippoEssentialsGenerated(internalName = "santanderbrxm:productsContainers")
@Node(jcrType = "santanderbrxm:productsContainers")
public class ProductsContainers extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:title")
    public String getTitle() {
        return getSingleProperty("santanderbrxm:title");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:items")
    public List<ProductContainer> getItems() {
        return getChildBeansByName("santanderbrxm:items",
                ProductContainer.class);
    }
}
