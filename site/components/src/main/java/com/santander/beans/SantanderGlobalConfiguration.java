package com.santander.beans;


import org.hippoecm.hst.content.beans.Node;
import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;

@HippoEssentialsGenerated(internalName = "santanderbrxm:SantanderGlobalConfiguration")
@Node(jcrType = "santanderbrxm:SantanderGlobalConfiguration")
public class SantanderGlobalConfiguration extends BaseDocument{

    @HippoEssentialsGenerated(internalName = "santanderbrxm:id")
	public String getId() { return getSingleProperty("santanderbrxm:id");}
    @HippoEssentialsGenerated(internalName = "santanderbrxm:value")
    public String getValue() { return getSingleProperty("santanderbrxm:value");}
}