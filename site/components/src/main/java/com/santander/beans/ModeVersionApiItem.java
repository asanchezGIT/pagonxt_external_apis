package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoCompound;

@HippoEssentialsGenerated(internalName = "santanderbrxm:ModeVersionApiItem")
@Node(jcrType = "santanderbrxm:ModeVersionApiItem")
public class ModeVersionApiItem extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:mode")
    public String getMode() {
        return getSingleProperty("santanderbrxm:mode");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:version")
    public String getVersion() {
        return getSingleProperty("santanderbrxm:version");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:exposureLocalCountry")
    public String getExposureLocalCountry() {
        return getSingleProperty("santanderbrxm:exposureLocalCountry");
    }
}
