package com.santander.beans;

import org.onehippo.cms7.essentials.dashboard.annotations.HippoEssentialsGenerated;
import org.hippoecm.hst.content.beans.Node;
import org.hippoecm.hst.content.beans.standard.HippoCompound;
import java.util.List;
import com.santander.beans.ModeVersionApiItem;

@HippoEssentialsGenerated(internalName = "santanderbrxm:LocalCountry")
@Node(jcrType = "santanderbrxm:LocalCountry")
public class LocalCountry extends HippoCompound {
    @HippoEssentialsGenerated(internalName = "santanderbrxm:text")
    public String getText() {
        return getSingleProperty("santanderbrxm:text");
    }

    @HippoEssentialsGenerated(internalName = "santanderbrxm:modeVersion")
    public List<ModeVersionApiItem> getModeVersion() {
        return getChildBeansByName("santanderbrxm:modeVersion",
                ModeVersionApiItem.class);
    }
}
