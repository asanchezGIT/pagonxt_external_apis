package com.santander.rest;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.standard.HippoFolderBean;
import org.hippoecm.hst.core.container.HstContainerURL;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class NewsAndArticlesPageResourceTest extends Mockito {

    private static NewsAndArticlesPageResource newsAndArticlesPageResource;
    private static final NewsAndArticlesPageResource newsAndArticlesPageResourceMock = Mockito.mock(NewsAndArticlesPageResource.class, RETURNS_DEEP_STUBS);
    private static final UriInfo uriInfoMock = Mockito.mock(UriInfo.class, RETURNS_DEEP_STUBS);
    private static final MultivaluedMap<String, String> queryParamsMock = Mockito.mock(MultivaluedMap.class, RETURNS_DEEP_STUBS);
    private static final List<String> typesMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static final List<String> offsetMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static final List<String> limitMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static final List<String> siteMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static final HstContainerURL hstContainerURLMock = Mockito.mock(HstContainerURL.class, RETURNS_DEEP_STUBS);
    private static final HstRequestContext hstRequestContextMock = Mockito.mock(HstRequestContext.class, RETURNS_DEEP_STUBS);
    private static final HippoFolderBean scopeMock = Mockito.mock(HippoFolderBean.class, RETURNS_DEEP_STUBS);
    private static final HttpServletRequest httpServletRequestMock = Mockito.mock(HttpServletRequest.class, RETURNS_DEEP_STUBS);
    private static final HttpServletResponse httpServletResponseMock = Mockito.mock(HttpServletResponse.class, RETURNS_DEEP_STUBS);


    @BeforeAll
    static void init() {
        if (newsAndArticlesPageResource == null) {
            newsAndArticlesPageResource = new NewsAndArticlesPageResource();
        }
    }

    @Test
    void getInformations() {
        String typeMockito = "common";
        String siteMockito = "paymentshub";
        int offsetMockito = 1;
        int limitMockito = 10;
        String scopePath = "site/paymentshub";
        String requestPath = "http://localhost:8080/customapi/informations";
        String contentPath = "marketplace";
        try (MockedStatic<RequestContextProvider> theMock = Mockito.mockStatic(RequestContextProvider.class)) {
            when(newsAndArticlesPageResourceMock.getInformations(httpServletRequestMock, httpServletResponseMock, uriInfoMock)).thenCallRealMethod();
            when(uriInfoMock.getQueryParameters()).thenReturn(queryParamsMock);
            when(queryParamsMock.get("types")).thenReturn(typesMock);
            when(queryParamsMock.get("_offset")).thenReturn(offsetMock);
            when(queryParamsMock.get("_limit")).thenReturn(limitMock);
            when(queryParamsMock.get("site")).thenReturn(siteMock);
            when(typesMock.get(0)).thenReturn(typeMockito);
            when(offsetMock.get(0)).thenReturn(String.valueOf(offsetMockito));
            when(limitMock.get(0)).thenReturn(String.valueOf(limitMockito));
            when(siteMock.get(0)).thenReturn(siteMockito);
            theMock.when(RequestContextProvider::get)
                    .thenReturn(hstRequestContextMock);
            when(hstRequestContextMock.getBaseURL()).thenReturn(hstContainerURLMock);
            when(hstContainerURLMock.getRequestPath()).thenReturn(requestPath);
            when(hstRequestContextMock.getResolvedMount().getMount().getContentPath()).thenReturn(contentPath);
            when(scopeMock.getPath()).thenReturn(scopePath);
            when(scopeMock.isHippoDocumentBean()).thenReturn(true);
            when(typesMock.isEmpty()).thenReturn(false);
            newsAndArticlesPageResourceMock.getInformations(httpServletRequestMock, httpServletResponseMock, uriInfoMock);
        } catch (Exception e) {

            assertTrue(Boolean.FALSE, "Invokation method NewsAndArticlesPageResource failed {} " + e.getMessage());
        }
    }

    @Test
    void getInformationsByID() {
        try (MockedStatic<RequestContextProvider> theMock = Mockito.mockStatic(RequestContextProvider.class)) {
            String typeMockito = "common";
            String siteMockito = "paymentshub";
            int offsetMockito = 1;
            int limitMockito = 10;
            String scopePath = "site/paymentshub";
            String uuidMock = "1234234234234234";
            String requestPath = "http://localhost:8080/customapi/informations";

            when(uriInfoMock.getQueryParameters()).thenReturn(queryParamsMock);
            when(newsAndArticlesPageResourceMock.getInformationsByID(httpServletRequestMock, httpServletResponseMock, uriInfoMock, uuidMock)).thenCallRealMethod();
            when(queryParamsMock.get("type")).thenReturn(typesMock);
            when(queryParamsMock.get("_offset")).thenReturn(offsetMock);
            when(queryParamsMock.get("_limit")).thenReturn(limitMock);
            when(queryParamsMock.get("site")).thenReturn(siteMock);
            when(typesMock.get(0)).thenReturn(typeMockito);
            when(typesMock.isEmpty()).thenReturn(false);
            when(offsetMock.get(0)).thenReturn(String.valueOf(offsetMockito));
            when(limitMock.get(0)).thenReturn(String.valueOf(limitMockito));
            when(siteMock.get(0)).thenReturn(siteMockito);
            theMock.when(RequestContextProvider::get).thenReturn(hstRequestContextMock);
            when(hstRequestContextMock.getBaseURL()).thenReturn(hstContainerURLMock);
            when(hstContainerURLMock.getContextPath()).thenReturn("");
            when(newsAndArticlesPageResourceMock.getMountContentBaseBean(hstRequestContextMock)).thenReturn(scopeMock);
            when(hstRequestContextMock.getBaseURL().getRequestPath()).thenReturn(requestPath);

            when(scopeMock.getPath()).thenReturn(scopePath);


            newsAndArticlesPageResourceMock.getInformationsByID(httpServletRequestMock, httpServletResponseMock, uriInfoMock, uuidMock);
        } catch (Exception e) {
            assertTrue(Boolean.FALSE, "Invokation method NewsAndArticlesPageResource failed " + e.getMessage());
        }

    }
}
