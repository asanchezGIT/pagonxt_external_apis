package com.santander.rest;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.standard.HippoFolderBean;
import org.hippoecm.hst.core.container.HstContainerURL;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

class FaqsPageResourceTest extends Mockito{

    private static FaqsPageResource faqsPageResource;
    private static final FaqsPageResource faqsPageResourceMock = Mockito.mock(FaqsPageResource.class, RETURNS_DEEP_STUBS);
    private static final UriInfo uriInfoMock = Mockito.mock(UriInfo.class, RETURNS_DEEP_STUBS);
    private static final MultivaluedMap<String, String> queryParamsMock  = Mockito.mock(MultivaluedMap.class, RETURNS_DEEP_STUBS);
    private static final List<String> typesMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static final List<String> offsetMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static final List<String> limitMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static final  List<String> siteMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static final HstContainerURL hstContainerURLMock =  Mockito.mock(HstContainerURL.class, RETURNS_DEEP_STUBS);
    private static final HstRequestContext hstRequestContextMock = Mockito.mock(HstRequestContext.class, RETURNS_DEEP_STUBS);
    private static final HippoFolderBean scopeMock = Mockito.mock(HippoFolderBean.class, RETURNS_DEEP_STUBS);

    @BeforeAll
    static void init() {
        if (faqsPageResource==null)
        {
            faqsPageResource = new FaqsPageResource();
        }
    }

    @Test
    void getFaqPage() {
        try(MockedStatic<RequestContextProvider> theMock = Mockito.mockStatic(RequestContextProvider.class)) {

            String typeMockito = "common";
            String siteMockito = "paymentshub";
            int offsetMockito = 1;
            int limitMockito = 10;
            String scopePath = "site/paymentshub";

            when(queryParamsMock.get("type")).thenReturn(typesMock);
            when(queryParamsMock.get("_offset")).thenReturn(offsetMock);
            when(queryParamsMock.get("_limit")).thenReturn(limitMock);
            when(queryParamsMock.get("site")).thenReturn(siteMock);
            when(typesMock.get(0)).thenReturn(typeMockito);
            when(typesMock.isEmpty()).thenReturn(false);
            when(offsetMock.get(0)).thenReturn(String.valueOf(offsetMockito));
            when(limitMock.get(0)).thenReturn(String.valueOf(limitMockito));
            when(siteMock.get(0)).thenReturn(siteMockito);
            theMock.when(RequestContextProvider::get).thenReturn(hstRequestContextMock);
            when(hstRequestContextMock.getBaseURL()).thenReturn(hstContainerURLMock);
            when(hstContainerURLMock.getContextPath()).thenReturn("");
            when(scopeMock.getPath()).thenReturn(scopePath);

            faqsPageResourceMock.getFaqPage();


        }catch(Exception e){

            assertTrue(Boolean.FALSE,"Invokation method Documentations failed "+e.getMessage());
        }
    }
}