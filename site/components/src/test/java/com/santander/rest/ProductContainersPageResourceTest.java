package com.santander.rest;


import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.content.beans.query.HstQueryManager;
import org.hippoecm.hst.content.beans.standard.HippoFolderBean;
import org.hippoecm.hst.core.container.HstContainerURL;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.UriInfo;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProductContainersPageResourceTest extends Mockito {

    private static ProductContainersPageResource productContainersPageResource;
    private static final ProductContainersPageResource productContainersPageResourceMock = Mockito.mock(ProductContainersPageResource.class, RETURNS_DEEP_STUBS);
    private static UriInfo uriInfoMock = Mockito.mock(UriInfo.class, RETURNS_DEEP_STUBS);
    private static MultivaluedMap<String, String> queryParamsMock = Mockito.mock(MultivaluedMap.class, RETURNS_DEEP_STUBS);
    private static List<String> typesMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static List<String> offsetMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static List<String> limitMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static List<String> siteMock = Mockito.mock(List.class, RETURNS_DEEP_STUBS);
    private static HstContainerURL hstContainerURLMock = Mockito.mock(HstContainerURL.class, RETURNS_DEEP_STUBS);
    private static HstRequestContext hstRequestContextMock = Mockito.mock(HstRequestContext.class, RETURNS_DEEP_STUBS);
    private static HippoFolderBean scopeMock = Mockito.mock(HippoFolderBean.class, RETURNS_DEEP_STUBS);
    private static HttpServletRequest httpServletRequestMock = Mockito.mock(HttpServletRequest.class, RETURNS_DEEP_STUBS);
    private static HttpServletResponse httpServletResponseMock = Mockito.mock(HttpServletResponse.class, RETURNS_DEEP_STUBS);
    private static HstQueryManager hstQueryManager = Mockito.mock(HstQueryManager.class, RETURNS_DEEP_STUBS);

    @BeforeAll
    static void init() {
        if (productContainersPageResource == null) {
            productContainersPageResource = new ProductContainersPageResource();
        }
    }

    @Test
    void testGetProductContainersByID() {
        try (MockedStatic<RequestContextProvider> theMock = Mockito.mockStatic(RequestContextProvider.class)) {
            String typeMockito = "common";
            String siteMockito = "paymentshub";
            int offsetMockito = 1;
            int limitMockito = 10;
            String scopePath = "site/paymentshub";
            String uuidMock = "1234234234234234";
            String requestPath = "http://localhost:8080/customapi/product_containers";

            when(uriInfoMock.getQueryParameters()).thenReturn(queryParamsMock);
            when(productContainersPageResourceMock.getProductContainersByID(httpServletRequestMock, httpServletResponseMock, uriInfoMock, uuidMock)).thenCallRealMethod();
            when(queryParamsMock.get("type")).thenReturn(typesMock);
            when(queryParamsMock.get("_offset")).thenReturn(offsetMock);
            when(queryParamsMock.get("_limit")).thenReturn(limitMock);
            when(queryParamsMock.get("site")).thenReturn(siteMock);
            when(typesMock.get(0)).thenReturn(typeMockito);
            when(typesMock.isEmpty()).thenReturn(false);
            when(offsetMock.get(0)).thenReturn(String.valueOf(offsetMockito));
            when(limitMock.get(0)).thenReturn(String.valueOf(limitMockito));
            when(siteMock.get(0)).thenReturn(siteMockito);
            theMock.when(RequestContextProvider::get).thenReturn(hstRequestContextMock);
            when(hstRequestContextMock.getBaseURL()).thenReturn(hstContainerURLMock);
            when(hstContainerURLMock.getContextPath()).thenReturn("");
            when(scopeMock.getPath()).thenReturn(scopePath);
            //TODO Nuevos when
            //when(productContainersPageResource.getHstQueryManager(hstRequestContextMock)).thenReturn(hstQueryManager);
            when(productContainersPageResourceMock.getMountContentBaseBean(hstRequestContextMock)).thenReturn(scopeMock);
            when(hstRequestContextMock.getBaseURL().getRequestPath()).thenReturn(requestPath);
            //


            //productContainersPageResourceMock.getProductContainersByID(httpServletRequestMock, httpServletResponseMock, uriInfoMock, eq(anyString()));
            productContainersPageResourceMock.getProductContainersByID(httpServletRequestMock, httpServletResponseMock, uriInfoMock, uuidMock);

        } catch (Exception e) {
            assertTrue(Boolean.FALSE, "Invokation method GetProductContainersByID failed " + e.getMessage());
        }
    }

    @Test
    void testGetProductContainers() {
        String typeMockito = "common";
        String siteMockito = "paymentshub";
        int offsetMockito = 1;
        int limitMockito = 10;
        String scopePath = "site/paymentshub";
        String requestPath = "http://localhost:8080/customapi/product_containers";
        String contentPath = "marketplace";
        try (MockedStatic<RequestContextProvider> theMock = Mockito.mockStatic(RequestContextProvider.class)) {
            when(productContainersPageResourceMock.getProductContainers(httpServletRequestMock, httpServletResponseMock, uriInfoMock)).thenCallRealMethod();
            when(uriInfoMock.getQueryParameters()).thenReturn(queryParamsMock);
            when(queryParamsMock.get("types")).thenReturn(typesMock);
            when(queryParamsMock.get("_offset")).thenReturn(offsetMock);
            when(queryParamsMock.get("_limit")).thenReturn(limitMock);
            when(queryParamsMock.get("site")).thenReturn(siteMock);
            when(typesMock.get(0)).thenReturn(typeMockito);
            when(offsetMock.get(0)).thenReturn(String.valueOf(offsetMockito));
            when(limitMock.get(0)).thenReturn(String.valueOf(limitMockito));
            when(siteMock.get(0)).thenReturn(siteMockito);
            theMock.when(RequestContextProvider::get)
                    .thenReturn(hstRequestContextMock);
            when(hstRequestContextMock.getBaseURL()).thenReturn(hstContainerURLMock);
            when(hstContainerURLMock.getRequestPath()).thenReturn(requestPath);
            when(hstRequestContextMock.getResolvedMount().getMount().getContentPath()).thenReturn(contentPath);
            when(scopeMock.getPath()).thenReturn(scopePath);
            when(scopeMock.isHippoDocumentBean()).thenReturn(true);
            when(typesMock.isEmpty()).thenReturn(false);
            productContainersPageResourceMock.getProductContainers(httpServletRequestMock, httpServletResponseMock, uriInfoMock);
        } catch (Exception e) {

            assertTrue(Boolean.FALSE, "Invokation method testGetProductContainers failed {} " + e.getMessage());
        }
    }
}