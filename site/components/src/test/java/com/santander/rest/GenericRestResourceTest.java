package com.santander.rest;

import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import static org.junit.jupiter.api.Assertions.*;

class GenericRestResourceTest extends Mockito{


    private static GenericRestResource genericRestResource;
    private static final HstRequestContext hstRequestContextMock = Mockito.mock(HstRequestContext.class, RETURNS_DEEP_STUBS);

    @BeforeAll
    static void init() {
        if (genericRestResource==null)
        {
            genericRestResource = new GenericRestResource();
        }
    }

    @Test
    void getChannels() {
        try(MockedStatic<RequestContextProvider> theMock = Mockito.mockStatic(RequestContextProvider.class)) {

            theMock.when(RequestContextProvider::get).thenReturn(hstRequestContextMock);

            genericRestResource.getChannels();


        }catch(Exception e){

            assertTrue(Boolean.FALSE,"Invokation method Documentations failed "+e.getMessage());
        }
    }
}