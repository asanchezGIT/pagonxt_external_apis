package com.santander.sync.kafka;

import com.santander.sync.model.IntegrationLayerConfigurationModel;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EventHubSyncTest {

    private static EventHubSync eventHubSync;
    private static IntegrationLayerConfigurationModel integrationLayerConfigurationModel;

    private static final String TEST_PRODUCERORG="{\"id\":\"junit-uuid-id-payload-producerorg\",\"timestamp\":1606994865,\"payload\":{\"id\":\"junit-uuid-producerorg\",\"name\":\"Junit ProducerOrg\",\"entities\":[{\"id\":\"junit-uuid-entity\",\"name\":\"Santander Digital\"}]}}";
    private static final String TEST_APICONTAINER="{\"id\":\"junit-uuid-id-payload-apicontainer\",\"timestamp\":1614072379,\"payload\":{\"id\":junit-uuid-api-container\",\"name\":\"junit-apiContainer\",\"title\":\"Junit ApiContainer\",\"version\":\"0\",\"containerProducerOrg\":\"junit-uuid-producerorg\",\"deprecated\":false,\"apis\":[{\"id\":junit-uuid-api,\"name\":\"Junit API\",\"title\":\"Junit API\",\"version\":\"1.0.0\",\"description\":\"lorem ipsum dolor\",\"deprecated\":false,\"products\":[{\"id\":6969,\"exposure\":\"INTERNET\",\"type\":\"INTERNAL\",\"mode\":\"SANDBOX\"}]}]}}";
    private static final String TEST_PRODUCT="{\"id\":\"junit-uuid-payload-product\",\"timestamp\":1607008333,\"payload\":{\"id\":junit-id-348,\"name\":\"junit-348\",\"version\":\"1\",\"containerProducerOrg\":\"junit-uuid-producerorg\",\"apis\":[junit-uuid-api],\"deprecated\":false}}";



    @BeforeAll
    static void init()
    {
        if (eventHubSync==null)
        {
            eventHubSync= new EventHubSync();
            integrationLayerConfigurationModel = IntegrationLayerConfigurationModel.builder().build();
        }


    }


    @Test
    void processProducerOrg() {
        try {

            eventHubSync.processProducerOrg(TEST_PRODUCERORG);
        } catch (Exception e)
        {


            assertTrue(Boolean.FALSE,"producerOrg created with errors "+e.getMessage());
        }

        assertTrue(Boolean.TRUE,"producerOrg created without error");

    }

    @Test
    void processAPI() {
        try {

            eventHubSync.processAPI(TEST_APICONTAINER, integrationLayerConfigurationModel);
        } catch (Exception e)
        {

            assertTrue(Boolean.FALSE,"Api created with errors "+e.getMessage());
        }

        assertTrue(Boolean.TRUE,"Api created without error");

    }

    @Test
    void processProduct() {
        try {

            eventHubSync.processProduct(TEST_PRODUCT);
        } catch (Exception e)
        {

            assertTrue(Boolean.FALSE,"Api created with errors "+e.getMessage());
        }

        assertTrue(Boolean.TRUE,"Api created without error");

    }
}