// Register the plugin within the editor.
CKEDITOR.plugins.add( 'alertwarning', {

    // Register the icons. They must match command names.
    icons: 'alertwarning',

    // The plugin initialization logic goes inside this method.
    init: function( editor ) {
        var pluginsDirectory = this.path;
        var hostname = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
		var localGatewayHostname ="http://localhost:8080";
		var devGatewayHostname = "https://brx.marketplace.dev.pagonxt.corp"
        var preGatewayHostname = "https://pre.assets.developer.pagonxt.com"
        var proGatewayHostname = "https://assets.developer.pagonxt.tech"
        var gatewayHostname = "";
        var iconPath = "/site/binaries/content/gallery/santander/common/public/"
        console.log("hostname: ", hostname);
        if(hostname.includes("localhost")) {
            console.log("localhost env detected");
            gatewayHostname = localGatewayHostname;
        }
        else if(hostname.includes("dev")) {
            console.log("DEV env detected");
            gatewayHostname = devGatewayHostname;
        }
        else if(hostname.includes("pre")) {
            console.log("PRE env detected");
            gatewayHostname = preGatewayHostname
        } else {
            console.log("PRO env detected");
            gatewayHostname = proGatewayHostname;
        }
        console.log("gatewayHostname: ", gatewayHostname);
        editor.addContentsCss(pluginsDirectory + 'styles/styles.css');

        // Define the editor command that inserts a timestamp.
        editor.addCommand( 'insertAlertwarning', {
            // Define the function that will be fired when the command is executed.
            exec: function( editor ) {
                // Insert the alert into the document.
                editor.insertHtml("<div class='alert alert-warning'><h4 class='alert-heading'>" +
                                  "<img src=" + gatewayHostname + iconPath + "attention-circle-warning.svg>Caution</h4>" +
                                  "<p>This is a caution message. Injury, damage to property, or financial losses can occur due to specific actions.</p></div>");
            }
        });

        // Create the toolbar button that executes the above command.
        editor.ui.addButton( 'AlertWarning', {
            label: 'Insert alert warning',
            command: 'insertAlertwarning',
            toolbar: 'insert'
        });

    }

});
