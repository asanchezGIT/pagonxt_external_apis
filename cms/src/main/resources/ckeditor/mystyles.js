/**(function () {
  "use strict";
  // The set of styles for the <b>Styles</b> drop-down list.
  CKEDITOR.stylesSet.add( 'default', [
      // Block Styles
      { name: 'Blue Title',       element: 'h3',      styles: { 'color': 'Blue', } },
      { name: 'Red Title',        element: 'h3',      styles: { 'color': 'Red' } },

      // Inline Styles
      { name: 'Marker: Yellow',   element: 'span',    styles: { 'background-color': 'Yellow' } },
      { name: 'Marker: Green',    element: 'span',    styles: { 'background-color': 'Lime' } },

      // Object Styles
      {
          name: 'Image on Left',
          element: 'img',
          attributes: {
              style: 'padding: 5px; margin-right: 5px',
              border: '2',
              align: 'left'
          }
      }
  ] );
}());**/
(function () {
  "use strict";

  CKEDITOR.stylesSet.add('santander', [
    {
      element: 'span',
      name: 'Santander Styles',
      attributes: {
        class: 'highlighted'
      }
    },
    {
          element: 'p',
          name: 'Normal'
    },
    {
          element: 'h1',
          name: 'Encabezado 1'
    },
    {
        element: 'h2',
        name: 'Encabezado 2'
    },
    {
        element: 'h3',
        name: 'Encabezado 3'
    },
    {
         element: 'h4',
         name: 'Encabezado 4'
    },
    {
         element: 'h5',
         name: 'Encabezado 5'
    },
    {
         element: 'h6',
         name: 'Encabezado 6'
    },
    {
         element: 'address',
         name: 'Dirección'
    },
    {
         element: 'pre',
         name: 'Texto preformateado'
    }
  ]);
}());