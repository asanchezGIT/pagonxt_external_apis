package com.santander.sync.modules;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.QueryResult;
import org.hippoecm.repository.api.Document;
import org.hippoecm.repository.api.WorkflowException;
import org.hippoecm.repository.impl.WorkspaceDecorator;
import org.hippoecm.repository.standardworkflow.FolderWorkflow;
import org.hippoecm.repository.util.MavenComparableVersion;
import org.onehippo.forge.content.pojo.mapper.jcr.DefaultJcrContentNodeMapper;
import org.onehippo.forge.content.pojo.model.ContentNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.santander.sync.IntegrationLayerException;
import com.santander.sync.model.ApiContainerPayload;
import com.santander.sync.model.ApiPayload;
import com.santander.sync.model.ProductContainerPayload;
import com.santander.utils.Constants;
import static com.santander.utils.Constants.ESCENARIO_3;
import static com.santander.utils.Constants.IS_INFERIOR;

public class IntegrationLayerExtractUtils {

	private static final String DESCRIPTION_OVERVIEW = "DESCRIPTION_OVERVIEW";
	private IntegrationLayerExtractUtils()
	{
		//empty constructor
	}
	
    private static final String AUTHOR_ICON = "authorIcon";
	private static final String AUTHOR_ICON_MODES = "authorIconModes";
	private static final String AUTHOR_ICON_FACETS = "authorIconFacets";
	private static final String AUTHOR_ICON_VALUES = "authorIconValues";
	private static final String COUNTRY_NODES = "countryNodes";
	private static final String DOCUMENTATION_DOCBASE = "documentationDocbase";
	private static final String MENU_DOCUMENTATION_DOCBASE = "MenuDocumentationDocbase";
	private static final String FEATURES_DOCBASE = "featuresDocbase";
	private static final String GLOBAL_EXPOSURE = "globalExposure";
	private static final String LINK_NODES = "linkNodes";
	private static final String INTEGRATION_PROD_NODE = "integrationProductNode";
	private static final String OLD_DESCRIPTION = "oldDescription";
	private static final String OLD_ICON_FACETS = "oldIconFacets";
	private static final String OLD_ICON_MODES = "oldIconModes";
	private static final String OLD_ICON = "oldIcon";
	private static final String OLD_ICON_VALUES = "oldIconValues";
	private static final String POSTMAN_DOCBASE = "postmanDocbase";
	private static final String YAML_DOCBASE = "yamlDocbase";
	private static final String SANTANDERBRXM_FEATURES_OVERVIEW = "santanderbrxm:featuresOverview";
	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationLayerModule.class);
	
	public static Node extractExistingNode(ApiPayload apiPayload, ApiContainerPayload apiContainerPayload,
			HashMap<String, Object> nodeParams, QueryResult queryResult, Node nodeToCopy, Node updateNode)
			throws RepositoryException, IntegrationLayerException {
		if(queryResult!=null){
			NodeIterator nodeIterator = queryResult.getNodes();
			if (nodeIterator.hasNext())
			{
				Node copyCandidate=null;
				while (nodeIterator.hasNext())
				{
					Node node = nodeIterator.nextNode();
					if (copyCandidate == null ) {
						//get the path of the handle node of the document to copy
						copyCandidate = node;
					} else
					{
						MavenComparableVersion nodeILVersion = new MavenComparableVersion
								(node.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
						MavenComparableVersion nodeCandidateVersion = new MavenComparableVersion(copyCandidate
								.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
						MavenComparableVersion newVersion = new MavenComparableVersion(apiPayload.getVersion());
						if (nodeILVersion.compareTo(nodeCandidateVersion) > 0) {
							copyCandidate = node;
						} else if (nodeILVersion.compareTo(newVersion) == 0)
						{
							updateNode=node;
							LOGGER.info("Escenario 2. existing api node with same id and version. it will be updated");

							extractApiNodeDescription(nodeParams, updateNode);

							extractApiNodeIcon(nodeParams, updateNode);

							extractNodeAuthorIcon(nodeParams, updateNode);

							extractApiLinkNodes(nodeParams, updateNode);

							extractApiYamlNode(nodeParams, updateNode);

							extractNodePostman(nodeParams, updateNode);

							extractApiNodeFeatures(nodeParams, updateNode);

							extractNodeExposure(nodeParams, updateNode);

							extractNodeDocument(nodeParams, updateNode);

							extractApiNodeCountry(nodeParams, updateNode);

							break;
						} else
						{
							//Do nothing
						}
					}
				}

				//there is no equal version, we clone if copy Candidate is superior to the existing one, otherwise,
				nodeToCopy = cloneCopyCandidate(apiPayload,apiContainerPayload, nodeToCopy, updateNode,copyCandidate);



			} else
			{
				LOGGER.info("Escenario 1. There are no previous versions, a new one will be created.");
			}
		}

		return nodeToCopy;
	}

	private static void extractApiNodeCountry(HashMap<String, Object> nodeParams, Node updateNode)
			throws RepositoryException {
		Node nodeCountry=null;	
		try
		{
			nodeCountry=updateNode.getNode(Constants.SANTANDERBRXM_LOCALCOUNTRY);
		} catch (RepositoryException e)
		{
			LOGGER.info("no country {}",e.getMessage());
		}
		if (nodeCountry!=null)
		{
			List<ContentNode> countryNodes=new ArrayList<>();
			DefaultJcrContentNodeMapper contentNodeMapper = new DefaultJcrContentNodeMapper();
			
			NodeIterator countryIterator=updateNode.getNodes(Constants.SANTANDERBRXM_LOCALCOUNTRY);
			
			while (countryIterator.hasNext())
			{
				countryNodes.add((ContentNode)contentNodeMapper.map(countryIterator.nextNode()).clone());
			}
			nodeParams.put(COUNTRY_NODES,countryNodes);
		}
	}

	private static void extractNodeExposure(HashMap<String, Object> nodeParams, Node updateNode)
			throws RepositoryException {
		Node nodeExposure=null;	
		try
		{
			nodeExposure=updateNode.getNode(Constants.SANTANDERBRXM_GLOBALEXPOSURE);
		} catch (RepositoryException e)
		{
			LOGGER.info("no globalexposure {}",e.getMessage());
		}
		if (nodeExposure!=null && nodeExposure.getProperty(Constants.HIPPO_CONTENT)!=null)
		{
			nodeParams.put(GLOBAL_EXPOSURE,nodeExposure.getProperty(Constants.HIPPO_CONTENT).getString());
		}
	}

	private static void extractApiNodeFeatures(HashMap<String, Object> nodeParams, Node updateNode)
			throws RepositoryException {
		Node nodeFeatures=null;	
		try
		{
			nodeFeatures=updateNode.getNode(SANTANDERBRXM_FEATURES_OVERVIEW);
		} catch (RepositoryException e)
		{
			LOGGER.info("no features {}",e.getMessage());
		}
		if (nodeFeatures!=null && nodeFeatures.getProperty(Constants.HIPPO_CONTENT)!=null)
		{
			nodeParams.put(FEATURES_DOCBASE,nodeFeatures.getProperty(Constants.HIPPO_CONTENT).getString());
			
		}
	}

	private static void extractApiYamlNode(HashMap<String, Object> nodeParams, Node updateNode)
			throws RepositoryException {
		Node nodeYaml=null;	
		try
		{
			nodeYaml=updateNode.getNode(Constants.SANTANDERBRXM_YAML);
		} catch (RepositoryException e)
		{
			LOGGER.info("no yalm {}",e.getMessage());
		}
		if (nodeYaml!=null && nodeYaml.getProperty(Constants.HIPPO_DOCBASE)!=null)
		{
			LOGGER.info("yaml docbase {}",nodeYaml.getProperty(Constants.HIPPO_DOCBASE).getValue());
			nodeParams.put(YAML_DOCBASE,nodeYaml.getProperty(Constants.HIPPO_DOCBASE).getValue());
			
		}
	}

	private static void extractApiLinkNodes(HashMap<String, Object> nodeParams, Node updateNode) {
		try
		{
			List<ContentNode> linkNodes=new ArrayList<>();
			DefaultJcrContentNodeMapper contentNodeMapper = new DefaultJcrContentNodeMapper();
			
			NodeIterator linksIterator=updateNode.getNodes(Constants.SANTANDERBRXM_LINKS);
			while (linksIterator.hasNext())
			{
				linkNodes.add((ContentNode)contentNodeMapper.map(linksIterator.nextNode()).clone());
			}
			nodeParams.put(LINK_NODES,linkNodes);
			
			
		} catch (RepositoryException e)
		{
			LOGGER.error("no link {}",e.getMessage());
		}
	}

	private static void extractApiNodeIcon(HashMap<String, Object> nodeParams, Node updateNode)
			throws  RepositoryException {
		Node nodeIcon=null;	
		try
		{
			nodeIcon=updateNode.getNode(Constants.SANTANDERBRXM_ICON);
		} catch (RepositoryException e)
		{
			LOGGER.info("no nod icon {}",e.getMessage());
		}
		if (nodeIcon!=null && nodeIcon.getProperty(Constants.HIPPO_DOCBASE)!=null 
				&& nodeIcon.getProperty(Constants.HIPPO_VALUES)!=null)
		{
			nodeParams.put(OLD_ICON,nodeIcon.getProperty(Constants.HIPPO_DOCBASE).getValue());
			nodeParams.put(OLD_ICON_VALUES,nodeIcon.getProperty(Constants.HIPPO_VALUES).getValues());
			nodeParams.put(OLD_ICON_MODES,nodeIcon.getProperty(Constants.HIPPO_MODES).getValues());
			nodeParams.put(OLD_ICON_FACETS,nodeIcon.getProperty(Constants.HIPPO_FACETS).getValues());
			
		}
	}

	private static void extractApiNodeDescription(HashMap<String, Object> nodeParams, Node updateNode)
			throws  RepositoryException{
		Node nodeDescription=null;	
		try
		{
			nodeDescription=updateNode.getNode("santanderbrxm:descriptionCMS");
		} catch (RepositoryException e)
		{
			LOGGER.info("no description {}",e.getMessage());
		}
		if (nodeDescription!=null && nodeDescription.getProperty(Constants.HIPPO_CONTENT)!=null)
		{
			nodeParams.put(OLD_DESCRIPTION,nodeDescription.getProperty(Constants.HIPPO_CONTENT).getString());
		}
	}

	
	public static void extractProductContainerInfo(Node nodeToCopy,HashMap<String, Object> nodeParams) 
			throws RepositoryException
	{
		
		extractNodeDescription(nodeParams, nodeToCopy);
    	
    	extractNodeIcon(nodeParams, nodeToCopy);
    	
    	extractNodeAuthorIcon(nodeParams, nodeToCopy);
    	
    	extractLinkNodes(nodeParams, nodeToCopy);
    	
    	extractNodePostman(nodeParams, nodeToCopy);

    	extractNodeFeatures(nodeParams, nodeToCopy);
 	
    	extractNodeDocument(nodeParams, nodeToCopy);
    	
    	extractNodeDescriptionOverview(nodeParams,nodeToCopy);

		extractNodeMenuDocumentation(nodeParams, nodeToCopy);
		//08/02/2022
		extractIntegrationProduct(nodeParams, nodeToCopy);
	}

	private static void extractIntegrationProduct(HashMap<String, Object> nodeParams, Node nodeToCopy) {
		try
		{
			List<ContentNode> integrationProductNode=new ArrayList<>();
			DefaultJcrContentNodeMapper contentNodeMapper = new DefaultJcrContentNodeMapper();

			NodeIterator integrationProductIterator=nodeToCopy.getNodes("santanderbrxm:integrationsProduct");
			while (integrationProductIterator.hasNext())
			{
				integrationProductNode.add((ContentNode)contentNodeMapper.map(integrationProductIterator.nextNode()).clone());
			}
			nodeParams.put(INTEGRATION_PROD_NODE,integrationProductNode);
		} catch (RepositoryException e)
		{
			LOGGER.info("no integration product {}",e.getMessage());
		}
	}

	private static void extractNodeMenuDocumentation(HashMap<String, Object> nodeParams, Node nodeToCopy) throws RepositoryException {
		Node nodeDocumentation=null;
		try
		{
			nodeDocumentation=nodeToCopy.getNode(Constants.SANTANDERBRXM_MENU_DOCUMENTATION);
		}
		catch (RepositoryException e) {
			LOGGER.info("no menu documentation {}",e.getMessage());
		}
		if (nodeDocumentation!=null && nodeDocumentation.getProperty(Constants.HIPPO_DOCBASE)!=null)
		{
			nodeParams.put(MENU_DOCUMENTATION_DOCBASE,nodeDocumentation.getProperty(Constants.HIPPO_DOCBASE).getValue());
		}
	}

	public static Document extractNodeToCopyInfo(ApiPayload apiPayload, Session threadSafeSession,
												 HashMap<String, Object> nodeParams, Node nodeToCopy, Document document) throws RepositoryException {

		if (nodeToCopy != null) 
        {
        	
        	extractNodeDescription(nodeParams, nodeToCopy);
        	
        	extractNodeIcon(nodeParams, nodeToCopy);
        	
        	extractNodeAuthorIcon(nodeParams, nodeToCopy);
        	
        	extractLinkNodes(nodeParams, nodeToCopy);
        	
        	extractNodeYAML(nodeParams, nodeToCopy);

        	extractNodePostman(nodeParams, nodeToCopy);

        	extractNodeFeatures(nodeParams, nodeToCopy);

        	Node nodeExposure=null;	
        	extractNodeExposure(nodeParams, nodeToCopy, nodeExposure);
     	
        	extractNodeDocument(nodeParams, nodeToCopy);
        	
        	extractNodeCountry(nodeParams, nodeToCopy);
			
        	
            WorkspaceDecorator workspaceDecorator = (WorkspaceDecorator) threadSafeSession.getWorkspace();
            FolderWorkflow folderWorkflow = (FolderWorkflow) workspaceDecorator.getWorkflowManager()
            		.getWorkflow("threepane", nodeToCopy.getParent().getParent());
            String pathToCopy = nodeToCopy.getParent().getPath();

            if (pathToCopy.indexOf('.') != -1) {
                pathToCopy = pathToCopy.substring(0, pathToCopy.indexOf('.'));
            }
            try
            {
            	document = folderWorkflow.copy(nodeToCopy.getName(), pathToCopy + "." + apiPayload.getVersion());
        
            } catch (WorkflowException | RepositoryException | RemoteException e)
            {
            	LOGGER.warn("cant duplicate api because {}",e.getMessage());
            	document=null;
            }
        }
		return document;
	}

	private static void extractNodeCountry(HashMap<String, Object> nodeParams, Node nodeToCopy)
			throws RepositoryException {
		Node nodeCountry=null;	
		try
		{
			nodeCountry=nodeToCopy.getNode(Constants.SANTANDERBRXM_LOCALCOUNTRY);
		} catch (RepositoryException e)
		{
			LOGGER.info("no country {}",e.getMessage());
		}
		if (nodeCountry!=null)
		{
			List<ContentNode> countryNodes=new ArrayList<>();
			DefaultJcrContentNodeMapper contentNodeMapper = new DefaultJcrContentNodeMapper();
			
			NodeIterator countryIterator=nodeToCopy.getNodes(Constants.SANTANDERBRXM_LOCALCOUNTRY);
			
			while (countryIterator.hasNext())
			{
				countryNodes.add((ContentNode)contentNodeMapper.map(countryIterator.nextNode()).clone());
			}
			
			nodeParams.put(COUNTRY_NODES,countryNodes);
		}
	}

	private static void extractNodeDocument(HashMap<String, Object> nodeParams, Node nodeToCopy)
			throws RepositoryException {
		Node nodeDocumentation=null;	
		try
		{
			nodeDocumentation=nodeToCopy.getNode(Constants.SANTANDERBRXM_DOCUMENTATION);
		} 
		catch (RepositoryException e) {
			LOGGER.info("no documentation {}",e.getMessage());
		}
		if (nodeDocumentation!=null && nodeDocumentation.getProperty(Constants.HIPPO_DOCBASE)!=null)
		{
			nodeParams.put(DOCUMENTATION_DOCBASE,nodeDocumentation.getProperty(Constants.HIPPO_DOCBASE).getValue());	
		}
	}

	private static void extractNodeExposure(HashMap<String, Object> nodeParams, Node nodeToCopy, Node nodeExposure)
			throws  RepositoryException {
		try
		{
			nodeExposure=nodeToCopy.getNode(Constants.SANTANDERBRXM_GLOBALEXPOSURE);
		} 
		catch (RepositoryException e)
		{
			LOGGER.info("no globalExposure {}",e.getMessage());
		}
		if (nodeExposure!=null && nodeExposure.getProperty(Constants.HIPPO_CONTENT)!=null)
		{
			nodeParams.put(GLOBAL_EXPOSURE,nodeExposure.getProperty(Constants.HIPPO_CONTENT).getString());	
		}
	}

	private static void extractNodeFeatures(HashMap<String, Object> nodeParams, Node nodeToCopy)
			throws  RepositoryException {
		Node nodeFeatures=null;	
		try
		{
			nodeFeatures=nodeToCopy.getNode(SANTANDERBRXM_FEATURES_OVERVIEW);
		} 
		catch (RepositoryException e)
		{
			LOGGER.info("no icon {}",e.getMessage());
		}
		if (nodeFeatures!=null && nodeFeatures.getProperty(Constants.HIPPO_CONTENT)!=null)
		{
			nodeParams.put(FEATURES_DOCBASE,nodeFeatures.getProperty(Constants.HIPPO_CONTENT).getString());	
		}
	}

	private static void extractNodePostman(HashMap<String, Object> nodeParams, Node nodeToCopy)
			throws  RepositoryException{
		Node nodePostman=null;	
		try
		{
			nodePostman=nodeToCopy.getNode(Constants.SANTANDERBRXM_POSTMAN);
		} catch (RepositoryException e)
		{
			LOGGER.info("no postman {}",e.getMessage());
		}
		if (nodePostman!=null && nodePostman.getProperty(Constants.HIPPO_DOCBASE)!=null)
		{
			nodeParams.put(POSTMAN_DOCBASE,nodePostman.getProperty(Constants.HIPPO_DOCBASE).getValue());	
		}
	}

	private static void extractNodeYAML(HashMap<String, Object> nodeParams, Node nodeToCopy)
			throws  RepositoryException {
		Node nodeYaml=null;	
		try
		{
			nodeYaml=nodeToCopy.getNode(Constants.SANTANDERBRXM_YAML);
		} catch (RepositoryException e)
		{
			LOGGER.info("no yaml {}",e.getMessage());
		}
		if (nodeYaml!=null && nodeYaml.getProperty(Constants.HIPPO_DOCBASE)!=null)
		{
			nodeParams.put(YAML_DOCBASE,nodeYaml.getProperty(Constants.HIPPO_DOCBASE).getValue());
			
		}
	}

	private static void extractLinkNodes(HashMap<String, Object> nodeParams, Node nodeToCopy) {
		try
		{
			List<ContentNode> linkNodes=new ArrayList<>();
			DefaultJcrContentNodeMapper contentNodeMapper = new DefaultJcrContentNodeMapper();
			
			NodeIterator linksIterator=nodeToCopy.getNodes(Constants.SANTANDERBRXM_LINKS);
			while (linksIterator.hasNext())
			{
				linkNodes.add((ContentNode)contentNodeMapper.map(linksIterator.nextNode()).clone());
			}
			nodeParams.put(LINK_NODES,linkNodes);
		} catch (RepositoryException e)
		{
			LOGGER.info("no link {}",e.getMessage());
		}
	}

	private static void extractNodeAuthorIcon(HashMap<String, Object> nodeParams, Node nodeToCopy)
			throws RepositoryException {
		Node nodeAuthorIcon=null;	
		try
		{
			nodeAuthorIcon=nodeToCopy.getNode(Constants.SANTANDERBRXM_ICON_AUTHOR);
		} catch (RepositoryException e)
		{
			LOGGER.info("no author icon {}",e.getMessage());
		}
		if (nodeAuthorIcon!=null && nodeAuthorIcon.getProperty(Constants.HIPPO_DOCBASE)!=null 
				&& nodeAuthorIcon.getProperty(Constants.HIPPO_VALUES)!=null)
		{
			nodeParams.put(AUTHOR_ICON,nodeAuthorIcon.getProperty(Constants.HIPPO_DOCBASE).getValue());
			nodeParams.put(AUTHOR_ICON_VALUES,nodeAuthorIcon.getProperty(Constants.HIPPO_VALUES).getValues());
			nodeParams.put(AUTHOR_ICON_MODES,nodeAuthorIcon.getProperty(Constants.HIPPO_MODES).getValues());
			nodeParams.put(AUTHOR_ICON_FACETS,nodeAuthorIcon.getProperty(Constants.HIPPO_FACETS).getValues());
			
		}
	}

	private static void extractNodeIcon(HashMap<String, Object> nodeParams, Node nodeToCopy)
			throws RepositoryException {
		Node nodeIcon=null;
		try
		{
			nodeIcon=nodeToCopy.getNode(Constants.SANTANDERBRXM_ICON);
		} catch (RepositoryException e)
		{
			LOGGER.info("no icon {}",e.getMessage());
		}
		if (nodeIcon!=null && nodeIcon.getProperty(Constants.HIPPO_DOCBASE)!=null 
				&& nodeIcon.getProperty(Constants.HIPPO_VALUES)!=null)
		{
			nodeParams.put(OLD_ICON,nodeIcon.getProperty(Constants.HIPPO_DOCBASE).getValue());
			nodeParams.put(OLD_ICON_VALUES,nodeIcon.getProperty(Constants.HIPPO_VALUES).getValues());
			nodeParams.put(OLD_ICON_MODES,nodeIcon.getProperty(Constants.HIPPO_MODES).getValues());
			nodeParams.put(OLD_ICON_FACETS,nodeIcon.getProperty(Constants.HIPPO_FACETS).getValues());
			
		}
	}

	private static void extractNodeDescription(HashMap<String, Object> nodeParams, Node nodeToCopy)
			throws  RepositoryException{
		Node nodeDescription= null;
		try
		{
			nodeDescription=nodeToCopy.getNode("santanderbrxm:descriptionCMS");  
			
		} catch (RepositoryException e)
		{
			LOGGER.info("no description {}",e.getMessage());
		}
		
		if (nodeDescription!=null && nodeDescription.getProperty(Constants.HIPPO_CONTENT)!=null)
		{
			nodeParams.put(OLD_DESCRIPTION,nodeDescription.getProperty(Constants.HIPPO_CONTENT).getString());	
		}
	}

	
	private static void extractNodeDescriptionOverview(HashMap<String, Object> nodeParams, Node nodeToCopy)
			throws  RepositoryException {
		Node nodeDescription= null;
		try
		{
			nodeDescription=nodeToCopy.getNode("santanderbrxm:descriptionOverview");  
			
		} catch (RepositoryException e)
		{
			LOGGER.info("no description overview {}",e.getMessage());
		}
		
		if (nodeDescription!=null && nodeDescription.getProperty(Constants.HIPPO_CONTENT)!=null)
		{
			nodeParams.put(DESCRIPTION_OVERVIEW,nodeDescription.getProperty(Constants.HIPPO_CONTENT).getString());	
		}
	}
	
	public static Node cloneCopyCandidate(ApiContainerPayload apiContainerPayload, Node updateNode, Node nodeToCopy,
			Node copyCandidate)
			throws RepositoryException, IntegrationLayerException {
		if (updateNode==null)
		{
			MavenComparableVersion newVersion = new MavenComparableVersion(apiContainerPayload.getVersion());
		    MavenComparableVersion nodeCandidateVersion = new MavenComparableVersion(copyCandidate
		    		.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		    int comparation = newVersion.compareTo(nodeCandidateVersion);
		    if  (comparation > 0)
		    {
		    	nodeToCopy = copyCandidate;
		        LOGGER.info(ESCENARIO_3+" {} version {}", nodeToCopy.getName()
		        		,nodeToCopy.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		    }
		    else
		    {
		    	throw new IntegrationLayerException("Escenario 4. can't create apiContainer because newVersion " 
		    			+ apiContainerPayload.getVersion() + IS_INFERIOR + copyCandidate.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString() );
		    }
		}
		return nodeToCopy;
	}

	
	public static Node cloneCopyCandidate(ProductContainerPayload productContainerPayload, Node updateNode
			, Node nodeToCopy,Node copyCandidate)
			throws RepositoryException, IntegrationLayerException {
		if (updateNode==null)
		{
			MavenComparableVersion newVersion = new MavenComparableVersion(productContainerPayload.getVersion());
		    MavenComparableVersion nodeCandidateVersion = new MavenComparableVersion(copyCandidate
		    		.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		    int comparation = newVersion.compareTo(nodeCandidateVersion);
		    if  (comparation > 0)
		    {
		    	nodeToCopy = copyCandidate;
		        LOGGER.info("Escenario 3. node to copy: {} version {}", nodeToCopy.getName()
		        		,nodeToCopy.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		    }
		    else
		    {
		    	throw new IntegrationLayerException("Escenario 4. can't create apiContainer because newVersion " 
		    			+ productContainerPayload.getVersion() + "is inferior to current version "
		    			+ copyCandidate.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString() );
		    }
		}
		return nodeToCopy;
	}
	
	
	
	public static Node cloneCopyCandidate(ApiPayload apiPayload, ApiContainerPayload apiContainerPayload,
			Node nodeToCopy, Node updateNode, Node copyCandidate)
			throws RepositoryException, IntegrationLayerException {
		if (updateNode==null)
		{
			MavenComparableVersion newVersion = new MavenComparableVersion(apiPayload.getVersion());
		    MavenComparableVersion nodeCandidateVersion = new MavenComparableVersion(copyCandidate
		    		.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		    int comparation = newVersion.compareTo(nodeCandidateVersion);
		    if  (comparation > 0)
		    {
		    	nodeToCopy = copyCandidate;
		        LOGGER.info("Escenario 3. node to copy: {} version {}", nodeToCopy.getName(),
		        		nodeToCopy.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		    }
		    else
		    {
		    		throw new IntegrationLayerException("Escenario 4. can't create api because newVersion " 
		    				+ apiContainerPayload.getVersion() 
		    				+ "is inferior to current version "
		    				+ copyCandidate.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString() );
		    } 
		    
		}
		return nodeToCopy;
	}

	public static void extractDescription(Node nodeToCopy, HashMap<String, Object> nodeParams, Node nodeDescription)
			throws  RepositoryException {
		nodeDescription = extractNodeDescription(nodeToCopy, nodeDescription);
		
		if (nodeDescription!=null && nodeDescription.getProperty(Constants.HIPPO_CONTENT)!=null)
		{
			nodeParams.put(OLD_DESCRIPTION,nodeDescription.getProperty(Constants.HIPPO_CONTENT).getString());	
		}
	}

	public static void extractNodeIcon(Node nodeToCopy, HashMap<String, Object> nodeParams)
			throws  RepositoryException {
		Node nodeIcon = extractNodeIcon(nodeToCopy);
		if (nodeIcon!=null && nodeIcon.getProperty(Constants.HIPPO_DOCBASE)!=null 
				&& nodeIcon.getProperty(Constants.HIPPO_VALUES)!=null)
		{
			nodeParams.put(OLD_ICON,nodeIcon.getProperty(Constants.HIPPO_DOCBASE).getValue());
			nodeParams.put(OLD_ICON_VALUES,nodeIcon.getProperty(Constants.HIPPO_VALUES).getValues());
			nodeParams.put(OLD_ICON_MODES,nodeIcon.getProperty(Constants.HIPPO_MODES).getValues());
			nodeParams.put(OLD_ICON_FACETS,nodeIcon.getProperty(Constants.HIPPO_FACETS).getValues());
			
		}
	}
	
	public static Node extractNodeIcon(Node updateNode, Node nodeIcon) {
		try
		{
			nodeIcon=updateNode.getNode(Constants.SANTANDERBRXM_ICON);
		} catch (RepositoryException e)
		{
			LOGGER.info("no node icon {}",e.getMessage());
		}
		return nodeIcon;
	}

	public static Node extractNodeDescriptionApi(Node updateNode, Node nodeDescription) {
		try
		{
			nodeDescription=updateNode.getNode(Constants.SANTANDERBRXM_DESCRIPTION);
		} catch (RepositoryException e)
		{
			LOGGER.info("no description  {}",e.getMessage());
		}
		return nodeDescription;
	}

	private static Node extractNodeDescription(Node nodeToCopy, Node nodeDescription) {
		try
		{
			nodeDescription=nodeToCopy.getNode("santanderbrxm:description");  
			
		} catch (RepositoryException e)
		{
			LOGGER.info("no description   {}",e.getMessage());
		}
		return nodeDescription;
	}

	private static Node extractNodeIcon(Node nodeToCopy) {
		Node nodeIcon=null;
		try
		{
			nodeIcon=nodeToCopy.getNode(Constants.SANTANDERBRXM_ICON);
		} catch (RepositoryException e)
		{
			LOGGER.info("no santander icon {}",e.getMessage());
		}
		return nodeIcon;
	}

	public static void checkProducerOrg(ApiContainerPayload apiContainerPayload) throws IntegrationLayerException {
		if (apiContainerPayload.getContainerProducerOrg() == null 
        		|| apiContainerPayload.getContainerProducerOrg().isEmpty()) {
            throw new IntegrationLayerException("Error creating API container. Producer Organization does not exist.");
        }
	}

}
