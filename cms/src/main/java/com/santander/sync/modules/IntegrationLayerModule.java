/**
 * Implements the creation of CMS items from the payloads
 * 
 */

package com.santander.sync.modules;

import com.santander.sync.IntegrationLayerException;
import com.santander.sync.model.*;
import com.santander.utils.Constants;
import com.santander.utils.GlobalConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.RegExUtils;
import org.hippoecm.repository.api.Document;
import org.hippoecm.repository.api.HippoWorkspace;
import org.hippoecm.repository.api.WorkflowException;
import org.hippoecm.repository.api.WorkflowManager;
import org.hippoecm.repository.impl.WorkspaceDecorator;
import org.hippoecm.repository.standardworkflow.FolderWorkflow;
import org.hippoecm.repository.util.MavenComparableVersion;
import org.onehippo.forge.content.exim.core.impl.WorkflowDocumentManagerImpl;
import org.onehippo.forge.content.exim.core.impl.WorkflowDocumentVariantImportTask;
import org.onehippo.forge.content.pojo.model.ContentNode;
import org.onehippo.forge.content.pojo.model.ContentPropertyType;
import org.onehippo.repository.documentworkflow.DocumentWorkflow;
import org.onehippo.repository.modules.DaemonModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.List;
import java.util.Locale;
import javax.annotation.CheckForNull;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;
import static com.santander.utils.Constants.*;

public class IntegrationLayerModule implements DaemonModule {

    private static final String PRODUCTSCONTAINERS = "/productscontainers/";
	private static final String TRUE = "true";
	private static final String AUTHOR_ICON = "authorIcon";
	private static final String AUTHOR_ICON_MODES = "authorIconModes";
	private static final String AUTHOR_ICON_FACETS = "authorIconFacets";
	private static final String AUTHOR_ICON_VALUES = "authorIconValues";
	private static final String COUNTRY_NODES = "countryNodes";
	private static final String DOCUMENTATION_DOCBASE = "documentationDocbase";
	private static final String MENU_DOCUMENTATION_DOCBASE = "MenuDocumentationDocbase";
	private static final String DESCRIPTION_OVERVIEW = "DESCRIPTION_OVERVIEW";
	private static final String FEATURES_DOCBASE = "featuresDocbase";
	private static final String GLOBAL_EXPOSURE = "globalExposure";
	private static final String LINK_NODES = "linkNodes";
	private static final String OLD_DESCRIPTION = "oldDescription";
	private static final String OLD_ICON_FACETS = "oldIconFacets";
	private static final String OLD_ICON_MODES = "oldIconModes";
	private static final String INTEGRATION_PROD_NODE = "integrationProductNode";
	private static final String OLD_ICON = "oldIcon";
	private static final String OLD_ICON_VALUES = "oldIconValues";
	private static final String POSTMAN_DOCBASE = "postmanDocbase";
	private static final String YAML_DOCBASE = "yamlDocbase";
	private static final String PATTERN_SCORE = "-";
	private static final String PATTERN_SPACES = "\\s+";
	private static final String APICONTAINERS = "/apicontainers/";
	private static final String APIS2 = "/apis/";
	private static final String SANTANDERBRXM_FEATURES_OVERVIEW = "santanderbrxm:featuresOverview";
	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationLayerModule.class);
    private static Session session;
	private static final String INTEGRATIONLAYER_PRODUCERORG_FOLDER_PREFIX= "INTEGRATIONLAYER_PRODUCERORG_FOLDER_";
	private static final String INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE_SUBFIX= "_LANGUAGE";
	private static final String INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE= "LANGUAGE_IL";
	private static final String INTEGRATIONLAYER_PRODUCERORG_FOLDER_DEFAULT= "INTEGRATIONLAYER_PRODUCERORG_FOLDER_DEFAULT";
	private static final String INTEGRATIONLAYER_PRODUCERORG_FRAMEWORK = "INTEGRATIONLAYER_PRODUCERORG_FRAMEWORK";
	private static final String INTEGRATIONLAYER_PRODUCERORG_TRANSLATIONS_PREFIX = "INTEGRATIONLAYER_TRANSLATIONS_";
	private static final String INTEGRATIONLAYER_PRODUCERORG_TRANSLATIONS_SUBFIX = "_TRANSLATIONS";
	private static final String DEFAULT ="default";
    private static final String PUBLISH ="publish";
    private static final String INTEGRATIONLAYER_PUBLISH ="INTEGRATIONLAYER_PUBLISH";
    private static final String PROCESSED="processed";
    private static final String SI ="Si";
    private static final String NO ="No";
    
    public static void createOrUpdateProductContainer(ProductContainerPayload productContainerPayload) 
    		throws IntegrationLayerException  {
    	createOrUpdateProductContainer(productContainerPayload,null);
    }
    
    public static void createOrUpdateProductContainer(ProductContainerPayload productContainerPayload,String country) 
    		throws IntegrationLayerException  {
    	
    	LOGGER.info("INICIO createOrUpdateProductContainer");
    	
        Session threadSafeSession = null;
        try {
            WorkflowDocumentManagerImpl documentManager;
            WorkflowDocumentVariantImportTask importTask;

            //For thread safety
            threadSafeSession = null;
            
            String producerOrgId=productContainerPayload.getContainerProducerOrg();
    		    		
    		
    		LOGGER.info("producerOrgId {}",producerOrgId);
    		
    		
            
            threadSafeSession = session.impersonate(new SimpleCredentials(session.getUserID(), new char[0]));
       		GlobalConfiguration configurationUtil = new GlobalConfiguration();
            String default_lang =configurationUtil.getConfigurationById
      				(INTEGRATIONLAYER_PRODUCERORG_FOLDER_PREFIX+producerOrgId
      						+INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE_SUBFIX, session);
            
            
            if (default_lang==null) default_lang="en_gb";
            
           
            
               String searchLocale = null;

			   if(country!=null){
				   searchLocale =  country.replaceFirst("_", "-");
			   }else{
				   searchLocale = default_lang.replaceFirst("_", "-");
			   }
              
               //bugfix, in the case of spain es_es is stored as only es and we need to replace it to have this work
                //this in the future should be revised and create a translation table for more countries.
                if (searchLocale.equalsIgnoreCase(ES_ES))
                {
                	searchLocale="es";
                }
                
                
    			String queryString="SELECT * from [santanderbrxm:ProductContainerItem] where [hippotranslation:locale]='"
    			+searchLocale+"' AND  ([santanderbrxm:PK]='"+RegExUtils
    					.replaceAll(generatePk(productContainerPayload),PATTERN_SPACES, PATTERN_SCORE)
    					.toLowerCase(Locale.UK)+OR_IDL+RegExUtils
    					.replaceAll(productContainerPayload.getId(),PATTERN_SPACES, PATTERN_SCORE)
    					+AND_PK+producerOrgId+"')) "
    					+ ORDERBY_VERSION;
    			
    			Query query = threadSafeSession.getWorkspace().getQueryManager().createQuery(
    					queryString,Query.JCR_SQL2);

    			QueryResult queryResult = query.execute();
    			
    			
    			
    			Node updateNode = null;
    			Node nodeToCopy = null;
    			
                //STEP 1. Find out if we have to
                // 1) Create a brand new instance: If There are no existing items
                // 2) Modify an existing instance: If Exists a apiContainer with the same name and id
                // 3) Clone  an existing instance: If The new apiContainer is of an existing type and a superior version
                // 4) Error: Create a new API of a non existant version.
                

            
    			HashMap<String,Object> nodeParams = new HashMap<>();
                
    			
                nodeToCopy = findCopyAndExtractInfo
                		(productContainerPayload, queryResult, updateNode, nodeToCopy, nodeParams);
                          
                //STEP 2. we do this step to obtain the producer organization or document which are used 
                //to get the path to the node
                // that will we update or create

                //get the producer organization to check its exits
                Node producerOrganization = null;

                producerOrganization =setProducerOrganization(producerOrgId, threadSafeSession,null,searchLocale);


                if (producerOrganization==null)
                {
                	
                	throw new IntegrationLayerException("Producer organization null");
                }

                
                //STEP 3. 
                //Product creation
                 ContentNode contentNode = new ContentNode(productContainerPayload.getName()+"_"+productContainerPayload
                		.getVersion(), Constants.SANTANDERBRXM_PRODUCT_CONTAINER);
                contentNode.setProperty(Constants.SANTANDERBRXM_ID_IL, productContainerPayload.getId());
                contentNode.setProperty(Constants.SANTANDERBRXM_NAME_IL, productContainerPayload.getName());
                contentNode.setProperty(Constants.SANTANDERBRXM_VERSION_IL, productContainerPayload.getVersion());
                contentNode.setProperty(Constants.SANTANDERBRXM_DEPRECATED,
                		TRUE.equals(productContainerPayload.getDeprecated())?SI:NO);
    			contentNode.setProperty(Constants.SANTANDERBRXM_UPDATEDATE,ContentPropertyType.DATE ,
        				new Date().toInstant().toString());
                contentNode.setProperty(Constants.SANTANDERBRXM_PK, productContainerPayload.getName()
                		+ "_" + productContainerPayload.getVersion() 
                		+ "_" + producerOrgId);
                
                contentNode.setProperty(PROCESSED, TRUE);
                
                //path del producto             
                documentManager = new WorkflowDocumentManagerImpl(threadSafeSession);
                importTask = new WorkflowDocumentVariantImportTask(documentManager);
                String nameProduct=productContainerPayload.getName().replace(' ', '-').toLowerCase();

                String newProductOrgPath="";

				newProductOrgPath = importTaskCreateOrUpdate(importTask, contentNode,
						producerOrgId, country, producerOrganization, nameProduct,
						productContainerPayload,searchLocale);

				LOGGER.info("newProductOrgPath: {}", newProductOrgPath);

               
                //nodo carpeta producto
                final HippoWorkspace workspace = (HippoWorkspace) threadSafeSession.getWorkspace();
                final WorkflowManager workflowManager = workspace.getWorkflowManager();
                final Node folder;
                Node documentNode;
                folder = getFolder(threadSafeSession, newProductOrgPath);
				documentNode = getDocumentNode(folder, productContainerPayload);
                
                //Enlace ProdOrg a Producto 
				linkProducerOrgToApiContainer(producerOrganization,newProductOrgPath,threadSafeSession);
				LOGGER.info("DESPUES DE ENLAZAR EL PRODORG AL PRODUCTO");

                
                //Enlace Producto a APisContainers
	                addApiRelationsToProductNode(newProductOrgPath,productContainerPayload,threadSafeSession,country);
	                LOGGER.info("DESPUES DE ENLAZAR LAS APIS AL PRODUCTO");

                //Parametros
				setParamsToProductContainerNode( threadSafeSession, nodeParams, newProductOrgPath);

                //Publicación
				publis(workflowManager, documentNode, productContainerPayload);

                //Multilenguaje
			    createLocalizedProductContainers(productContainerPayload,country,session);
	            
            LOGGER.info("FIN createOrUpdateProductContainer");
           
            	
        } 
        catch (Exception e) {
        	throw new IntegrationLayerException("Error creating product container {} "+e.getMessage());

		} finally {
            if (threadSafeSession != null) {
                threadSafeSession.logout();
            }
        }
    }

	private static void publis(WorkflowManager workflowManager, Node documentNode, ProductContainerPayload productContainerPayload) throws IntegrationLayerException {
		try {
			final DocumentWorkflow documentWorkflow = (DocumentWorkflow) workflowManager
					.getWorkflow(DEFAULT, documentNode);

			final GlobalConfiguration globalConfiguration = new GlobalConfiguration();
			boolean productDeprecated="true".equalsIgnoreCase(productContainerPayload.getDeprecated());

			//Si el producto está duplicado( con o sin publicación directa) o se tiene configurado,  la publicación directa
			if (productDeprecated || (documentWorkflow.hints().get(PUBLISH).equals(true) &&
					globalConfiguration.getConfigurationById(INTEGRATIONLAYER_PUBLISH, session) != null &&
					"true".equalsIgnoreCase(globalConfiguration.getConfigurationById(INTEGRATIONLAYER_PUBLISH, session)))) {
				documentWorkflow.publish();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new IntegrationLayerException("DocumentWorkflow");
		}
    }

	private static Node getDocumentNode(Node folder, ProductContainerPayload productContainerPayload) throws IntegrationLayerException {
		Node documentNode;
		try{

			documentNode = folder.getNode(RegExUtils
					.replaceAll(productContainerPayload.getName(),PATTERN_SPACES, PATTERN_SCORE)
					.toLowerCase(Locale.UK) + "." + productContainerPayload.getVersion().toLowerCase(Locale.UK));
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new IntegrationLayerException("folder.getNode");
		}
		return documentNode;
    }

	private static Node getFolder(Session threadSafeSession, String newProductOrgPath) throws IntegrationLayerException {
		final Node folder;

		try{

		folder = threadSafeSession.getNode(StringUtils.substringBefore(newProductOrgPath,
				PRODUCTSCONTAINERS) + PRODUCTSCONTAINERS);
		}
			catch(Exception e) {
			e.printStackTrace();
			throw new IntegrationLayerException("folder.getNode");
		}

		return folder;

	}


	private static String importTaskCreateOrUpdate(WorkflowDocumentVariantImportTask importTask, ContentNode contentNode,
												   String producerOrgId, String country, Node producerOrganization,
												   String nameProduct, ProductContainerPayload productContainerPayload,
												   String searchLocale) throws IntegrationLayerException {
		String newProductOrgPath="";
		try{
			newProductOrgPath = importTask.createOrUpdateDocumentFromVariantContentNode(
					contentNode, Constants.SANTANDERBRXM_PRODUCT_CONTAINER,
					getProducerOrgFolder(producerOrgId,country,session)
							+ producerOrganization.getName() + PRODUCTSCONTAINERS
							+ nameProduct+"."+productContainerPayload.getVersion().toLowerCase(Locale.UK),
					searchLocale, nameProduct);
		}
		catch(Exception e) {
			LOGGER.error("error createOrUpdateDocumentFromVariantContentNode: {}", e.getMessage());
			e.printStackTrace();
			throw new IntegrationLayerException("Error createOrUpdateDocumentFromVariantContentNode");
		}
		return newProductOrgPath;
    }

	private static void createLocalizedProductContainers(ProductContainerPayload productContainerPayload, String country,
    		Session session) throws IntegrationLayerException {
			try{
			String producerOrgId=productContainerPayload.getContainerProducerOrg();

			if (country!=null)
			{
				return;
			}
			GlobalConfiguration configurationUtil = new GlobalConfiguration();

			String translations=configurationUtil.getConfigurationById(INTEGRATIONLAYER_PRODUCERORG_TRANSLATIONS_PREFIX
					+ producerOrgId
					+ INTEGRATIONLAYER_PRODUCERORG_TRANSLATIONS_SUBFIX, session);

			if (translations!=null && !translations.isEmpty())
			{
				LOGGER.info("Translations retrieved: {}",translations);
				for (String translation: translations.split(","))
				{
					createOrUpdateProductContainer(productContainerPayload, translation);
				}
			} else
			{
					//donothing
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new IntegrationLayerException("createLocalizedProductContainers");
		}
    }
    
    private static void createLocalizedApiContainers(ApiContainerPayload apiContainerPayload, String country
    		,Session session) throws IntegrationLayerException {
    	if (country!=null)
    	{
    		return;
    	}
    	
    	GlobalConfiguration configurationUtil = new GlobalConfiguration();
    	
    	String translations=configurationUtil.getConfigurationById(INTEGRATIONLAYER_PRODUCERORG_TRANSLATIONS_PREFIX 
    			+ apiContainerPayload.getContainerProducerOrg() 
    			+ INTEGRATIONLAYER_PRODUCERORG_TRANSLATIONS_SUBFIX, session);
    	
    	if (translations!=null && !translations.isEmpty())
    	{
    		for (String translation: translations.split(","))
    		{
    			createApiContainer(apiContainerPayload, translation);
    		}	
    	}
    }
    
    private static void createLocalizedProducerOrganization(ProducerOrgPayload producerOrgPayload, String country
    		,Session session) throws IntegrationLayerException {
    	if (country!=null)
    	{
    		return;
    	}
    	
    	GlobalConfiguration configurationUtil = new GlobalConfiguration();
    	
    	String translations=configurationUtil.getConfigurationById(INTEGRATIONLAYER_PRODUCERORG_TRANSLATIONS_PREFIX 
    			+ producerOrgPayload.getId() 
    			+ INTEGRATIONLAYER_PRODUCERORG_TRANSLATIONS_SUBFIX, session);
    	
    	if (translations!=null && !translations.isEmpty())
    	{
    		for (String translation: translations.split(","))
    		{
    			createOrUpdateProducerOrganizationContainer(producerOrgPayload, translation);
    		}	
    	}
    }    

	private static String getProducerOrgFolder(String containerProducerOrg,String country,Session session) 
			throws IntegrationLayerException {

		GlobalConfiguration configurationUtil = new GlobalConfiguration();

		String result =configurationUtil.getConfigurationById
				(INTEGRATIONLAYER_PRODUCERORG_FOLDER_PREFIX+containerProducerOrg, session);
		String default_lang =configurationUtil.getConfigurationById
				(INTEGRATIONLAYER_PRODUCERORG_FOLDER_PREFIX+containerProducerOrg
						+INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE_SUBFIX, session);

		if (result==null || default_lang==null)
		{
			result =configurationUtil.getConfigurationById
					(INTEGRATIONLAYER_PRODUCERORG_FOLDER_DEFAULT+containerProducerOrg, session);
			if (result!=null)
			{
				LOGGER.info("returning producer organization root folder default: {}",result);
				return result;
			}
		}

		if (result==null)
		{
			throw new IntegrationLayerException("No default producer organization for "+containerProducerOrg);
		}
		
		if (country==null)
		{
			result= RegExUtils.replaceFirst(result,INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE, default_lang);

		} else
		{
			result= RegExUtils.replaceFirst(result,INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE, country);
		}
		LOGGER.info("producer organization root folder {}",result);
		return result;
	}

	public static void createOrUpdateProducerOrganizationContainer(ProducerOrgPayload producerOrgPayload)
			throws IntegrationLayerException, IllegalStateException {
		createOrUpdateProducerOrganizationContainer(producerOrgPayload,null);
	}

	public static void createOrUpdateProducerOrganizationContainer(ProducerOrgPayload producerOrgPayload, String country)
			throws IntegrationLayerException, IllegalStateException {
        Session threadSafeSession = null;
        try {
            WorkflowDocumentManagerImpl documentManager;
            WorkflowDocumentVariantImportTask importTask;

            //For thread safety
            threadSafeSession = session.impersonate(new SimpleCredentials(session.getUserID(), new char[0]));
            documentManager = new WorkflowDocumentManagerImpl(threadSafeSession);
            importTask = new WorkflowDocumentVariantImportTask(documentManager);


            ContentNode contentNodeProducerOrg = new ContentNode(producerOrgPayload.getName(), 
            		Constants.SANTANDERBRXM_PRODUCER_ORGANIZATION);
            contentNodeProducerOrg.setProperty(Constants.SANTANDERBRXM_ID_IL, producerOrgPayload.getId());
            contentNodeProducerOrg.setProperty(Constants.SANTANDERBRXM_TITLE_IL, producerOrgPayload.getName());

    		GlobalConfiguration configurationUtil = new GlobalConfiguration();
            String default_lang =configurationUtil.getConfigurationById
    				(INTEGRATIONLAYER_PRODUCERORG_FOLDER_PREFIX+producerOrgPayload.getId()
    						+INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE_SUBFIX, session);

            String searchLocale= country!=null?country.replaceFirst("_", "-"):default_lang.replaceFirst("_", "-");
            
            
            String newProducerOrgPath = importTask.createOrUpdateDocumentFromVariantContentNode(
                    contentNodeProducerOrg, Constants.SANTANDERBRXM_PRODUCER_ORGANIZATION,
					getProducerOrgFolder(producerOrgPayload.getId(),country,session) + RegExUtils
                    .replaceAll(producerOrgPayload.getName(),PATTERN_SPACES, PATTERN_SCORE)
                    .toLowerCase(Locale.UK) + "/" + RegExUtils
                    .replaceAll(producerOrgPayload.getName(),PATTERN_SPACES, PATTERN_SCORE).toLowerCase(Locale.UK),
                    searchLocale
                    
                    , RegExUtils
                    .replaceAll(producerOrgPayload.getName(),PATTERN_SPACES, PATTERN_SCORE).toLowerCase(Locale.UK));
            LOGGER.info("newProducerOrgPath {}",newProducerOrgPath);
            
            threadSafeSession.save();
            
            final HippoWorkspace workspace = (HippoWorkspace) threadSafeSession.getWorkspace();
            final WorkflowManager workflowManager = workspace.getWorkflowManager();

			final Node folder = threadSafeSession.getNode(getProducerOrgFolder(producerOrgPayload.getId(),country,session)
					+  RegExUtils.replaceAll(producerOrgPayload.getName(),PATTERN_SPACES, PATTERN_SCORE)
            		.toLowerCase(Locale.UK));
            Node documentNode = folder.getNode(RegExUtils.replaceAll(producerOrgPayload.getName()
            		,PATTERN_SPACES, PATTERN_SCORE)
            		.toLowerCase(Locale.UK));
            final DocumentWorkflow documentWorkflow = (DocumentWorkflow) workflowManager
            		.getWorkflow(DEFAULT, documentNode);

			final GlobalConfiguration globalConfiguration = new GlobalConfiguration();

			if (documentWorkflow.hints().get(PUBLISH).equals(true) &&
					globalConfiguration.getConfigurationById(INTEGRATIONLAYER_PUBLISH, session) != null &&
					globalConfiguration.getConfigurationById(INTEGRATIONLAYER_PUBLISH, session).equalsIgnoreCase("true")) {
				documentWorkflow.publish();
			}
            
            createLocalizedProducerOrganization(producerOrgPayload, country, threadSafeSession);
            

        } 
        catch (IllegalStateException e) {
			throw e;
        }	
        catch (Exception e) {
			throw new IntegrationLayerException("Error "+e.getMessage());
        } finally {
		if (threadSafeSession != null) {
                threadSafeSession.logout();
            }
        }
    }

	public static void createApiContainer(ApiContainerPayload apiContainerPayload) throws IntegrationLayerException {
		createApiContainer(apiContainerPayload,null);
	}
	
    public static void createApiContainer(ApiContainerPayload apiContainerPayload, String country) 
    		throws IntegrationLayerException {
    	
    	LOGGER.info("INICIO createApiContainer");
    	IntegrationLayerExtractUtils.checkProducerOrg(apiContainerPayload);
        LOGGER.info("checkProducerOrg");

        Session threadSafeSession = null;
        try 
        {

            threadSafeSession = session.impersonate(new SimpleCredentials(session.getUserID(), new char[0]));
    		GlobalConfiguration configurationUtil = new GlobalConfiguration();
            String default_lang =configurationUtil.getConfigurationById
    				(INTEGRATIONLAYER_PRODUCERORG_FOLDER_PREFIX+apiContainerPayload.getContainerProducerOrg()
    						+INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE_SUBFIX, session);
            

            String searchLocale= country!=null?country.replaceFirst("_", "-"):default_lang.replaceFirst("_", "-");

            //bugfix, in the case of spain es_es is stored as only es and we need to replace it to have this work
            //this in the future should be revised and create a translation table for more countries.
            if (searchLocale.equalsIgnoreCase(ES_ES))
            {
            	searchLocale="es";
            }
            
            
            
			String queryString="SELECT * from [santanderbrxm:ApiContainerItem] where [hippotranslation:locale]='"
			+searchLocale+"' AND  ([santanderbrxm:PK]='"+RegExUtils
					.replaceAll(generatePk(apiContainerPayload),PATTERN_SPACES, PATTERN_SCORE)
					.toLowerCase(Locale.UK)+"' OR  ([santanderbrxm:idIL] = '"+RegExUtils
					.replaceAll(apiContainerPayload.getId(),PATTERN_SPACES, PATTERN_SCORE)
					+AND_PK +apiContainerPayload.getContainerProducerOrg()+"')) " + ORDERBY_VERSION;
			
			
			LOGGER.info("querySTring {}",queryString);
			Query query = threadSafeSession.getWorkspace().getQueryManager().createQuery(
					queryString,Query.JCR_SQL2);

			QueryResult queryResult = query.execute();
			Node updateNode = null;
			Node nodeToCopy = null;
            
            
            
            //Step 1. Find out if we have to
            // 1) Create a brand new instance: If There are no existing items
            // 2) Modify an existing instance: If Exists a apiContainer with the same name and id. OJO!!!! EN ESTE CASO SE COMPRUEBA 
            // 3) Clone  an existing instance: If The new apiContainer is of an existing type and a superior version
            // 4) Error: Create a new API of a non existant version.
            
            
            HashMap<String,Object> nodeParams = new HashMap<>();
            
            nodeToCopy = findCopyAndExtractInfo(apiContainerPayload, queryResult, updateNode, nodeToCopy, nodeParams);

            //Step 2. we do this step to obtain the producer organization or document which are used 
            //to get the path to the node
            // that will we update or create
            
            Document document = null;
            Node producerOrganization = null;

            document = processNodeToCopy(apiContainerPayload,  nodeToCopy, nodeParams, document); 
            
              //get the producer organization to check its name
              producerOrganization = setProducerOrganization(apiContainerPayload.getContainerProducerOrg()
            		  , threadSafeSession,null,searchLocale);
              
           
            

            WorkflowDocumentManagerImpl documentManager;
            WorkflowDocumentVariantImportTask importTask;
            
            
            documentManager = new WorkflowDocumentManagerImpl(threadSafeSession);
            importTask = new WorkflowDocumentVariantImportTask(documentManager);

            
            //Step 3. 
            //We have to calculate the deprecation of the apiContainer by checking if there is not deprecated apis
            //To calculate the exposure, we need to extract it from every product asociated to the api.
            boolean apiContainerDeprecated = true;
            String exposureList = "";

            //For each ApiProduct search for the ApiProduct and add it to the Product
            if (apiContainerPayload.getApis()!=null && !apiContainerPayload.getApis().isEmpty())
    		{
	            for (ApiPayload apiPayload : apiContainerPayload.getApis()) {
	                //If all APIs are deprecated we should deprecate the API container too
	                apiContainerDeprecated = setApiContainerDeprecated(apiContainerDeprecated, apiPayload);
	                exposureList = setExposureList(exposureList, apiPayload);
	            }
    		}
            
            
            String newApiContainerPath = createApiContainer(apiContainerPayload, threadSafeSession, document,
					producerOrganization, importTask, apiContainerDeprecated, exposureList,country);

            LOGGER.info("newApiContainerPath {}",newApiContainerPath );
            
            linkProducerOrgToApiContainer(producerOrganization, newApiContainerPath, threadSafeSession);
            LOGGER.info("Description to copy from old version {}",nodeParams.get(OLD_DESCRIPTION));
			//08/02/2022
            setOldDocumentationApi(threadSafeSession, (Value)nodeParams.get(DOCUMENTATION_DOCBASE), newApiContainerPath);
            setOldDescription(threadSafeSession, nodeParams, newApiContainerPath);
            setOldIcon(threadSafeSession, (Value)nodeParams.get(OLD_ICON), (Value[])nodeParams.get(OLD_ICON_VALUES)
            		, (Value[])nodeParams.get(OLD_ICON_MODES), (Value[])nodeParams.get(OLD_ICON_FACETS)
            		, newApiContainerPath);
            
            final HippoWorkspace workspace = (HippoWorkspace) threadSafeSession.getWorkspace();
            final WorkflowManager workflowManager = workspace.getWorkflowManager();

            final Node folder = threadSafeSession.getNode(StringUtils.substringBefore(newApiContainerPath,
            		APICONTAINERS) 
            		+ APICONTAINERS);
            Node documentNode = folder.getNode(RegExUtils.replaceAll(apiContainerPayload.getName()
            		,PATTERN_SPACES, PATTERN_SCORE).toLowerCase(Locale.UK) 
            		+ "." + apiContainerPayload.getVersion().toLowerCase(Locale.UK));
            final DocumentWorkflow documentWorkflow = (DocumentWorkflow) workflowManager
            		.getWorkflow(DEFAULT, document == null ? documentNode : document.getNode(threadSafeSession));

			final GlobalConfiguration globalConfiguration = new GlobalConfiguration();

			//Si el apiContainer está deprecado o esta configurada la publicación automática
			if ( apiContainerDeprecated ||(documentWorkflow.hints().get(PUBLISH).equals(true) &&
					globalConfiguration.getConfigurationById(INTEGRATIONLAYER_PUBLISH, session) != null &&
					globalConfiguration.getConfigurationById(INTEGRATIONLAYER_PUBLISH, session).equalsIgnoreCase("true")) ){
				documentWorkflow.publish();
			}
			
			
			
            createLocalizedApiContainers(apiContainerPayload,country,session);
            
            
        } catch (Exception e) {
            throw new IntegrationLayerException("Error "+e.getMessage());
        } finally {
            if (threadSafeSession != null) {
                threadSafeSession.logout();
            }
        }
    }

	private static void setOldDocumentationApi(Session threadSafeSession, Value oldDocumentation, String newApiContainerPath) throws RepositoryException {
		if (oldDocumentation!=null)
		{
			setDocumentationApiToContainer(oldDocumentation, newApiContainerPath, threadSafeSession);
		}
	}

	private static void setDocumentationApiToContainer(Value oldDocumentation, String newApiContainerPath, Session threadSafeSession) throws RepositoryException {
		LOGGER.info("Setting documantaion Api to api container");

		Node newApiContainerNode = threadSafeSession.getNode(newApiContainerPath);
		for (NodeIterator apiContainerIterator = newApiContainerNode.getNodes(); apiContainerIterator.hasNext(); ) {
			Node apiContainerVariant = apiContainerIterator.nextNode();
			Node documentationNode = apiContainerVariant.addNode(Constants.SANTANDERBRXM_DOCUMENTATION_API, HIPPO_MIRROR);
			documentationNode.setProperty(Constants.HIPPO_DOCBASE, oldDocumentation);
			threadSafeSession.save();
		}
	}

	private static void setOldDescription(Session threadSafeSession, HashMap<String, Object> nodeParams,
			String newApiContainerPath) throws RepositoryException {
		if (nodeParams.get(OLD_DESCRIPTION)!=null)
		{
			setEnrichedTextToApiItem((String)nodeParams.get(OLD_DESCRIPTION),Constants.SANTANDERBRXM_DESCRIPTION
					,newApiContainerPath
					, threadSafeSession);
		}
	}

	private static void setDescriptionOverview(Session threadSafeSession, HashMap<String, Object> nodeParams,
			String newProductContainerPath) throws RepositoryException {
		if (nodeParams.get(DESCRIPTION_OVERVIEW)!=null)
		{
			setEnrichedTextToApiItem((String)nodeParams.get(DESCRIPTION_OVERVIEW),Constants.SANTANDERBRXM_DESCRIPTION_OVERVIEW
					,newProductContainerPath
					, threadSafeSession);
		}
	}

	private static String createApiContainer(ApiContainerPayload apiContainerPayload, Session threadSafeSession,
			Document document, Node producerOrganization, WorkflowDocumentVariantImportTask importTask,
			boolean apiContainerDeprecated, String exposureList, String country)
			throws IntegrationLayerException, RepositoryException, WorkflowException, RemoteException {
		ContentNode contentNodeApiContainer = new ContentNode(apiContainerPayload.getName(), 
				Constants.SANTANDERBRXM_APICONTAINERITEM);
		contentNodeApiContainer.setProperty(Constants.SANTANDERBRXM_ID_IL, apiContainerPayload.getId());
		contentNodeApiContainer.setProperty(Constants.SANTANDERBRXM_VERSION_IL, apiContainerPayload.getVersion());
		contentNodeApiContainer.setProperty(Constants.SANTANDERBRXM_TITLE_IL, apiContainerPayload.getTitle());
		contentNodeApiContainer.setProperty(Constants.SANTANDERBRXM_NAME_IL, apiContainerPayload.getName());
		contentNodeApiContainer.setProperty(Constants.SANTANDERBRXM_PRODUCER_ORGANIZATION,
				apiContainerPayload.getContainerProducerOrg());
		contentNodeApiContainer.setProperty(Constants.SANTANDERBRXM_STATUS, Constants.STATUS_REAL);
		contentNodeApiContainer.setProperty(Constants.SANTANDERBRXM_DEPRECATED, apiContainerDeprecated?SI:NO);
		contentNodeApiContainer.setProperty(Constants.SANTANDERBRXM_TYPE, apiContainerPayload.getType());
		contentNodeApiContainer.setProperty("santanderbrxm:updateDate",ContentPropertyType.DATE ,
				new Date().toInstant().toString());
		if (exposureList!=null && !exposureList.isEmpty())
		{
			contentNodeApiContainer.setProperty("santanderbrxm:availableExposureMode"
					, exposureList.split(","));
		} else
		{
			contentNodeApiContainer.setProperty("santanderbrxm:availableExposureMode"
					, new String[0]);
		}
		contentNodeApiContainer.setProperty(Constants.SANTANDERBRXM_PK, apiContainerPayload.getName()
				+ "_" + apiContainerPayload.getVersion() + "_" + apiContainerPayload.getContainerProducerOrg());
		contentNodeApiContainer.setProperty(PROCESSED, TRUE);
				contentNodeApiContainer.setProperty("santanderbrxm:idBusinessArea", apiContainerPayload.getIdBusinessArea());
		contentNodeApiContainer.setProperty("santanderbrxm:idBusinessDomain", apiContainerPayload.getIdBusinessDomain());
		contentNodeApiContainer.setProperty("santanderbrxm:idServiceDomain", apiContainerPayload.getIdServiceDomain());
		checkProducerOrg(apiContainerPayload, producerOrganization);
		
	
		
		
		
		GlobalConfiguration configurationUtil = new GlobalConfiguration();
        String default_lang =configurationUtil.getConfigurationById
				(INTEGRATIONLAYER_PRODUCERORG_FOLDER_PREFIX+apiContainerPayload.getContainerProducerOrg()
						+INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE_SUBFIX, session);

        String searchLocale= country!=null?country.replaceFirst("_", "-"):default_lang.replaceFirst("_", "-");
        
        
		
		String newApiContainerPath = importTask.createOrUpdateDocumentFromVariantContentNode(
		        contentNodeApiContainer, Constants.SANTANDERBRXM_APICONTAINERITEM,
		        document != null ? document.getNode(threadSafeSession).getPath() :
						(getProducerOrgFolder(apiContainerPayload.getContainerProducerOrg(),country,session) + producerOrganization.getName() +
		                        APICONTAINERS + RegExUtils
		                        .replaceAll(apiContainerPayload.getName(),PATTERN_SPACES,PATTERN_SCORE)
		                        .toLowerCase(Locale.UK) + "." + apiContainerPayload.getVersion().toLowerCase(Locale.UK)),
		        searchLocale, apiContainerPayload.getName());


		//For each ApiProduct search for the ApiProduct and add it to the Product
		if (apiContainerPayload.getApis()!=null && !apiContainerPayload.getApis().isEmpty())
		{
			for (ApiPayload apiPayload : apiContainerPayload.getApis()) {
			    createApiAndLinkToContainer(apiPayload, newApiContainerPath, threadSafeSession, apiContainerPayload,country);
			}
		}
		
		return newApiContainerPath;
	}

	private static void checkProducerOrg(ApiContainerPayload apiContainerPayload, Node producerOrganization)
			throws IntegrationLayerException {
		
		if (producerOrganization==null)
		{
			throw new IntegrationLayerException
			("Producer Org not found: "+ apiContainerPayload.getContainerProducerOrg());
		}
	}

	private static Node findCopyAndExtractInfo(ApiContainerPayload apiContainerPayload, QueryResult queryResult,
			Node updateNode, Node nodeToCopy, HashMap<String, Object> nodeParams)
			throws RepositoryException,  IntegrationLayerException {
		NodeIterator nodeIterator = queryResult.getNodes();
		if (nodeIterator.hasNext())
		{
			Node copyCandidate=null;
			while (nodeIterator.hasNext())
			{
		        Node node = nodeIterator.nextNode();
		        if (copyCandidate == null ) {
		            //get the path of the handle node of the document to copy
		        	copyCandidate = node;
		        } else 
		        {
		            MavenComparableVersion nodeILVersion = new MavenComparableVersion(node
		            		.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		            MavenComparableVersion nodeCandidateVersion = new MavenComparableVersion(copyCandidate
		            		.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		            MavenComparableVersion newVersion = new MavenComparableVersion(apiContainerPayload
		            		.getVersion());
		            if (nodeILVersion.compareTo(nodeCandidateVersion) > 0) {
		            	copyCandidate = node;
		            } else if (nodeILVersion.compareTo(newVersion) == 0) 
		            {
		            	updateNode=node;
		            	Node nodeDescription=null;	
		            	extractNodeDescription(updateNode, nodeParams, nodeDescription);
						//NEW 08/02/22
						extractNodeDocumentationApi(updateNode, nodeParams);
						extractApiNodeIcon(updateNode, nodeParams);
		            	LOGGER.info("Escenario 2. existing node with same id and version. it will be updated");
		            	break;
		            } else
		            {
		            	//Do nothing
		            }
		            
		        }
		    }
		    
		    //there is no equal version, we clone if copy Candidate is superior to the existing one, otherwise, 
		    nodeToCopy = IntegrationLayerExtractUtils.cloneCopyCandidate(apiContainerPayload, updateNode, nodeToCopy
		    		, copyCandidate);
		    
		    
		    
		} else
		{
		    LOGGER.info("Escenario 1. There are no previous versions, a new one will be created.");
		}
		return nodeToCopy;
	}

	private static Node findCopyAndExtractInfo(ProductContainerPayload productContainerPayload, QueryResult queryResult,
			Node updateNode, Node nodeToCopy, HashMap<String, Object> nodeParams)
			throws RepositoryException,  IntegrationLayerException {
		NodeIterator nodeIterator = queryResult.getNodes();
		if (nodeIterator.hasNext())
		{
			Node copyCandidate=null;
			while (nodeIterator.hasNext())
			{
		        Node node = nodeIterator.nextNode();

		     		        if (copyCandidate == null ) {
		            //get the path of the handle node of the document to copy
		        	copyCandidate = node;
		        } else 
		        {
		            MavenComparableVersion nodeILVersion = new MavenComparableVersion(node
		            		.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		            MavenComparableVersion nodeCandidateVersion = new MavenComparableVersion(copyCandidate
		            		.getProperty(Constants.SANTANDERBRXM_VERSION_IL).getString());
		            MavenComparableVersion newVersion = new MavenComparableVersion(productContainerPayload
		            		.getVersion());
		            if (nodeILVersion.compareTo(nodeCandidateVersion) > 0) {
		            	copyCandidate = node;
		            } else if (nodeILVersion.compareTo(newVersion) == 0) 
		            {
		            	updateNode=node;
		            	IntegrationLayerExtractUtils.extractProductContainerInfo(node, nodeParams);
		            	
		            	
		            	LOGGER.info("Escenario 2. existing node with same id and version. it will be updated");
		            	break;
		            } else
		            {
		            	//Do nothing
		            }
		            
		        }
		    }
		    
		    //there is no equal version, we clone if copy Candidate is superior to the existing one, otherwise, 
		    nodeToCopy = IntegrationLayerExtractUtils.cloneCopyCandidate(productContainerPayload, updateNode, nodeToCopy
		    		, copyCandidate);
		    
		    
		    
		} else
		{
		    LOGGER.info("Escenario 1. There are no previous versions, a new one will be created.");
		}
		return nodeToCopy;
	}

	public static void extractNodeDocumentationApi(Node nodeToCopy, HashMap<String, Object> nodeParams) throws RepositoryException {
		Node nodeDocumentation = extractNodeDocumnetation(nodeToCopy);
		if (nodeDocumentation!=null && nodeDocumentation.getProperty(Constants.HIPPO_DOCBASE)!=null)
		{
			nodeParams.put(DOCUMENTATION_DOCBASE,nodeDocumentation.getProperty(Constants.HIPPO_DOCBASE).getValue());
		}
	}

	private static Node extractNodeDocumnetation(Node nodeToCopy) {
		Node nodeDocument=null;
		try
		{
			nodeDocument=nodeToCopy.getNode(SANTANDERBRXM_DOCUMENTATION_API);
		} catch (RepositoryException e)
		{
			LOGGER.info("no santander documentation api in ApiContainer {}",e.getMessage());
		}
		return nodeDocument;
	}

	private static void extractApiNodeIcon(Node updateNode, HashMap<String, Object> nodeParams)
			throws RepositoryException {
		Node nodeIcon=null;	
		nodeIcon = IntegrationLayerExtractUtils.extractNodeIcon(updateNode, nodeIcon);
		if (nodeIcon!=null && nodeIcon.getProperty(Constants.HIPPO_DOCBASE)!=null
				&& nodeIcon.getProperty(Constants.HIPPO_VALUES)!=null)
		{
			nodeParams.put(OLD_ICON,nodeIcon.getProperty(Constants.HIPPO_DOCBASE).getValue());
			nodeParams.put(OLD_ICON_VALUES,nodeIcon.getProperty(Constants.HIPPO_VALUES).getValues());
			nodeParams.put(OLD_ICON_MODES,nodeIcon.getProperty(Constants.HIPPO_MODES).getValues());
			nodeParams.put(OLD_ICON_FACETS,nodeIcon.getProperty(Constants.HIPPO_FACETS).getValues());
			
		}
	}

	private static void extractNodeDescription(Node updateNode, HashMap<String, Object> nodeParams,
			Node nodeDescription) throws  RepositoryException{
		nodeDescription = IntegrationLayerExtractUtils.extractNodeDescriptionApi(updateNode, nodeDescription);
		if (nodeDescription!=null && nodeDescription.getProperty(Constants.HIPPO_CONTENT)!=null)
		{
			nodeParams.put(OLD_DESCRIPTION,nodeDescription.getProperty(Constants.HIPPO_CONTENT)
					.getString());
		}
	}

	private static Document processNodeToCopy(ApiContainerPayload apiContainerPayload, 
			Node nodeToCopy, HashMap<String, Object> nodeParams, Document document) throws
			RepositoryException,  WorkflowException, RemoteException {
		if (nodeToCopy != null) 
		{
		    WorkspaceDecorator workspaceDecorator = (WorkspaceDecorator) session.getWorkspace();
		    FolderWorkflow folderWorkflow = (FolderWorkflow) workspaceDecorator.getWorkflowManager()
		    		.getWorkflow("threepane", nodeToCopy.getParent().getParent());
		    String pathToCopy = nodeToCopy.getParent().getPath();

		    if (pathToCopy.indexOf('.') != -1) {
		        pathToCopy = pathToCopy.substring(0, pathToCopy.indexOf('.'));
		    }

		    LOGGER.info("Copy node {} into {}.{}",nodeToCopy.getName(),pathToCopy, 
		    		apiContainerPayload.getVersion());
		    try
		    {
		    	document = folderWorkflow.copy(nodeToCopy.getName(), pathToCopy 
		    			+ "." + apiContainerPayload.getVersion().toLowerCase(Locale.UK));
		    	
		    	
		    	//We have to manually clone some nodes of the original content
		    	
		    	Node nodeDescription= null;
		    	IntegrationLayerExtractUtils.extractDescription(nodeToCopy, nodeParams, nodeDescription);

		    	IntegrationLayerExtractUtils.extractNodeIcon(nodeToCopy, nodeParams);
		    	
		    } catch (RepositoryException e)
		    {
		    	LOGGER.warn("cant duplicate node {} , using apiContainer data to update/create node",e.getMessage());
		    	document=null;
		    }
		}
		return document;
	}
	
	public static boolean exitsApisContainers(ProductContainerPayload productContainerPayload) throws  RepositoryException {
		//nota: Se podría meter como comprobación adicional que existe en los tres idiomas
		
		
		
		List<String> apiList=productContainerPayload.getApisContainers();
		if (apiList.isEmpty()) {
			
			return true;
		}
		else {
			
			
			//Compruebo que existen todos los apiContainers que se quiere asocial al ProductContainer
			@CheckForNull
			Session threadSafeSession = session.impersonate(new SimpleCredentials(session.getUserID(), new char[0]));
			try {

				boolean existAllApis=true;
	            
				for (String apiId : productContainerPayload.getApisContainers())
						
				{
					
					LOGGER.info("searching apiId {}",apiId);
		            
					if (!existApiContainer(apiId)) {
						existAllApis=false;
						break;
					}
					
				}
				
				threadSafeSession.logout();

				return  existAllApis;
			}
			catch(Exception e) {
				threadSafeSession.logout();
				return false;
			}
			
			
		}
		
	}

	public static boolean existProducerOrganization(ApiContainerPayload apiContainerPayload) throws  RepositoryException {
		
		LOGGER.info("INICIO existProducerOrganizationAPI");
		@CheckForNull
		Session threadSafeSession = session.impersonate(new SimpleCredentials(session.getUserID(), new char[0]));
		try{

			LOGGER.info("ANTES producerOrgId");
			String producerOrgId=apiContainerPayload.getContainerProducerOrg();

				  
			  // Se quita la condición de que esté publicado el prodOrganizarion	
			  Query prodOrgQuery = threadSafeSession.getWorkspace().getQueryManager().createQuery(QUERY_PRODUCER + producerOrgId + "']", Query.XPATH);

			  QueryResult prodOrgQueryResult = prodOrgQuery.execute();
			  
			  
			  LOGGER.info("FIN existProducerOrganization");
			  if (prodOrgQueryResult.getNodes().getSize()==0) {
				  threadSafeSession.logout();
				  return false;
			  }
			  else {
				  threadSafeSession.logout();
				  return true;
			  }
		}
		catch(Exception e)
		{
			LOGGER.error("Exception: {}",  e.getMessage());
			  threadSafeSession.logout();
			 return false;
		}
	}

	public static boolean existProducerOrganization(ProductContainerPayload productContainerPayload) throws  RepositoryException {
		LOGGER.info("INICIO existProducerOrganizationAPI");

		@CheckForNull
		Session threadSafeSession = session.impersonate(new SimpleCredentials(session.getUserID(), new char[0]));
		try{

			String producerOrgId=productContainerPayload.getContainerProducerOrg();

			  Query prodOrgQuery = threadSafeSession.getWorkspace().getQueryManager().createQuery(QUERY_PRODUCER + producerOrgId + "']", Query.XPATH);

			  QueryResult prodOrgQueryResult = prodOrgQuery.execute();
			
			  if (prodOrgQueryResult.getNodes().getSize()==0) {
				  threadSafeSession.logout();
				  LOGGER.info(" NO existProducerOrganizationAPI");
				  return false;
			  }
			  else {
				  threadSafeSession.logout();
				  LOGGER.info("SI existProducerOrganizationAPI");
				  return true;
			  }
		}
		catch(Exception e)
		{
			 threadSafeSession.logout();
			 return false;
		}
	}
		
	public static boolean existApiContainer(String  idApi) throws  RepositoryException {

			@CheckForNull
			Session threadSafeSession = session.impersonate(new SimpleCredentials(session.getUserID(), new char[0]));
			try{

				LOGGER.info("idApi{}",idApi);

				  Query apiContainerQuery = threadSafeSession.getWorkspace().getQueryManager().createQuery(
				            "/jcr:root/content/documents/santander//element"
				            + "(*, santanderbrxm:ApiContainerItem)[@santanderbrxm:idIL ='" 
				  + idApi + "']", Query.XPATH);
				  
				  QueryResult apiContainerQueryResult = apiContainerQuery.execute();
				  
				  
				  
				  if (apiContainerQueryResult.getNodes().getSize()==0) {
					  threadSafeSession.logout();
					
					  return false;
				  }
				  else {
					  threadSafeSession.logout();
					  return true;
				  }
			}
			catch(Exception e)
			{
				LOGGER.error("Exception: {}",  e.getMessage());
				  threadSafeSession.logout();
				 return false;
			}
		}	
		
	
	private static Node setProducerOrganization(String producerOrgId, Session threadSafeSession,
			Node producerOrganization, String locale) throws  IntegrationLayerException {
		try{
    	LOGGER.info("INICIO setProducerOrganization. Locale{}",locale);
		  
		  Query prodOrgQuery = threadSafeSession.getWorkspace().getQueryManager().createQuery(
		            "/jcr:root/content/documents/santander//element"
		            + "(*, santanderbrxm:ProducerOrganizationContainer)[@santanderbrxm:idIL ='" 
		  + producerOrgId  + "' and @hippotranslation:locale = '"+locale+"']", Query.XPATH);
		  
		 
		  
		  LOGGER.info("prodOrgQuery{}",prodOrgQuery);
		  QueryResult prodOrgQueryResult = prodOrgQuery.execute();
		  
	     
		  NodeIterator nodeIteratorTest = prodOrgQueryResult.getNodes();
		  LOGGER.info("num de prodEncontrados: {}",nodeIteratorTest.getSize());
				  
		  for (NodeIterator nodeIterator = prodOrgQueryResult.getNodes(); nodeIterator.hasNext(); ) {
			  
		        producerOrganization = nodeIterator.nextNode();
		  }
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new IntegrationLayerException("Error setProducerOrganization");
		}
		return producerOrganization;
	}

	private static String setExposureList(String exposureList, ApiPayload apiPayload) {
		if (apiPayload.getProducts()!=null)
		{
			for(ProductAPIPayload productAPIPayload : apiPayload.getProducts()) {
				if (productAPIPayload.getMode()!=null && !exposureList.contains(productAPIPayload.getMode()))
				{
					exposureList=exposureList.isEmpty()? productAPIPayload.getMode() :
							(exposureList + "," + productAPIPayload.getMode());
				}
			}
		}
		return exposureList;
	}

	private static boolean setApiContainerDeprecated(boolean apiContainerDeprecated, ApiPayload apiPayload) {
		if(apiPayload.getDeprecated()!=null && "false".equals(apiPayload.getDeprecated())){
			apiContainerDeprecated = false;
		}
		return apiContainerDeprecated;
	}

	private static void createApiAndLinkToContainer(ApiPayload apiPayload, String newApiContainerPath,
    		Session threadSafeSession, ApiContainerPayload apiContainerPayload, String country) 
    				throws IntegrationLayerException, RepositoryException, WorkflowException,RemoteException {


        //STEP 1, search exisiting API and clone it
        // 1) Create a brand new instance: If There are no existing items
        // 2) Modify an existing instance: If Exists a apiContainer with the same name and id
        // 3) Clone  an existing instance: If The new apiContainer is of an existing type and a superior version
        // 4) Error: Create a new API of a non existant version.
        
    	HashMap<String, Object> nodeParams= new HashMap<>();

    	GlobalConfiguration configurationUtil = new GlobalConfiguration();
        String default_lang =configurationUtil.getConfigurationById
				(INTEGRATIONLAYER_PRODUCERORG_FOLDER_PREFIX+apiContainerPayload.getContainerProducerOrg()
						+INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE_SUBFIX, session);

        String searchLocale= country!=null?country.replaceFirst("_", "-"):default_lang.replaceFirst("_", "-");  
        if (searchLocale.equalsIgnoreCase("es-es"))
        {
        	searchLocale="es";
        }
    	

		String queryString="SELECT * from [santanderbrxm:ApiItem] where [hippotranslation:locale]='"+searchLocale
				+"' and ([santanderbrxm:PK]='"+RegExUtils
				.replaceAll(generatePk(apiPayload,apiContainerPayload.getContainerProducerOrg())
						,PATTERN_SPACES, PATTERN_SCORE)
				.toLowerCase(Locale.UK)+"' OR  ([santanderbrxm:idIL] = '"+RegExUtils
				.replaceAll(apiPayload.getId(),PATTERN_SPACES, PATTERN_SCORE)
				+"'  and [santanderbrxm:PK] like '%"+apiContainerPayload.getContainerProducerOrg()+"')) "
				+ "order by [santanderbrxm:versionIL], [santanderbrxm:versionCMS]";
		LOGGER.info("Api Item queryString {} ", queryString);

		Query query = threadSafeSession.getWorkspace().getQueryManager().createQuery(
				queryString, Query.JCR_SQL2);
		QueryResult queryResult = query.execute();
		Node nodeToCopy = null;
		Node updateNode = null;
        nodeToCopy = IntegrationLayerExtractUtils.extractExistingNode(apiPayload, apiContainerPayload, 
        		nodeParams, queryResult, nodeToCopy,
				updateNode);

        Document document = null;

        document = IntegrationLayerExtractUtils.extractNodeToCopyInfo(apiPayload, threadSafeSession, 
        		nodeParams, nodeToCopy, document);
        
        //Step 2, creates the new Api node
        WorkflowDocumentManagerImpl documentManager;
        WorkflowDocumentVariantImportTask importTask;
        
        documentManager = new WorkflowDocumentManagerImpl(threadSafeSession);
        importTask = new WorkflowDocumentVariantImportTask(documentManager);

        ContentNode contentNodeApi = new ContentNode(apiPayload.getName(), "santanderbrxm:ApiItem");
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_ID_IL, apiPayload.getId());
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_VERSION_IL, apiPayload.getVersion());
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_NAME, apiPayload.getName());
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_TITLE_IL, apiPayload.getTitle());
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_NAME_IL, apiPayload.getName());
        LOGGER.info("description IL {}",apiPayload.getDescription());
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_STATUS, Constants.STATUS_REAL);
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_TITLE, apiPayload.getTitle());
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_DESCRIPTION, apiPayload.getDescription());
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_DEPRECATED,
        		TRUE.equals(apiPayload.getDeprecated())?SI:NO);
        contentNodeApi.setProperty("santanderbrxm:updateDate",ContentPropertyType.DATE 
        		, new Date().toInstant().toString());
        contentNodeApi.setProperty(Constants.SANTANDERBRXM_PK, apiPayload.getName() + "_" + apiPayload.getVersion()
        	+ "_" + apiContainerPayload.getContainerProducerOrg());
        contentNodeApi.setProperty(PROCESSED, TRUE);

        LOGGER.info("Api name {}",apiPayload.getName());
        LOGGER.info("Version {}",apiPayload.getVersion());

        String newApiPath = importTask.createOrUpdateDocumentFromVariantContentNode(
                contentNodeApi, "santanderbrxm:ApiItem",
                document != null ? document.getNode(threadSafeSession).getPath() :
                        (StringUtils.substringBeforeLast(newApiContainerPath, "/") 
                        + APIS2 
                        + RegExUtils.replaceAll(apiPayload.getName(),PATTERN_SPACES, PATTERN_SCORE).toLowerCase() 
                        + "." + apiPayload.getVersion().toLowerCase(Locale.UK)),
                searchLocale, apiPayload.getName());

        Node newApiNode = threadSafeSession.getNode(newApiPath);

		//Step 3 creates de productsAPI nodes and bound them to the Api item
		setParamsToApiNode(apiPayload, threadSafeSession, nodeParams, newApiPath, newApiNode);
        
        //Step 4, bounds the created Api to the API Container
        boundApiToApiContainer(newApiContainerPath, threadSafeSession, newApiPath);

        //Step 5, publishes the api item
        publishApiItem(apiPayload, threadSafeSession, document, newApiPath);
    }

    private static void setParamsToApiNode(ApiPayload apiPayload,Session threadSafeSession, HashMap<String, Object> nodeParams, String newApiPath, Node newApiNode)
			throws RepositoryException, IntegrationLayerException {
		setDescription(apiPayload, threadSafeSession, newApiNode);
        
        
        setOldDescription(threadSafeSession, (String)nodeParams.get(OLD_DESCRIPTION), newApiPath);
        setOldIcon(threadSafeSession, (Value)nodeParams.get(OLD_ICON), 
        		(Value[])nodeParams.get(OLD_ICON_VALUES), (Value[])nodeParams.get(OLD_ICON_MODES), 
        		(Value[])nodeParams.get(OLD_ICON_FACETS), newApiPath);
        
        setLinkNodes(threadSafeSession, (List<ContentNode>)nodeParams.get(LINK_NODES), newApiPath);

        setYaml(threadSafeSession, (Value)nodeParams.get(YAML_DOCBASE), newApiPath);

        setPostman(threadSafeSession, (Value)nodeParams.get(POSTMAN_DOCBASE), newApiPath);

		//addIdFrameworkCMS(apiPayload, threadSafeSession, newApiPath);

		setFeatures(threadSafeSession, (String)nodeParams.get(FEATURES_DOCBASE), newApiPath);

        setGlobalExposure(threadSafeSession, (String)nodeParams.get(GLOBAL_EXPOSURE), newApiPath);
        
        if (nodeParams.get(DOCUMENTATION_DOCBASE)!=null)
        {
        	HashMap<String,Object> tempParams=new HashMap<>();
			tempParams.put(Constants.HIPPO_DOCBASE, nodeParams.get(DOCUMENTATION_DOCBASE));
        	setDocToNode(tempParams, Constants.SANTANDERBRXM_DOCUMENTATION,Constants.HIPPO_MIRROR
        			, newApiPath, threadSafeSession);
        }
        
        if (nodeParams.get(AUTHOR_ICON)!=null)
        {
        	HashMap<String,Object> tempParams=new HashMap<>();
			tempParams.put(Constants.HIPPO_VALUES, nodeParams.get(AUTHOR_ICON_VALUES));
			tempParams.put(Constants.HIPPO_DOCBASE, nodeParams.get(AUTHOR_ICON));
			tempParams.put(Constants.HIPPO_MODES, nodeParams.get(AUTHOR_ICON_MODES));
			tempParams.put(Constants.HIPPO_FACETS, nodeParams.get(AUTHOR_ICON_FACETS));
			
        	setDocToNode(tempParams, 
        			Constants.SANTANDERBRXM_ICON_AUTHOR
        			,IMAGELINK , newApiPath, threadSafeSession);
        }
        
        if (nodeParams.get(COUNTRY_NODES)!=null)
        {
        	createCountryNodes((List<ContentNode>)nodeParams.get(COUNTRY_NODES),newApiPath,threadSafeSession);
        }
	}


	private static void setParamsToProductContainerNode( Session threadSafeSession, HashMap<String, Object> nodeParams, String newProductContainerPath)
			throws  IntegrationLayerException {
	try{

		setOldDescription(threadSafeSession, (String)nodeParams.get(OLD_DESCRIPTION), newProductContainerPath);
		setOldIcon(threadSafeSession, (Value)nodeParams.get(OLD_ICON),
		(Value[])nodeParams.get(OLD_ICON_VALUES), (Value[])nodeParams.get(OLD_ICON_MODES),
		(Value[])nodeParams.get(OLD_ICON_FACETS), newProductContainerPath);

		setLinkNodes(threadSafeSession, (List<ContentNode>)nodeParams.get(LINK_NODES), newProductContainerPath);


		setPostman(threadSafeSession, (Value)nodeParams.get(POSTMAN_DOCBASE), newProductContainerPath);
		//08/02/22 - set integrationProduct
		setIntegrationProduct(threadSafeSession, (List<ContentNode>)nodeParams.get(INTEGRATION_PROD_NODE), newProductContainerPath);

		//set Menu Documentation
		if (nodeParams.get(MENU_DOCUMENTATION_DOCBASE)!=null)
		{
			HashMap<String,Object> tempParams=new HashMap<>();
			tempParams.put(Constants.HIPPO_DOCBASE, nodeParams.get(MENU_DOCUMENTATION_DOCBASE));
			setDocToNode(tempParams, Constants.SANTANDERBRXM_MENU_DOCUMENTATION,Constants.HIPPO_MIRROR
					, newProductContainerPath, threadSafeSession);
		}

		setFeatures(threadSafeSession, (String)nodeParams.get(FEATURES_DOCBASE), newProductContainerPath);

		setGlobalExposure(threadSafeSession, (String)nodeParams.get(GLOBAL_EXPOSURE), newProductContainerPath);

		if (nodeParams.get(DOCUMENTATION_DOCBASE)!=null)
		{
			HashMap<String,Object> tempParams=new HashMap<>();
			tempParams.put(Constants.HIPPO_DOCBASE, nodeParams.get(DOCUMENTATION_DOCBASE));
			setDocToNode(tempParams, Constants.SANTANDERBRXM_DOCUMENTATION,Constants.HIPPO_MIRROR
					, newProductContainerPath, threadSafeSession);
		}

		if (nodeParams.get(AUTHOR_ICON)!=null)
		{
			HashMap<String,Object> tempParams=new HashMap<>();
			tempParams.put(Constants.HIPPO_VALUES, nodeParams.get(AUTHOR_ICON_VALUES));
			tempParams.put(Constants.HIPPO_DOCBASE, nodeParams.get(AUTHOR_ICON));
			tempParams.put(Constants.HIPPO_MODES, nodeParams.get(AUTHOR_ICON_MODES));
			tempParams.put(Constants.HIPPO_FACETS, nodeParams.get(AUTHOR_ICON_FACETS));

			setDocToNode(tempParams,
					Constants.SANTANDERBRXM_ICON_AUTHOR
					,IMAGELINK , newProductContainerPath, threadSafeSession);
		}


		setDescriptionOverview(threadSafeSession, nodeParams, newProductContainerPath);

	}catch(Exception e) {
		e.printStackTrace();
		throw new IntegrationLayerException("setParamsToProductContainerNode");
	}
}

	private static void publishApiItem(ApiPayload apiPayload, Session threadSafeSession, Document document,
			String newApiPath)
			throws RepositoryException, WorkflowException, RemoteException {
		final HippoWorkspace workspace = (HippoWorkspace) threadSafeSession.getWorkspace();
        final WorkflowManager workflowManager = workspace.getWorkflowManager();

        final Node folder = threadSafeSession.getNode(StringUtils.substringBefore(newApiPath, APIS2) + APIS2);
        Node documentNode = folder.getNode(RegExUtils
        		.replaceAll(apiPayload.getName(),PATTERN_SPACES, PATTERN_SCORE).toLowerCase(Locale.UK) + "." 
        + apiPayload.getVersion().toLowerCase(Locale.UK));
        final DocumentWorkflow documentWorkflow = (DocumentWorkflow) workflowManager.getWorkflow(DEFAULT
        		, document == null ? documentNode : document.getNode(threadSafeSession));

		final GlobalConfiguration globalConfiguration = new GlobalConfiguration();
		
		boolean apiDeprecated="true".equalsIgnoreCase(apiPayload.getDeprecated());
		
		

		//o si el Api está deprecado (este o no configurada la publicación directa) o Si se tiene configurado la publicacion directa
		if (apiDeprecated ||(documentWorkflow.hints().get(PUBLISH).equals(true) &&
				globalConfiguration.getConfigurationById(INTEGRATIONLAYER_PUBLISH, session) != null &&
				globalConfiguration.getConfigurationById(INTEGRATIONLAYER_PUBLISH, session).equalsIgnoreCase("true")) ) {
			documentWorkflow.publish();
		}
	}

	private static void boundApiToApiContainer(String newApiContainerPath, Session threadSafeSession, String newApiPath)
			throws RepositoryException {
		Node newApiContainerNode = threadSafeSession.getNode(newApiContainerPath);
        for (NodeIterator apiContainerIterator = newApiContainerNode.getNodes(); apiContainerIterator.hasNext(); ) {
            Node apiContainerVariant = apiContainerIterator.nextNode();
            Node apis = apiContainerVariant.addNode("santanderbrxm:api", Constants.HIPPO_MIRROR);
            apis.setProperty(Constants.HIPPO_DOCBASE, threadSafeSession.getNode(newApiPath).getIdentifier());
            threadSafeSession.save();
        }
	}

	/*private static void addIdFrameworkCMS(ApiPayload apiPayload, Session threadSafeSession, String apiPath )
			throws IntegrationLayerException {
		try
		{
			Node apiNode = threadSafeSession.getNode(apiPath);
			for (NodeIterator apiIterator = apiNode.getNodes(); apiIterator.hasNext(); )
			{
				Node apiVariant = apiIterator.nextNode();
				List<IdFrameworkPayload> listIdFramework = apiPayload.getIdFramework();
				if (listIdFramework!=null)
				{
					GlobalConfiguration globalConfiguration = new GlobalConfiguration();

					for (IdFrameworkPayload idFrameworkItem : listIdFramework)
					{
						String idFramework = idFrameworkItem.getId();


						String frameworkProducerOrgId = globalConfiguration
								.getConfigurationById(INTEGRATIONLAYER_PRODUCERORG_FRAMEWORK, threadSafeSession);

						String queryString="SELECT * from [santanderbrxm:ApiItem] where ([santanderbrxm:idIL] = '"+RegExUtils
								.replaceAll(idFramework,PATTERN_SPACES, PATTERN_SCORE)
								+"'  and [santanderbrxm:PK] like '%"+frameworkProducerOrgId+"') "
								+ "order by [santanderbrxm:versionIL], [santanderbrxm:versionCMS]";



						LOGGER.info("searching idFrameworkApi {}",queryString);
						Query query = threadSafeSession.getWorkspace().getQueryManager().createQuery(
								queryString, Query.JCR_SQL2);
						QueryResult queryResult = query.execute();

						NodeIterator iterator =queryResult.getNodes();

						if(iterator.getSize() > 0){
							boundApiFrameworkCMSToApi(threadSafeSession, apiVariant,idFramework, iterator);
						} else {
							//do nothing
						}

					}
				}
			}
		}
		catch (RepositoryException e)
		{
			throw new IntegrationLayerException(e.getMessage());
		}
	}*/


	private static void setGlobalExposure(Session threadSafeSession, String globalExposure, String newApiPath)
			throws RepositoryException {
		if (globalExposure!=null)
        {
        	setEnrichedTextToApiItem(globalExposure, 
        			Constants.SANTANDERBRXM_GLOBALEXPOSURE, newApiPath, threadSafeSession);
        }
	}

	private static void setFeatures(Session threadSafeSession, String featuresDocbase, String newApiPath)
			throws RepositoryException {
		if (featuresDocbase!=null)
        {
        	setEnrichedTextToApiItem(featuresDocbase, SANTANDERBRXM_FEATURES_OVERVIEW, newApiPath, threadSafeSession);
        }
	}

	private static void setPostman(Session threadSafeSession, Value postmanDocbase, String newApiPath)
			throws RepositoryException {
		if (postmanDocbase!=null)
        {
			HashMap<String,Object> tempParams=new HashMap<>();
			tempParams.put(Constants.HIPPO_DOCBASE, postmanDocbase);
        	setDocToNode(tempParams, Constants.SANTANDERBRXM_POSTMAN,Constants.HIPPO_MIRROR ,
        			newApiPath, threadSafeSession);
        }
	}

	private static void setYaml(Session threadSafeSession, Value yamlDocbase, String newApiPath)
			throws RepositoryException {
		if (yamlDocbase!=null)
        {
			HashMap<String,Object> tempParams=new HashMap<>();
			tempParams.put(Constants.HIPPO_DOCBASE, yamlDocbase);
        	setDocToNode(tempParams, Constants.SANTANDERBRXM_YAML,Constants.HIPPO_MIRROR ,
        			newApiPath, threadSafeSession);
        }
	}

	private static void setIntegrationProduct(Session threadSafeSession, List<ContentNode> integrationProdNode, String newProductContainerPath) throws RepositoryException {
		if (integrationProdNode!=null && !integrationProdNode.isEmpty())
		{
			setIntegrationProdNode(integrationProdNode, newProductContainerPath,threadSafeSession);
		}
	}

	private static void setLinkNodes(Session threadSafeSession, List<ContentNode> linkNodes, String newApiPath)
			throws RepositoryException {
		if (linkNodes!=null && !linkNodes.isEmpty())
        {
        	setLinkDownload(linkNodes,newApiPath,threadSafeSession);
        }
	}

	private static void setOldIcon(Session threadSafeSession, Value oldIcon, Value[] oldIconValues,
			Value[] oldIconModes, Value[] oldIconFacets, String newApiPath) throws RepositoryException {
		if (oldIcon!=null)
        {
        	setIconToApiContainer(oldIcon, oldIconValues, oldIconModes, oldIconFacets, newApiPath, threadSafeSession);
        }
	}

	private static void setOldDescription(Session threadSafeSession, String oldDescription, String newApiPath)
			throws RepositoryException {
		if (oldDescription!=null)
        {
        	setEnrichedTextToApiItem(oldDescription,Constants.SANTANDERBRXM_DESCRIPTION_CMS ,
        			newApiPath, threadSafeSession);
        }
	}

	private static void setDescription(ApiPayload apiPayload, Session threadSafeSession, Node newApiNode) throws RepositoryException {
		for (NodeIterator apiNodeIterator =  newApiNode.getNodes(); apiNodeIterator.hasNext();)
		{
			Node apiNodeVariant = apiNodeIterator.nextNode();

			if (apiPayload.getProducts()!=null)
			{
				for(ProductAPIPayload productAPIPayload : apiPayload.getProducts())
				{
					Node productNode = apiNodeVariant.addNode("santanderbrxm:products", "santanderbrxm:productsApi");
					productNode.setProperty("santanderbrxm:id", productAPIPayload.getId());
					productNode.setProperty("santanderbrxm:type", productAPIPayload.getType());
					productNode.setProperty(Constants.SANTANDERBRXM_MODE, productAPIPayload.getMode());
					productNode.setProperty("santanderbrxm:exposure", productAPIPayload.getExposure());
					productNode.setProperty("santanderbrxm:deprecated", productAPIPayload.getDeprecated());
					threadSafeSession.save();
				}
			}

		/*	if (apiPayload.getIdFramework()!=null)
			{
				for(IdFrameworkPayload idFramework : apiPayload.getIdFramework()){
					Node productNode = apiNodeVariant.addNode("santanderbrxm:idFramework", "santanderbrxm:idFramework");
					productNode.setProperty("santanderbrxm:id_framework", idFramework.getId());
					threadSafeSession.save();
				}
			}
			Node descriptionNode = apiNodeVariant.addNode(Constants.SANTANDERBRXM_DESCRIPTION_IL, "hippostd:html");
			descriptionNode.setProperty(Constants.HIPPO_CONTENT, apiPayload.getDescription());*/
		}
	}


	private static void linkProducerOrgToApiContainer(Node producerOrganization, String newApiContainerPath,
    		Session threadSafeSession) throws RepositoryException, IntegrationLayerException {
	try{
    	LOGGER.info("linking producer organization to api/product container");
    	
    	
        Node newApiContainerNode = threadSafeSession.getNode(newApiContainerPath);
        LOGGER.info("despues de coger el nodo del Api/Producto");
        
        for (NodeIterator apiContainerIterator = newApiContainerNode.getNodes(); apiContainerIterator.hasNext(); ) {
        	
        
            Node apiContainerVariant = apiContainerIterator.nextNode();
          
            
            Node producerOrg = apiContainerVariant.addNode("santanderbrxm:producerOrganizationContainer", 
            		Constants.HIPPO_MIRROR);
       
            producerOrg.setProperty(Constants.HIPPO_DOCBASE, producerOrganization.getIdentifier());
          
            threadSafeSession.save();
         
        }

	}catch(Exception e) {
		LOGGER.error("error al enlazar el prodOrg al Producto {}", e.getMessage());
		e.printStackTrace();
		throw new IntegrationLayerException("Error al enlazar el ProdOrganization");
	}
        
    }
    
    private static void setEnrichedTextToApiItem(String message, String nodeName , String newNodePath
    		, Session threadSafeSession) throws RepositoryException {

    	LOGGER.info("Setting desctiption to api item");

        Node newApiItemNode = threadSafeSession.getNode(newNodePath);
        for (NodeIterator apiContainerIterator = newApiItemNode.getNodes(); apiContainerIterator.hasNext(); ) {
            Node apiContainerVariant = apiContainerIterator.nextNode();
            Node descriptionNode = apiContainerVariant.addNode(nodeName, "hippostd:html");
        	descriptionNode.setProperty(Constants.HIPPO_CONTENT, message);
            
            threadSafeSession.save();
        }
    }
    

    private static void setIconToApiContainer(Value icon, Value[] iconValues,Value[] iconModes,Value[] iconFacets
    		,String newApiContainerPath, Session threadSafeSession) throws RepositoryException 
    {

    	LOGGER.info("Setting icon to api container");

        Node newApiContainerNode = threadSafeSession.getNode(newApiContainerPath);
        for (NodeIterator apiContainerIterator = newApiContainerNode.getNodes(); apiContainerIterator.hasNext(); ) {
            Node apiContainerVariant = apiContainerIterator.nextNode();
            Node descriptionNode = apiContainerVariant.addNode(Constants.SANTANDERBRXM_ICON,
            		"hippogallerypicker:imagelink");
        	descriptionNode.setProperty(Constants.HIPPO_DOCBASE, icon);
        	descriptionNode.setProperty(Constants.HIPPO_VALUES, iconValues);
        	descriptionNode.setProperty(Constants.HIPPO_MODES, iconModes);
        	descriptionNode.setProperty(Constants.HIPPO_FACETS, iconFacets);
        	threadSafeSession.save();
        }
    }
        
    private static void setDocToNode(HashMap<String,Object> nodeParams,String nodeName
    		,String nodeType,String newNodePath, Session threadSafeSession) throws RepositoryException 
    {

        	LOGGER.info("Setting icon to api container");

        	
        	Value docbase = (Value)nodeParams.get(Constants.HIPPO_DOCBASE);
        	Value[] values = (Value[])nodeParams.get(Constants.HIPPO_VALUES);
        	Value[] modes = (Value[])nodeParams.get(Constants.HIPPO_MODES);
        	Value[] facets = (Value[])nodeParams.get(Constants.HIPPO_FACETS);
        	
            Node newApiContainerNode = threadSafeSession.getNode(newNodePath);
            for (NodeIterator apiContainerIterator = newApiContainerNode.getNodes(); apiContainerIterator.hasNext(); ) {
                Node apiContainerVariant = apiContainerIterator.nextNode();
                Node descriptionNode = apiContainerVariant.addNode(nodeName, nodeType);
            	descriptionNode.setProperty(Constants.HIPPO_DOCBASE, docbase);
            	if (values!=null)
            	{
            		descriptionNode.setProperty(Constants.HIPPO_VALUES, values);
            	}
            	if (modes!=null)
            	{
            		descriptionNode.setProperty(Constants.HIPPO_MODES, modes);
            	}
            	if (facets!=null)
                {
            		descriptionNode.setProperty(Constants.HIPPO_FACETS, facets);
                }
            	threadSafeSession.save();
            }
        }

	private static void setIntegrationProdNode(List<ContentNode> integrationProdNodeList, String newProductContainerPath, Session threadSafeSession) throws RepositoryException {
		Node productContainerNode = threadSafeSession.getNode(newProductContainerPath);
		for (NodeIterator productContainerIterator = productContainerNode.getNodes(); productContainerIterator.hasNext(); ) {
			Node productContainerVariant = productContainerIterator.nextNode();

			for (ContentNode oldIntegrationProd :integrationProdNodeList)
			{
				Node integrationProdNode=productContainerVariant.addNode("santanderbrxm:integrationsProduct", "santanderbrxm:IntegrationProduct");
				integrationProdNode.setProperty(Constants.SANTANDERBRXM_TITLE, oldIntegrationProd.getProperty(Constants.SANTANDERBRXM_TITLE).getValue());

				try
				{
					setItems(oldIntegrationProd,integrationProdNode,threadSafeSession);
					setIcon(oldIntegrationProd,integrationProdNode,threadSafeSession);

				} catch (RepositoryException e)
				{
					LOGGER.info("no items or icon in product Integration {}",e.getMessage());
				}
			}
			threadSafeSession.save();
		}
	}

	private static void setIcon(ContentNode oldIntegrationProd, Node integrationProd, Session threadSafeSession) {
		try{
			//icon - items
			String[] values = new String[0];
			String[] modes = new String[0];
			String[] facets = new String[0];

			ContentNode oldIcon = oldIntegrationProd.getNode(Constants.SANTANDERBRXM_ICON);
			Node iconNode = integrationProd.addNode(Constants.SANTANDERBRXM_ICON,"hippogallerypicker:imagelink");

			iconNode.setProperty(Constants.HIPPO_DOCBASE, oldIcon.getProperty(Constants.HIPPO_DOCBASE).getValue());
			iconNode.setProperty(Constants.HIPPO_VALUES, values);
			iconNode.setProperty(HIPPO_MODES, modes);
			iconNode.setProperty(Constants.HIPPO_FACETS, facets);

			threadSafeSession.save();

		} catch (RepositoryException e) {
			LOGGER.info("no icon in product Integration {}",e.getMessage());
		}
	}

	private static void setItems(ContentNode oldIntegrationProd, Node integrationProd, Session threadSafeSession) throws RepositoryException {
		//ITEMS
		try{
		List<ContentNode> oldItemsList = oldIntegrationProd.getNodes();
		for(ContentNode oldItems : oldItemsList){
			if(oldItems.getName().contains("items")){
				Node itemsNode = integrationProd.addNode("santanderbrxm:items", "santanderbrxm:CallToActionItemSimple");
				if(oldItems.getProperty(TAB).getValue()!=null) {
					itemsNode.setProperty(TAB, oldItems.getProperty(TAB).getValue());
				}
				if(oldItems.getProperty(Constants.SANTANDERBRXM_TITLE).getValue()!=null) {
					itemsNode.setProperty(Constants.SANTANDERBRXM_TITLE, oldItems.getProperty(Constants.SANTANDERBRXM_TITLE).getValue());
				}

				//intenal link - items
				ContentNode oldInternalLink = oldItems.getNode(INTERNAL_LINK);
				Node internalLinkNode = itemsNode.addNode(INTERNAL_LINK, HIPPO_MIRROR);
				if(oldInternalLink.getProperty(Constants.HIPPO_DOCBASE).getValue()!=null) {
					internalLinkNode.setProperty(HIPPO_DOCBASE, oldInternalLink.getProperty(HIPPO_DOCBASE).getValue());
				}
				threadSafeSession.save();
			}
			threadSafeSession.save();
		}
		} catch (RepositoryException e) {
			LOGGER.info("no items or icon in product Integration {}",e.getMessage());
		}
	}

	private static void setLinkDownload (List<ContentNode> linkNodes,String newApiPath, Session threadSafeSession)
    		throws RepositoryException
    {
    	
    	Node newApiContainerNode = threadSafeSession.getNode(newApiPath);
        for (NodeIterator apiContainerIterator = newApiContainerNode.getNodes(); apiContainerIterator.hasNext(); ) {
            Node apiContainerVariant = apiContainerIterator.nextNode();
            
            for (ContentNode oldLinkNode:linkNodes)
            {
            
            	Node linkNode=apiContainerVariant.addNode(Constants.SANTANDERBRXM_LINKS,
            			"santanderbrxm:linksAndDownloads");
            	linkNode.setProperty("santanderbrxm:externalLink", oldLinkNode
            			.getProperty("santanderbrxm:externalLink").getValue());
            	linkNode.setProperty(Constants.SANTANDERBRXM_TITLE, oldLinkNode
            			.getProperty(Constants.SANTANDERBRXM_TITLE).getValue());
            	linkNode.setProperty("santanderbrxm:tab", oldLinkNode.getProperty("santanderbrxm:tab").getValue());
            
            	try
            	{
            		ContentNode oldInternalLink =oldLinkNode.getNode("santanderbrxm:internalLink");
            		Node internalLinkNode = linkNode.addNode("santanderbrxm:internalLink", Constants.HIPPO_MIRROR);
            		internalLinkNode.setProperty(Constants.HIPPO_DOCBASE,
            				oldInternalLink.getProperty(Constants.HIPPO_DOCBASE).getValue());
            		
            	} catch (RepositoryException e)
            	{
            		LOGGER.info("no internal link {}",e.getMessage());
            	}
            
            }
            
        	threadSafeSession.save();
        }

    	
    }
    
    private static void createCountryNodes(List<ContentNode> countryNodes, String newApiPath
    		, Session threadSafeSession) throws RepositoryException
    {
    	Node newApiItemNode = threadSafeSession.getNode(newApiPath);
        for (NodeIterator apiContainerIterator = newApiItemNode.getNodes(); apiContainerIterator.hasNext(); ) 
        {
        	Node apiContainerVariant = apiContainerIterator.nextNode();
        	
        	for (ContentNode countryNode:countryNodes)
        	{
        		Node country= apiContainerVariant.addNode
        				(Constants.SANTANDERBRXM_LOCALCOUNTRY,Constants.SANTANDERBRXM_LOCALCOUNTRY_TYPE);
        		country.setProperty("santanderbrxm:text", countryNode.getProperty("santanderbrxm:text").getValue());
        		
        		cloneCountryNode(countryNode, country);
        		
        		threadSafeSession.save();
        	}
        }
    }

	private static void cloneCountryNode(ContentNode countryNode, Node country)
			throws  RepositoryException {
		for (ContentNode versionNode: countryNode.getNodes())
		{
			if ("santanderbrxm:ModeVersionApiItem".equals(versionNode.getPrimaryType()))
			{
				Node version=country.addNode("santanderbrxm:modeVersion","santanderbrxm:ModeVersionApiItem");
				version.setProperty(Constants.SANTANDERBRXM_MODE, versionNode
						.getProperty(Constants.SANTANDERBRXM_MODE).getValue());
				version.setProperty("santanderbrxm:version", versionNode.getProperty("santanderbrxm:version").getValue());
				version.setProperty("santanderbrxm:exposureLocalCountry", 
						versionNode.getProperty("santanderbrxm:exposureLocalCountry").getValue());
			}
		}
	}

	private static void addApiRelationsToProductNode(String productContainerPath,ProductContainerPayload productContainerPayload, Session threadSafeSession, String country) throws IntegrationLayerException {
		try {
			Node newApiContainerNode = threadSafeSession.getNode(productContainerPath);
			
			String producerOrgId=productContainerPayload.getContainerProducerOrg();
    	   		
    		LOGGER.info("producerOrgId {}",producerOrgId);
    		
    		
			for (NodeIterator apiContainerIterator = newApiContainerNode.getNodes(); apiContainerIterator.hasNext(); )
			{
				Node apiContainerVariant = apiContainerIterator.nextNode();
				if (productContainerPayload.getApisContainers()!=null)
				{
					for (String apiId : productContainerPayload.getApisContainers())
					{
						LOGGER.info("searching apiId {}",apiId);


			    		GlobalConfiguration configurationUtil = new GlobalConfiguration();

			    		String default_lang =configurationUtil.getConfigurationById
			    				(INTEGRATIONLAYER_PRODUCERORG_FOLDER_PREFIX+producerOrgId
			    						+INTEGRATIONLAYER_PRODUCERORG_FOLDER_LANGUAGE_SUBFIX, session);
			            String searchLocale= country!=null?country.replaceFirst("_", "-"):default_lang.replaceFirst("_", "-");
			           
			            //bugfix, in the case of spain es_es is stored as only es and we need to replace it to have this work
			            //this in the future should be revised and create a translation table for more countries.
			            if (searchLocale.equalsIgnoreCase("es-es"))
			            {
			            	searchLocale="es";
			            }
			            String queryString="SELECT * from [santanderbrxm:ApiContainerItem]  "
								+ "where [hippotranslation:locale]='"+searchLocale+"' and  (([santanderbrxm:idIL] = '"+RegExUtils
								.replaceAll(apiId,PATTERN_SPACES, PATTERN_SCORE)
								+"')  and [santanderbrxm:PK] like '%"+producerOrgId+"' "
								+ ") order by [santanderbrxm:versionIL], [santanderbrxm:versionCMS]";

			            
						Query query = threadSafeSession.getWorkspace().getQueryManager().createQuery(
								queryString, Query.JCR_SQL2);
						QueryResult queryResult = query.execute();

						NodeIterator iterator =queryResult.getNodes();

						boundApiContainerToProductContainer(productContainerPayload, threadSafeSession, apiContainerVariant,
								apiId, iterator);
					}
				}
			}
		}
		catch (RepositoryException e)
		{
			e.printStackTrace();
			throw new IntegrationLayerException("Error al enlazar el los ApisContainers al Product");
		}

	}

	private static void boundApiContainerToProductContainer(ProductContainerPayload productContainerPayload,
			Session threadSafeSession, Node apiContainerVariant, String apiId, NodeIterator iterator)
			throws RepositoryException, IntegrationLayerException {
		if (iterator.hasNext())
		{
			Node apiNode=iterator.nextNode();
			LOGGER.info("API Node {}",apiNode.getName());
			Node producerOrg = apiContainerVariant.addNode("santanderbrxm:apiContainers", 
		    		Constants.HIPPO_MIRROR);
		    producerOrg.setProperty(Constants.HIPPO_DOCBASE, apiNode.getParent().getIdentifier());
		    threadSafeSession.save();
			
		} else
		{
			LOGGER.info("No Api found for id {} , breaking ",apiId);
			throw new IntegrationLayerException("No api "+apiId+" found for product idIL"
			+productContainerPayload.getId());
		}
	}

	/*private static void boundApiFrameworkCMSToApi(Session threadSafeSession, Node apiVariant, String idFramework, NodeIterator iterator) throws IntegrationLayerException, RepositoryException {
		if (iterator.hasNext())
		{
			Node apiNode=iterator.nextNode();
			LOGGER.info("API Node {}",apiNode.getName());
			Node apiFrameworkCMSNode = apiVariant.addNode("santanderbrxm:api_Framework_cms",
					Constants.HIPPO_MIRROR);
			apiFrameworkCMSNode.setProperty(Constants.HIPPO_DOCBASE, apiNode.getIdentifier());
			threadSafeSession.save();

		} else
		{
			LOGGER.info("No Api Framework found with id {} , breaking ",idFramework);
			throw new IntegrationLayerException("No api Framework with idIL"+idFramework+" found");
		}
	}*/

	@Override
    public void initialize(final Session session) throws RepositoryException {
        this.session = session;
        LOGGER.debug("Daemon module initialized.");
    }

    @Override
    public void shutdown() {
        LOGGER.debug("Daemon module shutdown.");
    }

	public static String generatePk(ProductContainerPayload productContainerPayload)
	{
		
		
		return productContainerPayload.getName()
				+ "_" + productContainerPayload.getVersion()
				+ "_" + productContainerPayload.getContainerProducerOrg();
	}

	public static String generatePk(ApiContainerPayload apiContainerPayload)
	{
		return apiContainerPayload.getName()
				+ "_" + apiContainerPayload.getVersion()
				+ "_" + apiContainerPayload.getContainerProducerOrg();
	}

	public static String generatePk(ApiPayload apiPayload, String producerOrg)
	{
		return apiPayload.getName()
				+ "_" + apiPayload.getVersion()
				+ "_" + producerOrg;
	}
	
	public static Session getSession()
	{
		return session;
	}

}
