/**
 * Cronjob class that read payloads from kafka to create CMS objects
 * 
 */
package com.santander.sync.modules;

import com.santander.sync.kafka.EventHubSync;
import com.santander.sync.model.IntegrationLayerConfigurationModel;
import com.santander.utils.Constants;
import com.santander.utils.IntegrationLayerLogUtil;
import org.onehippo.repository.scheduling.RepositoryJob;
import org.onehippo.repository.scheduling.RepositoryJobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.RepositoryException;

public class IntegrationLayerSchedulerApis implements RepositoryJob {

	

	private static Logger logger = LoggerFactory.getLogger(IntegrationLayerSchedulerApis.class);


    public static final String LOCAL_ENV  = "localhost";
    
    public static final String BRX_HOSTNAME = System.getenv("BRX_HOSTNAME");
    public static final String ASSET_HOSTNAME = System.getenv("ASSET_HOSTNAME");
    public static final String EVENTHUB_APP_SECRET = System.getenv("EVENTHUB_APP_SECRET");
    public static final String EVENTHUB_APP_ID = System.getenv("EVENTHUB_APP_ID");
    public static final String EVENTHUB_TENANT_ID = System.getenv("EVENTHUB_TENANT_ID");
    
    public static final String BOOTSTRAP_SERVERS = "BOOTSTRAP_SERVERS";

    
    public static final String GROUP_ID = "GROUP_ID";
    public static final String LOCAL_TESTING = "LOCAL_TESTING";
    public static final String REQUEST_TIMEOUT_MS = "REQUEST_TIMEOUT_MS";
    public static final String SECURITY_PROTOCOL= "SECURITY_PROTOCOL";
    public static final String SASL_MECHANISM = "SASL_MECHANISM";
    public static final String SASL_JAAS_CONFIG = "SASL_JAAS_CONFIG";
    public static final String SASL_LOGIN_CALLBACK_HANDLER_CLASS = "SASL_LOGIN_CALLBACK_HANDLER_CLASS";
	
	public void execute(RepositoryJobExecutionContext context) throws RepositoryException {

		
		
		logger.info("ejecutando IntegrationLayerSchedulerApis");
		// se invocan a kafka para leer los topics del eventhub

		// Primero se leen los topics de producer organization
		EventHubSync eventhubSync = new EventHubSync();
		boolean lastResult=false;
		
		
		IntegrationLayerConfigurationModel mockILConfiguration = getILConfiguration("",context);
		
		
		//CARGA DESDE LOCAL. EN EL CASO DE ESTAR RELLENOS LAS PROPIEDADES MOCK_TOPIC Y MOCK_MESSAGE EN EL CONSOLE
		if (mockILConfiguration.getMockTopic()!=null && mockILConfiguration.getMockMessage()!=null) 
		{
			String mockTopic=mockILConfiguration.getMockTopic();
			String recordValue=mockILConfiguration.getMockMessage();
			
			logger.error("Warning: running in mock mode, no calls to Event hub will be made this time");
			
			processMock(eventhubSync, mockILConfiguration, mockTopic, recordValue);
		} 
		//CARGA DESDE EL EVENTHUB
		else {
			
			//1.- Carga de Producer organization
			try {
				//llamada a kafka
				logger.info("Step1. Importacion Producers Orgs");
				lastResult=eventhubSync.consumes(getILConfiguration(Constants.PRODUCER_ORG_TOPIC,context));
			
			} catch (Exception e) {
				logger.error("Error consumesProducerOrgs:  ",  e);
				lastResult=false;
				
			}
	
			if (!lastResult)
			{
				logger.info("Aborting step 1 in Producer Orgs");
				return;
			}
			
			//2.- Carga de API Container
			try {
				logger.info("Step2. Importación APIs Container");
				lastResult=eventhubSync.consumes(getILConfiguration(Constants.API_TOPIC,context));
				
			} catch (Exception e) {
				logger.error("Error consumesApiContainers: ",e);
				lastResult=false;
			}
	
	
			if (!lastResult)
			{
				logger.info("Aborting step2 in APIs Container ");
			}
			
			
		}

	}

	private void processMock(EventHubSync eventhubSync, IntegrationLayerConfigurationModel mockILConfiguration,
							 String mockTopic, String recordValue) {
		try
		{
			if (mockTopic.equals(mockILConfiguration.getApiTopic())){
				eventhubSync.processAPI(recordValue,mockILConfiguration);
			} 
			else if (mockTopic.equals(mockILConfiguration.getProducerOrgTopic())){
				eventhubSync.processProducerOrg(recordValue);
			} 
			else{
				//Do nothing
			}
		}	catch (Exception e)
		{
			logger.error("error creating document with mock", e );
			//setting up error event variables.
			String userId="Cronjob";
			String environment= mockILConfiguration.getEnvironment()!=null?mockILConfiguration.getEnvironment():"mockEnv";
			IntegrationLayerLogUtil.error("mock", EventHubSync.class.getName(), 
					environment, mockTopic , userId, e.getMessage(), recordValue);

		}
	}

	/**
	 * We will retrieve the synchronization configuration 
	 * @return
	 */
	private IntegrationLayerConfigurationModel getILConfiguration(String topic, RepositoryJobExecutionContext context)
	{
		
		IntegrationLayerConfigurationModel ILConfiguration = IntegrationLayerConfigurationModel.builder()
				.bootstrapServers(System.getenv(BOOTSTRAP_SERVERS))
				.groupId(context.getAttribute(GROUP_ID))
				.requestTimeoutMS(context.getAttribute(REQUEST_TIMEOUT_MS))
				.securityProtocol(context.getAttribute(SECURITY_PROTOCOL))
				.saslMechanism(context.getAttribute(SASL_MECHANISM))
				.saslJaasConfig(context.getAttribute(SASL_JAAS_CONFIG))
				.saslLoginCallbackHandlerClass(context.getAttribute(SASL_LOGIN_CALLBACK_HANDLER_CLASS))
				.localTesting(context.getAttribute(LOCAL_TESTING))
				.productTopic(context.getAttribute(Constants.PRODUCT_TOPIC))
				.producerOrgTopic(context.getAttribute(Constants.PRODUCER_ORG_TOPIC))
				.apiTopic(context.getAttribute(Constants.API_TOPIC))
				.mockTopic(context.getAttribute(Constants.MOCK_TOPIC))
				.mockMessage(context.getAttribute(Constants.MOCK_MESSAGE))
				.autoOffsetReset(context.getAttribute(Constants.AUTO_OFFSET_RESET))
				.topic(context.getAttribute(topic))
				.build();
		
		logger.info("Atributes {}", context.getAttributeNames().toArray()[0]);
		
		
		return ILConfiguration;
	}
	

}
