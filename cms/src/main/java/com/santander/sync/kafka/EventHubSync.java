/**
 * Integration layer class that performs the comunication with kafka
 */
package com.santander.sync.kafka;

import com.google.gson.Gson;
import com.santander.sync.model.ApiContainerPayload;
import com.santander.sync.model.ApiPayload;
import com.santander.sync.model.IntegrationLayerConfigurationModel;
import com.santander.sync.model.WrapperAPIContainerPayload;
import com.santander.sync.model.WrapperProducerOrgPayload;
import com.santander.sync.model.WrapperProductPayload;
import com.santander.sync.modules.IntegrationLayerModule;
import com.santander.utils.IntegrationLayerLogUtil;
import java.time.Duration;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.UUID;
import static com.santander.utils.Constants.ERROR;
import static com.santander.utils.Constants.SUCCESS;

public class EventHubSync {

	private static final String CONNECTION_CLOSED = "connection closed";
	private static Logger logger = LoggerFactory.getLogger(EventHubSync.class);
	private static Gson gson = new Gson();
	private static final Integer DURATION=5;

	public void processProducerOrg(String recordValue)
	{
		logger.info("INICIO  processProducerOrg");
		logger.info("ProducerOrg Record:({})", recordValue);
	
		WrapperProducerOrgPayload producerOrgPayload = gson.fromJson(recordValue, WrapperProducerOrgPayload.class);
		//Se procede a su alta o modificacion. Si el ProducerOrg estuviese bloqueado, lo detecta al intentar crearlo
		try {
			
			IntegrationLayerModule.createOrUpdateProducerOrganizationContainer(producerOrgPayload.getPayload());
		
			logger.info("ProducerOrganization created successfully ");
			IntegrationLayerLogUtil.sendProducerOrgMail(SUCCESS,producerOrgPayload, IntegrationLayerModule.getSession());
			logger.info("FIN  processProducerOrg");
			
		}
		catch( Exception e) {
			//ENVIO MAIL ERROR.
			logger.error("error creacion ProducerOrg : {} ", e.getMessage());
			IntegrationLayerLogUtil.sendProducerOrgMail(ERROR,producerOrgPayload, IntegrationLayerModule.getSession());
			
		}
	}

	public void processAPI(String recordValue,IntegrationLayerConfigurationModel ilConfigurationModel)
	{
		logger.info("INICIO  processAPI"); 
		logger.info("API Container Record:({})", recordValue);
		logger.info("ANTES DE CARGAR valores en apiContainerPayload desd el recordValue ");

		WrapperAPIContainerPayload apiContainerPayload = gson.fromJson(recordValue, WrapperAPIContainerPayload.class);
		
		logger.info("apiContainerPayload id: {}", apiContainerPayload.getPayload().getId());
		logger.info("apiContainerPayload Name: {}", apiContainerPayload.getPayload().getName());
		logger.info("apiContainerPayload Title: {}", apiContainerPayload.getPayload().getTitle());
		logger.info("apiContainerPayload Version: {}", apiContainerPayload.getPayload().getVersion());
		logger.info("apiContainerPayload prodOrg: {}", apiContainerPayload.getPayload().getContainerProducerOrg());
		try {

			 //Si no existe su ProductOrganizacion---> return
			if (!IntegrationLayerModule.existProducerOrganization(apiContainerPayload.getPayload())){ 
				
				 logger.info("NO EXISTE EL PROD ORGANIZARION QUE TIENE ASIGNADO");
				 IntegrationLayerLogUtil.sendApiMail(ERROR,apiContainerPayload, IntegrationLayerModule.getSession());
				return;
			}
		
			//Se procede a su alta o modificacion. Si el ApiProduct estuviese bloqueado lo detecta al intentar crearlo
			//[WorkflowDocumentManagerImpl.obtainEditableDocument:481] Failed to obtain editable instance for document .....
			else { 
				logger.info("EXISTE EL PROD ORGANIZARION QUE TIENE ASIGNADO");
				filterApisVersion(apiContainerPayload.getPayload(),ilConfigurationModel);
				IntegrationLayerModule.createApiContainer(apiContainerPayload.getPayload());
				IntegrationLayerLogUtil.sendApiMail("success",apiContainerPayload, IntegrationLayerModule.getSession());
				logger.info("API Container created successfully ");
			}
			logger.info("FIN  processAPI");
		}
		catch( Exception e) {
			
			IntegrationLayerLogUtil.sendApiMail(ERROR,apiContainerPayload, IntegrationLayerModule.getSession());
			
		}
		
	}


	public void processProduct(String recordValue)
	{
		logger.info("INICIO processProduct");
		logger.info("Product Record:({})", recordValue);
		WrapperProductPayload productContainerPayload = gson.fromJson(recordValue, WrapperProductPayload.class);
		logger.info("apiContainerPayload id: {}", productContainerPayload.getPayload().getId());
		logger.info("apiContainerPayload Name: {}", productContainerPayload.getPayload().getName());
		logger.info("apiContainerPayload deprecated: {}", productContainerPayload.getPayload().getDeprecated());
		logger.info("apiContainerPayload Version: {}", productContainerPayload.getPayload().getVersion());
		logger.info("apiContainerPayload prodOrg: {}", productContainerPayload.getPayload().getContainerProducerOrg());

		try {
			logger.info("apis: {}",productContainerPayload.getPayload().getApisContainers());

					
			
			if (!IntegrationLayerModule.existProducerOrganization(productContainerPayload.getPayload()) || !IntegrationLayerModule.exitsApisContainers(productContainerPayload.getPayload())){ // O NO EXISTE ALGUNO DE SUS APIS.FALTAAAA HACER
				 
				logger.info("NO EXISTE EL PROD ORGANIZARION O ALGUNO DE LOS APIS CONTAINERS QUE TIENE ASIGNADO");
				IntegrationLayerLogUtil.sendProductMail("error",productContainerPayload, IntegrationLayerModule.getSession());
				return;
			}
			
			else {
			//SE PROCEDE A SU ALTA O MODIFICACION. Si el product estuviese bloqueado lo detecta al intentar crearlo
			//[WorkflowDocumentManagerImpl.obtainEditableDocument:481] Failed to obtain editable instance for document .....
				logger.info("SI EXISTE EL PROD ORGANIZARION O ALGUNO DE LOS APIS CONTAINERS QUE TIENE ASIGNADO");
				IntegrationLayerModule.createOrUpdateProductContainer(productContainerPayload.getPayload());
				logger.info("Product created successfully ");
				IntegrationLayerLogUtil.sendProductMail("success",productContainerPayload, IntegrationLayerModule.getSession());
			}
			logger.info("FIN processProduct");
		}
		catch( Exception e) {
			//ENVIO MAIL ERROR
			IntegrationLayerLogUtil.sendProductMail("error",productContainerPayload, IntegrationLayerModule.getSession());
			
		}
	}


	public boolean consumes(IntegrationLayerConfigurationModel ilConfiguration)
	{

		Consumer<String, String> consumer = null;
		try
		{
			consumer = createConsumer(ilConfiguration);

			logger.info("Inicio consumes");
			Duration duration =  Duration.ofSeconds(DURATION);
			final ConsumerRecords<String, String> consumerRecords = consumer.poll(duration);
			logger.info("topic {} records count {}",ilConfiguration.getTopic(),consumerRecords.count());
			for(ConsumerRecord<String, String> cr : consumerRecords)
			{
				processConsumerRecord(ilConfiguration, cr);
				
			}
			logger.info("commiting all mensajes batch ");
			consumer.commitAsync();

		} catch (Exception e)
		{
			logger.error("error creating consumer or getting partition",e);
			if (consumer!=null)
			{
				consumer.close();
				logger.info(CONNECTION_CLOSED);
				return false;
			}
			
		}  finally
		{
			if (consumer!=null)
			{
				consumer.close();
				logger.info(CONNECTION_CLOSED);
			}
		}

		logger.info("Exit consumer");
		return true;

	}

	private void processConsumerRecord
			(IntegrationLayerConfigurationModel ilConfiguration, ConsumerRecord<String, String> cr)
	{
		logger.info("looping consumer records");
		if (logger.isInfoEnabled())
		{
			logger.info("Consumer Record:(Value [{}])",
					cr.value());
		}
		logger.info("Topic {}", ilConfiguration.getTopic());


		//setting up error event variables.
		String userId="Cronjob";
		String environment=ilConfiguration.getEnvironment()!=null?ilConfiguration.getEnvironment():"notConfiguredInJob";

		final String topic = ilConfiguration.getTopic();

		if (ilConfiguration.getProducerOrgTopic().equals(ilConfiguration.getTopic()))
		{
			try
			{
				processProducerOrg(cr.value());
				
			} catch (Exception e)
			{

				IntegrationLayerLogUtil
						.error(cr.key(), EventHubSync.class.getName(), environment, topic , userId, e.getMessage(), cr.value());
				logger.error("error processing producer org. Process will continue",e);
			}

		} else if (ilConfiguration.getProductTopic().equals(ilConfiguration.getTopic()))
		{
			try
			{
				processProduct(cr.value());
				
			} catch (Exception e)
			{
				IntegrationLayerLogUtil
						.error(cr.key(), EventHubSync.class.getName(), environment, topic , userId, e.getMessage(), cr.value());
				logger.error("error processing product. Process will continue",e);
			}


		} else if (ilConfiguration.getApiTopic().equals(ilConfiguration.getTopic()))
		{
			try
			{
				processAPI(cr.value(),ilConfiguration);
				

			} catch (Exception e)
			{
				IntegrationLayerLogUtil
						.error(cr.key(), EventHubSync.class.getName(), environment, topic , userId, e.getMessage(), cr.value());
				logger.error("error procesing api Container. Process will continue",e);
			}
		} else
		{
			logger.warn("topic {} not found",ilConfiguration.getTopic());
		}
	}

	private Consumer<String, String> createConsumer(IntegrationLayerConfigurationModel ILConfiguration)
	{

		final Properties properties = new Properties();
		synchronized (EventHubSync.class) {
			properties.put(ConsumerConfig.CLIENT_ID_CONFIG, UUID.randomUUID().toString());
		}
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

		properties.put("group.id", ILConfiguration.getGroupId());
		properties.put("request.timeout.ms", ILConfiguration.getRequestTimeoutMS());
		properties.put("security.protocol", ILConfiguration.getSecurityProtocol());
		properties.put("sasl.mechanism", ILConfiguration.getSaslMechanism());
		properties.put("sasl.jaas.config", ILConfiguration.getSaslJaasConfig());
		properties.put("sasl.login.callback.handler.class", ILConfiguration.getSaslLoginCallbackHandlerClass());
		if (ILConfiguration.getAutoOffsetReset()!=null)
		{
			properties.put("auto.offset.reset", ILConfiguration.getAutoOffsetReset());
		}
		properties.put("enable.auto.commit", "false");
		properties.put("bootstrap.servers", ILConfiguration.getBootstrapServers());


		// Create the consumer using properties.
		final Consumer<String, String> consumer = new KafkaConsumer<>(properties);

		// Subscribe to the topic.
		consumer.subscribe(Collections.singletonList(ILConfiguration.getTopic()));
		return consumer;


	}

	private static void filterApisVersion(ApiContainerPayload apiContainerPayload,
										  IntegrationLayerConfigurationModel ilConfigurationModel)
	{
		logger.info("INICIO filterApisVersion");
		String[] blacklistedTerms=null;
		if (ilConfigurationModel.getVersionBlacklist()!=null && !ilConfigurationModel.getVersionBlacklist().isEmpty()
				&& apiContainerPayload.getApis()!=null && !apiContainerPayload.getApis().isEmpty())
		{
			blacklistedTerms=ilConfigurationModel.getVersionBlacklist().split(";");
		} else
		{
			return;
		}

		List<ApiPayload> filteredApis= new ArrayList<>();
		for (ApiPayload api: apiContainerPayload.getApis())
		{
			String lowerVersion= api.getVersion().toLowerCase(Locale.UK);
			for (String term: blacklistedTerms)
			{

				if (lowerVersion.contains(term.toLowerCase(Locale.UK)))
				{
					logger.info("Skipping Api entry {} with version {}",api.getId(),lowerVersion);

				} else
				{
					filteredApis.add(api);
				}
			}

		}
		apiContainerPayload.setApis(filteredApis);
		logger.info("FIN filterApisVersion");
	}
}
