package com.santander.sync.workflow;


import com.google.gson.Gson;
import com.santander.sync.model.*;
import com.santander.utils.Constants;
import com.santander.utils.GlobalConfiguration;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.wicket.model.StringResourceModel;
import org.hippoecm.addon.workflow.StdWorkflow;
import org.hippoecm.addon.workflow.WorkflowDescriptorModel;
import org.hippoecm.frontend.plugin.IPluginContext;
import org.hippoecm.frontend.plugin.config.IPluginConfig;
import org.hippoecm.frontend.service.render.RenderPlugin;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.repository.api.WorkflowDescriptor;
import org.onehippo.repository.documentworkflow.DocumentWorkflow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;


public class ExportToAnotherEnvironmentWorkflowPlugin extends RenderPlugin<WorkflowDescriptor> {
 
	private static final Logger LOGGER = LoggerFactory.getLogger(ExportToAnotherEnvironmentWorkflowPlugin.class);
	
	public static final String IMPORT_TARGET_URL="IMPORT_TARGET_URL";
	public static final String IMPORT_PAYLOAD_PARAM="IMPORT_PAYLOAD_PARAM";
	public static final String IMPORT_TYPE_PARAM="IMPORT_TYPE_PARAM";
	private static final transient GlobalConfiguration globalConfiguration= new GlobalConfiguration();
    private static Gson gson = new Gson();

    public ExportToAnotherEnvironmentWorkflowPlugin(IPluginContext context, IPluginConfig config) 
    {
        super(context, config);
 
        LOGGER.info("Rendering Migrate Option");


            add(new StdWorkflow<DocumentWorkflow>("Migrate",
                    new StringResourceModel("Migrate", this),
                    (WorkflowDescriptorModel) getModel()) {


                @Override
                public String getSubMenu() {
                    // TODO Auto-generated method stub
                    return new StringResourceModel("MigrateMenu", this).getString();
                }

                @Override
                protected String execute(DocumentWorkflow workflow) throws Exception {
                    LOGGER.info("Executing Migration for object {}", workflow.getNode().getName());
                    //workflow.add("new-document", "myproject:index", "index");

                    return sendDocument(workflow.getNode());
                }

            });

    }

    public String sendDocument(Node node) throws Exception
    {
    	
    	LOGGER.info("entra");
        //Session
        Session threadSafeSession = RequestContextProvider.get().getSession().impersonate(new SimpleCredentials(RequestContextProvider.get().getSession().getUserID(), new char[0]));

        String importUrl =globalConfiguration.getConfigurationById(IMPORT_TARGET_URL, threadSafeSession);
    	//String payloadParam =globalConfiguration.getConfigurationById(IMPORT_PAYLOAD_PARAM, threadSafeSession);
    //	String typeParam =globalConfiguration.getConfigurationById(IMPORT_TYPE_PARAM, threadSafeSession);

    	/**Creating object of Documentation **/
        DocumentationPayloadMigration documentationPayloadMigration = new DocumentationPayloadMigration();
        String type = node.getNode(node.getName()).getPrimaryNodeType().getName();
        LOGGER.info("the type is {} of Object documentationPayloadMigration ",type);
        String json = "";

        if(type.equals(Constants.SANTANDERBRXM_DOCUMENTATION)){

            documentationPayloadMigration = getObjectDataApiContainer(documentationPayloadMigration, node);
            json = gson.toJson(documentationPayloadMigration);
            LOGGER.info("Json represenatation of Object documentationPayloadMigration is  {}", json);

        }

    	HttpPost post = new HttpPost(importUrl);
    	
    	List<NameValuePair> urlParameters = new ArrayList<>();
        urlParameters.add(new BasicNameValuePair("payload", json));
        urlParameters.add(new BasicNameValuePair("type",type));

        post.setEntity(new UrlEncodedFormEntity(urlParameters));
        
        
        post.setHeader("Accept", MediaType.APPLICATION_JSON);
        post.setHeader("Content-type", MediaType.APPLICATION_FORM_URLENCODED);
        
        LOGGER.info("Before post");
        try (CloseableHttpClient httpClient = HttpClients.createDefault();

            CloseableHttpResponse response = httpClient.execute(post)){
        	LOGGER.info("Response code {}", response.getStatusLine().getStatusCode());
            String result = EntityUtils.toString(response.getEntity());
            threadSafeSession.logout();
            LOGGER.info("result {}",result);

        }

    	
    	return null;
    }

    /** Get the data to be inserted into the object ApiContainer **/
    public static DocumentationPayloadMigration getObjectDataApiContainer(DocumentationPayloadMigration documentationPayload, Node node) throws RepositoryException {
        try {
            /**insert the data**/
            documentationPayload.setPath(node.getPath());
            node = node.getNode(node.getName());

            documentationPayload.setAlias(node.getProperty("santanderbrxm:alias").getString());
            documentationPayload.setTitle(node.getProperty("santanderbrxm:title").getString());
            documentationPayload.setType(node.getProperty("santanderbrxm:type").getString());

            //CONTROLAR QUE NO EXISTA
            try {
                Node nodeDescription = node.getNode(Constants.SANTANDERBRXM_DESCRIPTION);
                documentationPayload.setDescription(nodeDescription.getProperty(Constants.HIPPO_CONTENT).getString());
            }catch (Exception e) {
                LOGGER.error("nodo description dont exist");
            }


        }catch (Exception e ){
            LOGGER.info("Fail to set ApiContainerPayload Export Migration {}",e.getMessage());
        }

        return documentationPayload;
    }
}