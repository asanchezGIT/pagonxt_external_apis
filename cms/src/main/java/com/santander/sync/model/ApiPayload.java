package com.santander.sync.model;

import java.util.List;

public class ApiPayload {
	
	public ApiPayload() {
		// Auto-generated constructor stub
	}
	
	public String getName_version_producerOrg() {
		return name_version_producerOrg;
	}
	public void setName_version_producerOrg(String name_version_producerOrg) {
		this.name_version_producerOrg = name_version_producerOrg;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getDeprecated() {
		return deprecated;
	}
	public void setDeprecated(String deprecated) {
		this.deprecated = deprecated;
	}
	public List<ProductAPIPayload> getProducts() {
		return products;
	}
	public void setProducts(List<ProductAPIPayload> products) {
		this.products = products;
	}
/*	public void setIdFramework(List<IdFrameworkPayload> idFramework) {
		this.idFramework = idFramework;
	}*/
	protected String name_version_producerOrg;
	protected String id;
	protected String name;
	protected String title;
	protected String version;
	protected String description;
	protected String deprecated;
	private   List<ProductAPIPayload> products;
	//private   List<IdFrameworkPayload> idFramework;
}
