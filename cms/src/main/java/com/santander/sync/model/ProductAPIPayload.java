/**
 * Payload class for Products bounds to  API containers
 * 
 */
package com.santander.sync.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProductAPIPayload {
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getExposure() {
		return exposure;
	}
	public void setExposure(String exposure) {
		this.exposure = exposure;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	public String getDeprecated() {
		return deprecated;
	}
	public void setDeprecated(String deprecated) {
		this.deprecated = deprecated;
	}
	
	protected String id;
    protected String exposure;
    protected String type;
    protected String mode;
    protected String deprecated;

}
