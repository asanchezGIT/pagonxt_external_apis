package com.santander.sync.model;

import java.util.Map;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DocumentCreationResponse {

	protected boolean success;
	protected Object body;
	protected Map<String, Object> error;

}
