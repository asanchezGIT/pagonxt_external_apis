/**
 * Wrapper class for API Containers from kafka
 */
package com.santander.sync.model;


public class WrapperAPIContainerPayload {
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public ApiContainerPayload getPayload() {
		return payload;
	}
	public void setPayload(ApiContainerPayload payload) {
		this.payload = payload;
	}
	protected String id;
    protected String timestamp;
    protected ApiContainerPayload payload;

}
