package com.santander.validators;

import com.santander.utils.Constants;
import org.onehippo.cms.services.validation.api.ValidationContext;
import org.onehippo.cms.services.validation.api.Validator;
import org.onehippo.cms.services.validation.api.Violation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.RepositoryException;
import java.util.Optional;

public class FieldProcessedValidator implements Validator<String> {


    private static Logger logger = LoggerFactory.getLogger(FieldProcessedValidator.class);


    public FieldProcessedValidator() {
        //
    }

    public Optional<Violation> validate(final ValidationContext context, final String value) {
        try {

        	logger.info("context.getDocumentNode() {}", context.getDocumentNode().getName());
        	
            logger.info("checking change of pk processed: {}, modified: {}",
                    context.getDocumentNode().hasProperty(Constants.PROCESSED)&&
                    context.getDocumentNode().getProperty(Constants.PROCESSED).getBoolean(),context.getDocumentNode().getProperty("santanderbrxm:PK").isModified() );
            
                      
            
            
            if (context.getDocumentNode().hasProperty(Constants.PROCESSED) && context.getDocumentNode().getProperty(Constants.PROCESSED).getBoolean() &&
            		(context.getDocumentNode().getProperty("santanderbrxm:PK").isModified()||context.getDocumentNode().getProperty("santanderbrxm:idIL").isModified() || context.getDocumentNode().getProperty("santanderbrxm:nameIL").isModified()
            				|| context.getDocumentNode().getProperty("santanderbrxm:versionIL").isModified()))
            {
                return Optional.of(context.createViolation());
            }
        } catch (RepositoryException e) {
            logger.error("FieldProcessedValidator error: {}", e.getMessage());
        }
        return Optional.empty();

    }
}
