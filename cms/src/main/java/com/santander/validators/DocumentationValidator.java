package com.santander.validators;

import com.santander.sync.modules.IntegrationLayerModule;
import org.onehippo.cms.services.validation.api.ValidationContext;
import org.onehippo.cms.services.validation.api.Validator;
import org.onehippo.cms.services.validation.api.Violation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import java.util.Locale;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class DocumentationValidator implements Validator<String> {

    private static Logger logger = LoggerFactory.getLogger(DocumentationValidator.class);

    public DocumentationValidator() {
        //
    }


    public Optional<Violation> validate(final ValidationContext context, final String value) {

        try {

            logger.info("name node Context: {}", context.getDocumentNode().getName());

            String urlNode = context.getDocumentNode().getPath();
            String locale ="";
            Pattern p ;
            //PARA VER SI VIENE ARGENTINA QUE TIENE 3 SIGLAS
            if(urlNode.toLowerCase(Locale.ROOT).contains("arg")){
                p = Pattern.compile("[a-z][a-z]_[a-z][a-z][a-z]");
            }else{
                p = Pattern.compile("[a-z][a-z]_[a-z][a-z]");
            }
            Matcher m = p.matcher(urlNode);
            if (m.find()) {
                locale = m.group(0).replace("_", "-");
                if (locale.equalsIgnoreCase("es-ES"))
                {
                    locale="es";
                }
                logger.debug("localeProcessed: {}" , locale);
            }





            if (context.getDocumentNode().getProperty("santanderbrxm:alias").isModified())
            {

                String alias = context.getDocumentNode().getProperty("santanderbrxm:alias").getValue().getString();
                String queryString = "SELECT * from [santanderbrxm:documentation] where [santanderbrxm:alias] = '"+alias+"'" +
                        "and [hippotranslation:locale]='"+locale+"' order by [santanderbrxm:alias]";

                logger.info("queryString DocumentationValidator: {} ", queryString);

                Session session = IntegrationLayerModule.getSession();

                Query query =  session.getWorkspace().getQueryManager().createQuery(queryString, Query.JCR_SQL2);

                QueryResult result = query.execute();

                NodeIterator aliasDocumentation = result.getNodes();

                //Si es mayor a 0, existe y creamos la excepcion
                if(aliasDocumentation.getSize()>0){
                    return Optional.of(context.createViolation());
                }

            }

        } catch (RepositoryException e) {
            logger.error("DocumentationValidator error: {}", e.getMessage());
        }

        return Optional.empty();
    }
}

