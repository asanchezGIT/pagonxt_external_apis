package com.santander.utils;

import java.util.HashMap;
import java.util.Map;

public final class Constants {

	private Constants() {
		// Auto-generated constructor stub
	}
	
    public static final String LOCAL_ENV  = "localhost";
    public static final String BRX_HOSTNAME = System.getenv("BRX_HOSTNAME");
    public static final String ASSET_HOSTNAME = System.getenv("ASSET_HOSTNAME");
    public static final String EVENTHUB_APP_SECRET = System.getenv("EVENTHUB_APP_SECRET");
    public static final String EVENTHUB_APP_ID = System.getenv("EVENTHUB_APP_ID");
    public static final String EVENTHUB_TENANT_ID = System.getenv("EVENTHUB_TENANT_ID");

    public static final String PRODUCER_ORG_TOPIC = "PRODUCER_ORG_TOPIC";
    public static final String PRODUCT_TOPIC = "PRODUCT_TOPIC";
    public static final String API_TOPIC = "API_TOPIC";
    public static final String MOCK_TOPIC = "MOCK_TOPIC";
    public static final String MOCK_MESSAGE = "MOCK_MESSAGE";
    public static final String AUTO_OFFSET_RESET = "AUTO_OFFSET_RESET";
    
    
    public static final String MODE = "mode";
    public static final String VIEW = "view";
    public static final String EDIT = "edit";
    public static final String HIPPO_MIRROR = "hippo:mirror";
    public static final String PUBLISHED = "published";
    public static final String HIPPOSTD_STATE = "hippostd:state";
    public static final String PROCESSED = "processed";
    public static final String AUTHOR = "author_";
    public static final String EDITOR = "editor_";
    public static final String TRANSLATOR = "translator_";
    public static final String ERROR = "error";
    public static final String SUCCESS = "success";
    public static final String ESCENARIO_3 = "Escenario 3. node to copy:";
    public static final String IS_INFERIOR = "is inferior to current version";
    public static final String NO_MAIL = "No mail configured for producer organization";
    public static final String INTERNAL_LINK = "santanderbrxm:internalLink";
    public static final String IMAGELINK = "hippogallerypicker:imagelink";
    public static final String TAB = "santanderbrxm:tab";
    public static final String ES_ES = "es-es";
    public static final String OR_IDL = "' OR  ([santanderbrxm:idIL] = '";
    public static final String AND_PK = "'  and [santanderbrxm:PK] like '%";
    public static final String ORDERBY_VERSION = "order by [santanderbrxm:versionIL], [santanderbrxm:versionCMS]";
    public static final String AND_DRAFT = "' and @hippostd:state = 'draft' and @hippostd:stateSummary = 'changed' ]";
    public static final String QUERY_PRODUCER = "/jcr:root/content/documents/santander//element(*, santanderbrxm:ProducerOrganizationContainer)[@santanderbrxm:idIL ='";
    public static final String QUERY_APICONTAINER = "/jcr:root/content/documents/santander//element(*, santanderbrxm:ApiContainerItem)[@santanderbrxm:idIL ='";
    public static final String QUERY_PRODUCTCONTAINER = "/jcr:root/content/documents/santander//element(*, santanderbrxm:ProductContainerItem)[@santanderbrxm:idIL ='";

    public static final String SANTANDERBRXM_ID_IL = "santanderbrxm:idIL";
    public static final String SANTANDERBRXM_TITLE_IL = "santanderbrxm:titleIL";
    public static final String SANTANDERBRXM_TITLE= "santanderbrxm:title";
    public static final String SANTANDERBRXM_NAME_IL = "santanderbrxm:nameIL";
    public static final String SANTANDERBRXM_NAME = "santanderbrxm:name";
    public static final String SANTANDERBRXM_DESCRIPTION_IL = "santanderbrxm:descriptionIL";
    public static final String SANTANDERBRXM_DESCRIPTION_CMS = "santanderbrxm:descriptionCMS";
    public static final String SANTANDERBRXM_DESCRIPTION_OVERVIEW = "santanderbrxm:descriptionOverview";
    public static final String SANTANDERBRXM_DESCRIPTION = "santanderbrxm:description";
    public static final String SANTANDERBRXM_STATUS = "santanderbrxm:status";
    public static final String SANTANDERBRXM_APICONTAINERITEM = "santanderbrxm:ApiContainerItem";
    public static final String SANTANDERBRXM_VERSION_IL = "santanderbrxm:versionIL";
    public static final String SANTANDERBRXM_PK ="santanderbrxm:PK";
    public static final String SANTANDERBRXM_DEPRECATED="santanderbrxm:deprecated";
    public static final String SANTANDERBRXM_MODE="santanderbrxm:mode";
    public static final String SANTANDERBRXM_LOCALCOUNTRY="santanderbrxm:localCountry";
    public static final String SANTANDERBRXM_LOCALCOUNTRY_TYPE="santanderbrxm:LocalCountry";
    public static final String SANTANDERBRXM_YAML="santanderbrxm:yaml";
    public static final String STATUS_REAL ="Real";
    public static final String SANTANDERBRXM_ICON="santanderbrxm:icon";
    public static final String SANTANDERBRXM_POSTMAN="santanderbrxm:postmanCollectionlink";
    public static final String SANTANDERBRXM_LINKS="santanderbrxm:links";
    public static final String SANTANDERBRXM_ICON_AUTHOR="santanderbrxm:iconAuthor";
    public static final String SANTANDERBRXM_GLOBALEXPOSURE ="santanderbrxm:globalExposure";
    public static final String SANTANDERBRXM_TYPE ="santanderbrxm:type";
    public static final String SANTANDERBRXM_DOCUMENTATION="santanderbrxm:documentation";
    public static final String SANTANDERBRXM_DOCUMENTATION_API="santanderbrxm:document";
    public static final String SANTANDERBRXM_MENU_DOCUMENTATION="santanderbrxm:menuDoc";
    public static final String SANTANDERBRXM_UPDATEDATE ="santanderbrxm:updateDate";

    public static final String HIPPO_DOCBASE = "hippo:docbase";
    public static final String HIPPO_CONTENT ="hippostd:content";
    public static final String HIPPO_VALUES = "hippo:values";
    public static final String HIPPO_MODES ="hippo:modes";
    public static final String HIPPO_FACETS ="hippo:facets";

    //Path where the documents will be stored
    public static final String PRODUCER_ORGANIZATIONS_FOLDER = 
    		"/content/documents/santander/santander-paymentshub/en_gb/content/producercontainers/";
    
    public static final String EN_GB = "en-gb";

    //querys
    public static final String ORDERBY_VERSIONIL_AND_VERSIONCMS ="' ] order by @santanderbrxm:versionIL, @santanderbrxm:versionCMS";


    //Doctypes
    public static final String SANTANDERBRXM_PRODUCER_ORGANIZATION = "santanderbrxm:ProducerOrganizationContainer";
    public static final String SANTANDERBRXM_PRODUCT_CONTAINER = "santanderbrxm:ProductContainerItem";

  //------------------
    //**WORKFLOWS
    //------------------
    public static final String CONTENT_FOLDER = "/content/documents/santander/";
    public static final String TRANSLATOR_PREFIX_GROUP= "translator_";
    
    //Pagonext
    public static final Map<String, String[]> MAPPING = new HashMap<>() ;
    static {
        MAPPING.put(AUTHOR, new String[]{TRANSLATOR, EDITOR});
        MAPPING.put(TRANSLATOR, new String[]{AUTHOR});
        MAPPING.put(EDITOR, new String[]{AUTHOR});
    }


   //--------------------------
     //**INTEGRACION AZURE AD
     //---------------------------
     public static final String GROUPS_BRXM_AD_DEV = "536be3f5-85c9-41cb-869c-d31501d1397b/admin,8885c6a2-783c-44a8-991f-9832a5a55286/author_SMPS,55bb4521-09b3-4b1d-950d-10134c0e3edc/author_Paymentshub,2dfae68f-6683-4944-89d8-ff411480cc70/author_Superdigital,8885c6a2-783c-44a8-991f-9832a5a55286/author_OneTrade,6001aa62-3184-48fe-a5a6-e34c90bbcbfc/editor_SMPS,07c1c48b-e188-4f75-856f-cdff1a994b78/editor_Paymentshub,33e6a43d-0c6c-42c9-a3f2-a7a8d637bffd/editor_Superdigital,6a0dd1fe-6b9c-4480-aeb4-a07b9bcaee83/editor_OneTrade,e427f579-a5d0-438c-91ff-07f48634de77/translator_SMPS,92be02d1-749c-4549-8125-70d452c5a831/translator_Paymentshub,523f58c4-f12d-476b-9d51-a4bfaedefdf4/translator_Superdigital,20897673-3123-4387-8b06-64164e6b0e1a/translator_OneTrade";
     public static final String GROUPS_BRXM_AD_PRE = "ad4f68d5-6e39-418d-b882-c42bfd8dba70/admin,3ce56f30-fc41-4fca-b205-bde4f0cb53f7/author_SMPS,17232aa6-379a-402c-a40e-3307b0776a7a/author_Paymentshub,c12b76c5-65f1-4650-9467-b88a6af1e979/author_Superdigital,15346ef6-2012-48eb-b25a-8f7819387abf/author_OneTrade,ecd542f6-cbb3-455f-87ca-4cbf5eacedff/editor_SMPS,5f07f653-5c72-4ba5-af19-bf2c7628f0ba/editor_Paymentshub,a70a1f69-e538-47a9-bdaf-68490718132c/editor_Superdigital,4172c9c7-5a0e-4029-b864-46d38fb9259c/editor_OneTrade,38282221-0437-4b67-9789-4d7b84e752c4/translator_SMPS,1e97c7ed-e754-4dea-a8c1-9faaa73a8473/translator_Paymentshub,7659a792-fe0f-4d8b-943a-f6d2b5846a35/translator_Superdigital,7b8b2602-9bc1-44ff-bf62-786238cbe333/translator_OneTrade";
     public static final String GROUPS_BRXM_AD_PRO = "a7d2c13e-5d6d-465a-b7a5-aa86e5d945a6/admin,4f5dc3ab-d6b4-4215-bace-519b75b0f2f4/author_SMPS,52e6c5bd-2b85-4b86-aa68-2af5f7a6b162/author_Paymentshub,037c4ef9-aae5-4218-a80c-4a32db66b2ea/author_Superdigital,c95ac5c9-a977-4906-877e-a732f0f2fd65/author_OneTrade,76f6f16a-2507-4a14-9acb-18fc33291e55/editor_SMPS,94939e34-55de-4358-bbff-8d1b145a8770/editor_Paymentshub,362e2d25-4610-4a96-a753-850d1d381a8e/editor_Superdigital,4b284b0b-08ab-44ec-b30c-eeca904571fa/editor_OneTrade,3d63d09c-1abf-4e8b-a5f9-e7268d53c79d/translator_SMPS,3a6117b8-f539-4fcd-af0c-29f4c6af1f5c/translator_Paymentshub,fbc59a0b-5584-4f58-9fe8-7bc35e4bbe2c/translator_Superdigital,9fc92520-b1be-4b03-9525-6edf1cde88c8/translator_OneTrade";

}