package com.santander.utils;


import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;

import org.hippoecm.hst.core.request.HstRequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GlobalConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalConfiguration.class);

    public String getConfigurationById(String id, HstRequestContext requestContext) {

        try {
        	

            Session threadSafeSession = requestContext.getSession()
            		.impersonate(new SimpleCredentials(requestContext.getSession().getUserID(), new char[0]));

            Query query = threadSafeSession.getWorkspace().getQueryManager().createQuery(
                    "/jcr:root/content/documents"
                    + "/santander//element(*, santanderbrxm:SantanderGlobalConfiguration) "
                    + "[@santanderbrxm:id ='" + id + "' ] ", Query.XPATH);
            QueryResult queryResult = query.execute();
        	
            for (NodeIterator nodeIterator = queryResult.getNodes(); nodeIterator.hasNext(); ) 
		    {
            	Node node = nodeIterator.nextNode();
            	
            	return node.getProperty("santanderbrxm:value").getString();
		    }
            


        } catch (RepositoryException e) {
            LOGGER.error("Error get configuration by ID", e);
        }

        return null;


    }
    
    
    
    public String getConfigurationById(String id, Session session) {

        try {
        	


            Query query = session.getWorkspace().getQueryManager().createQuery(
                    "/jcr:root/content/documents"
                    + "/santander//element(*, santanderbrxm:SantanderGlobalConfiguration) "
                    + "[@santanderbrxm:id ='" + id + "' ] ", Query.XPATH);
            QueryResult queryResult = query.execute();
        	
            for (NodeIterator nodeIterator = queryResult.getNodes(); nodeIterator.hasNext(); ) 
		    {
            	Node node = nodeIterator.nextNode();
            	
            	return node.getProperty("santanderbrxm:value").getString();
		    }
            


        } catch (RepositoryException e) {
            LOGGER.error("Error get configuration by ID", e);
        }

        return null;


    }
    
    
}
