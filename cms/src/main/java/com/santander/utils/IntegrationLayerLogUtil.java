/**
 * Wrapper log class that creates logs with santander format and sends mails
 *
 */
package com.santander.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.gson.Gson;
import com.santander.sync.model.SantanderLogModel;
import com.santander.sync.model.WrapperAPIContainerPayload;
import com.santander.sync.model.WrapperProducerOrgPayload;
import com.santander.sync.model.WrapperProductPayload;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Value;
import javax.jcr.query.Query;
import javax.jcr.query.QueryResult;
import javax.mail.Authenticator;
import javax.mail.Session;
import static com.santander.utils.Constants.NO_MAIL;
import static com.santander.utils.Constants.SUCCESS;


/**
 * Wrapper class for writing logs in Satander's format
 * @author marcos.cordon
 *
 */
public final class IntegrationLayerLogUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(IntegrationLayerLogUtil.class);

	public static final String IL_NOTIFICATION_RECIPIENTS = "IL_NOTIFICATION_RECIPIENTS_";
	public static final String IL_NOTIFICATION_RECIPIENTS_ERROR = "IL_NOTIFICATION_RECIPIENTS_ERROR_";
	public static final String IL_NOTIFICATION_SUBJECT ="IL_NOTIFICATION_SUBJECT_";
	public static final String IL_NOTIFICATION_SUBJECT_ERROR = "IL_NOTIFICATION_SUBJECT_ERROR_";
	public static final String IL_NOTIFICATION_BODY = "IL_NOTIFICATION_BODY_";
	public static final String IL_NOTIFICATION_BODY_ERROR = "IL_NOTIFICATION_BODY_ERROR_";
	public static final String IL_NOTIFICATION_FROM = "IL_NOTIFICATION_FROM_";
	public static final String IL_NOTIFICATION_SITE = "IL_NOTIFICATION_SITE_";
	public static final String IL_NOTIFICATION_HOST = "IL_NOTIFICATION_HOST_";
	public static final String IL_NOTIFICATION_PORT = "IL_NOTIFICATION_PORT_";
	public static final String IL_NOTIFICATION_USER = "IL_NOTIFICATION_USER_";
	public static final String IL_NOTIFICATION_PASSWORD = "IL_NOTIFICATION_PASSWORD_";
	public static final String IL_NOTIFICATION_TLS      = "IL_NOTIFICATION_TLS_";
	public static final String NOTIFICATION_BODY_TYPE = "_NOTIFICATION_BODY_TYPE";
	public static final String NOTIFICATION_BODY_NAME = "_NOTIFICATION_BODY_NAME";
	public static final String NOTIFICATION_BODY_VERSION = "_NOTIFICATION_BODY_VERSION";
	public static final String NOTIFICATION_BODY_SITE = "_NOTIFICATION_BODY_SITE";
	private static final Gson gson = new Gson();

	private IntegrationLayerLogUtil()
	{
		//nothing
	}

	public static void error(String entryId, String component, String environment, String eventId, String userId
			,String description,String eventData ) {
		SantanderLogModel logModel = new SantanderLogModel();

		logModel.setTimeStamp(String.valueOf(System.currentTimeMillis()));
		logModel.setEntryId(entryId);
		logModel.setComponent(component);
		logModel.setEnviroment(environment);
		logModel.setEventId(eventId);
		logModel.setUserId(userId);
		logModel.setDescription(description);
		logModel.setEventData(eventData);

		String logBody = gson.toJson(logModel);

		LOGGER.error(logBody);



	}
	
	public static void sendApiMail(String typeEmail,WrapperAPIContainerPayload apiContainerPayload 
			,javax.jcr.Session sessionJCR2) {
		
		LOGGER.info("Sending api created mail.typeEmail {}", typeEmail);
		
		
		try
		
		{
			Map<String,String> mailProperties = new HashMap<>();
		
			//AL getSiteBasedProperties hay que pasar el nombre del SITE.
			//Cuando llega el del API, le llega el ID. Hay que conseguir el NOMBRE DEL SITE. FALTAAAAA
			
			String producerOrgId=apiContainerPayload.getPayload().getContainerProducerOrg();
		
			getSiteBasedProperties(mailProperties,producerOrgId,sessionJCR2);
			
			if (!mailProperties.containsKey(IL_NOTIFICATION_SITE))
			{
				LOGGER.warn(NO_MAIL);
				return;
			}
			
			String changedBody="";
			if (typeEmail.equals(SUCCESS)) {
			 changedBody = mailProperties.get(IL_NOTIFICATION_BODY);
			}
			else {
					changedBody = mailProperties.get(IL_NOTIFICATION_BODY_ERROR);
				
			}
			
						
			changedBody = changedBody.replace(NOTIFICATION_BODY_NAME, apiContainerPayload.getPayload().getTitle());
			changedBody = changedBody.replace(NOTIFICATION_BODY_TYPE, "API");
			changedBody = changedBody.replace(NOTIFICATION_BODY_VERSION, apiContainerPayload.getPayload().getVersion());
			changedBody = changedBody.replace(NOTIFICATION_BODY_SITE, mailProperties.get(IL_NOTIFICATION_SITE));
			mailProperties.put(IL_NOTIFICATION_BODY,changedBody);
			
			
			
			sendMail(typeEmail,mailProperties);
		} catch (Exception e)
		{
			LOGGER.error("Error sending api notification",e);
		}
	}
	

	public static void sendProductMail(String typeEmail, WrapperProductPayload productContainerPayload 
			,javax.jcr.Session sessionJCR2)	{
		
		LOGGER.info("Sending product created mail.typeEmail {}", typeEmail);
		String producerOrgId=productContainerPayload.getPayload().getContainerProducerOrg();
		try
		
		{
			Map<String,String> mailProperties = new HashMap<>();
			
			
			//AL getSiteBasedProperties hay que pasar el nombre del SITE.
			//Cuando llega el del API, le llega el ID. Hay que conseguir el NOMBRE DEL SITE. FALTAAAAA
			
			getSiteBasedProperties(mailProperties,producerOrgId,sessionJCR2);
		
			
			if (!mailProperties.containsKey(IL_NOTIFICATION_SITE))
			{
				LOGGER.warn("No mail configured for producer organization");
				return;
			}
			
			String changedBody="";
			if (typeEmail.equals(SUCCESS)) {
			 changedBody = mailProperties.get(IL_NOTIFICATION_BODY);
			}
			else {
				changedBody = mailProperties.get(IL_NOTIFICATION_BODY_ERROR);
			}
			changedBody = changedBody.replace(NOTIFICATION_BODY_NAME, productContainerPayload.getPayload().getName());
			changedBody = changedBody.replace(NOTIFICATION_BODY_TYPE, "Product");
			changedBody = changedBody.replace(NOTIFICATION_BODY_VERSION, productContainerPayload.getPayload().getVersion());
			changedBody = changedBody.replace(NOTIFICATION_BODY_SITE, mailProperties.get(IL_NOTIFICATION_SITE));
			
			
			
			mailProperties.put(IL_NOTIFICATION_BODY,changedBody);
			
			sendMail(typeEmail,mailProperties);
			
			
		} catch (Exception e)
		{
			LOGGER.error("Error sending api notification",e);
		}
	}

	
	public static void sendProducerOrgMail(String typeEmail, WrapperProducerOrgPayload produccerOrgContainerPayload 
			,javax.jcr.Session sessionJCR2)	{
		
		LOGGER.info("Sending producer org created mail. typeEmail{}", typeEmail);
		
		
		try
		
		{
			Map<String,String> mailProperties = new HashMap<>();
		
			
			getSiteBasedProperties(mailProperties,produccerOrgContainerPayload.getPayload().getId(),sessionJCR2);
						
			
			
			if (!mailProperties.containsKey(IL_NOTIFICATION_SITE))
			{
				LOGGER.warn("No mail configured for producer organization");
				return;
			}
			
			
			
			String changedBody="";
			if (typeEmail.equals(SUCCESS)) {
			 changedBody = mailProperties.get(IL_NOTIFICATION_BODY);
			
			}
			else {
				changedBody = mailProperties.get(IL_NOTIFICATION_BODY_ERROR);
				
			}
			
			
			if (produccerOrgContainerPayload.getPayload().getName()!=null) {
				changedBody = changedBody.replace(NOTIFICATION_BODY_NAME,produccerOrgContainerPayload.getPayload().getName());
				
				
			}
			else if (produccerOrgContainerPayload.getPayload().getId()!=null) { 
				
				changedBody = changedBody.replace(NOTIFICATION_BODY_NAME,produccerOrgContainerPayload.getPayload().getId());
				
			}
			else {
				
			
				changedBody = changedBody.replace(NOTIFICATION_BODY_NAME,"");
				
			}
			
			
			
			changedBody = changedBody.replace(NOTIFICATION_BODY_TYPE, "Producer Organization");
			
			changedBody = changedBody.replace(NOTIFICATION_BODY_VERSION, "");
			
			changedBody = changedBody.replace(NOTIFICATION_BODY_SITE, mailProperties.get(IL_NOTIFICATION_SITE));
			
			mailProperties.put(IL_NOTIFICATION_BODY,changedBody);
			
			
			sendMail(typeEmail,mailProperties);
			
		} catch (Exception e)
		{
			LOGGER.error("Error sending prodOrganization notification {}",e.getMessage());
		}
	}

	private static void getSiteBasedProperties
		(Map<String,String> mailProperties , String prodOrganization,javax.jcr.Session sessionJCR2) throws RepositoryException
	{
		
		
		GlobalConfiguration globalConfiguration = new GlobalConfiguration();
		String site= globalConfiguration.getConfigurationById(IL_NOTIFICATION_SITE+prodOrganization, sessionJCR2);
	
		if (site==null)
		{
				return;
		}
		mailProperties.put(IL_NOTIFICATION_BODY, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_BODY+site, sessionJCR2));
		mailProperties.put(IL_NOTIFICATION_BODY_ERROR,
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_BODY_ERROR+site, sessionJCR2));
		mailProperties.put(IL_NOTIFICATION_FROM, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_FROM+site, sessionJCR2));
		mailProperties.put(IL_NOTIFICATION_SUBJECT, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_SUBJECT+site, sessionJCR2));
		mailProperties.put(IL_NOTIFICATION_SUBJECT_ERROR, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_SUBJECT_ERROR+site, sessionJCR2));
		mailProperties.put(IL_NOTIFICATION_HOST, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_HOST+site, sessionJCR2));
		mailProperties.put(IL_NOTIFICATION_PORT, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_PORT+site, sessionJCR2));
		mailProperties.put(IL_NOTIFICATION_USER, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_USER+site, sessionJCR2));	
		mailProperties.put(IL_NOTIFICATION_PASSWORD, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_PASSWORD+site, sessionJCR2));
		mailProperties.put(IL_NOTIFICATION_TLS, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_TLS+site, sessionJCR2));
		
		mailProperties.put(IL_NOTIFICATION_RECIPIENTS, getReceiptsForSite(site,sessionJCR2));
		
		mailProperties.put(IL_NOTIFICATION_RECIPIENTS_ERROR, 
				globalConfiguration.getConfigurationById(IL_NOTIFICATION_RECIPIENTS_ERROR+site, sessionJCR2));
		mailProperties.put(IL_NOTIFICATION_SITE,site);
		
	}

	private static void sendMail(String typeEmail, Map<String,String> mapProperties)  {
		try {
			
			LOGGER.info("INICIO ENVIO EMAIL");

			final HtmlEmail email = new HtmlEmail();
			
			final Session session = getSession(mapProperties);
			
			//RECIPIENTS
			String[] to2;
			if (typeEmail.equals("success")){
				to2=mapProperties.get(IL_NOTIFICATION_RECIPIENTS).split(",");
				LOGGER.info("IDESTINATARIOS EDITORES DEL SITE");
			}
			else {
				to2  = mapProperties.get(IL_NOTIFICATION_RECIPIENTS_ERROR).split(",");
				LOGGER.info("DESTINATARIOS COGIDOS DE IL_NOTIFICATION_RECIPIENTS_ERROR");
			}
			
					
			if(to2[0].length()!=0){
				LOGGER.info("Hay destinatarios de correo");
				
				if (session == null) {
					LOGGER.error("Unable to send mail; no mail session available");
					return;
				}
				
				//body value
				String html =  mapProperties.get(IL_NOTIFICATION_BODY);
				
				//SESSION
				email.setMailSession(session);
				
				
				//FROM
				email.setFrom(mapProperties.get(IL_NOTIFICATION_FROM));
				
			
				//SUBJECT
				//subject value
				String subject2="";

				if (typeEmail.equals("success")){
					subject2  = mapProperties.get(IL_NOTIFICATION_SUBJECT);
				}
				else{
					subject2  = mapProperties.get(IL_NOTIFICATION_SUBJECT_ERROR);
				}
	
				if( subject2!=null)	{
					email.setSubject(subject2);
				}
				else{
					email.setSubject("Exception Integration Layer");
				}
				
				
				//RECIPIENTS
				for(int cnt=0;cnt<to2.length;cnt++)
				{
					email.addTo(to2[cnt]);
					
				}
				
				//BODY
				email.setHtmlMsg(html);
				

				
				if (StringUtils.isNotEmpty(html)) 
				{
					email.setHtmlMsg(html);
					
				} else 
				{
					LOGGER.error("No HTML template available");
					return;
				}
				email.send();
				
			
			
			}else
			{
				LOGGER.info("No recipients to send the email"); 
			}

		} catch (EmailException e) {
			LOGGER.error("mail Exception {}", e.getMessage());
						
		}

	}

	static Session getSession(Map<String,String> mailProperties) 
	{
		Properties props = new Properties();
		try {
			
			String mailhost = mailProperties.get(IL_NOTIFICATION_HOST);
			String mailport = mailProperties.get(IL_NOTIFICATION_PORT);
			String username = mailProperties.get(IL_NOTIFICATION_USER);
			String password = mailProperties.get(IL_NOTIFICATION_PASSWORD);
			String tls = mailProperties.get(IL_NOTIFICATION_TLS);
			
			LOGGER.info("valor de HOST por variables de entorno: {}", mailhost);
			LOGGER.info("valor de PORT por variables de entorno: {}", mailport);
			LOGGER.info("valor de USER por variables de entorno: {}", username);

			if (tls!=null && "true".equals(tls))
			{
				props.put("mail.smtp.starttls.enable", "true");
			}
			props.put("mail.smtp.host", mailhost);
			props.put("mail.smtp.port", mailport);

			if (!"false".equals(username)) 
			{
				props.put("mail.smtp.auth", "true");
				Authenticator authenticator = new DefaultAuthenticator(username, password);
				return Session.getInstance(props, authenticator);
			} else {
				props.put("mail.smtp.auth", "false");
				return Session.getInstance(props);
			}

		} catch (Exception e) 
		{
			LOGGER.error("Error envio por variables de entorno", e);
		}
		return Session.getInstance(props);

}

	private static String  getReceiptsForSite(String site, javax.jcr.Session sessionJCR2) throws RepositoryException
	{
	
		String resultMails="";
		String queryString="SELECT * from [hipposys:group] as sitegroups where LOWER(NAME())='editor_"+site+"' ";
    			
    			LOGGER.info("querySTring {}",queryString);
    			Query query = sessionJCR2.getWorkspace().getQueryManager().createQuery(
    					queryString,Query.JCR_SQL2);
		
    			QueryResult result=query.execute();
    			
    			NodeIterator nodeIterator= result.getNodes();
    			if (nodeIterator.hasNext())  
    			{
    				Node group=nodeIterator.nextNode();
    				
    				
    				Property members =group.getProperty("hipposys:members");
    				
    					
    					for (Value value:members.getValues())
    					{
    						
    						String userName=value.getString();
    						LOGGER.info("member {}",userName);
    						String queryStringUser="SELECT * from [hipposys:user] as users where NAME()='"+userName+"' ";
    						
    						
    						
    						Query queryUser = sessionJCR2.getWorkspace().getQueryManager().createQuery(
    		    					queryStringUser,Query.JCR_SQL2);
    						QueryResult userResult=queryUser.execute();
    		    			
    		    			NodeIterator nodeUserIterator= userResult.getNodes();
    		    			if (nodeUserIterator.hasNext())  
    		    			{
    		    				Node user=nodeUserIterator.nextNode();
    		    				
    		    				
    		    				try
    		    				{
    		    					Property mail =user.getProperty("hipposys:email");
    		    					
    		    					resultMails=resultMails.isEmpty()?mail.getString():(resultMails+","+mail.getString());
    		    					
    		    				} catch (Exception e)
    		    				{
    		    					LOGGER.error("user {} has no mail",value.getString());
    		    				}
    		    				
    		    				
    		    			}
    				
    						
    						
    					}
    				
    				
    			} 
    			LOGGER.info("Numero de editores del site found {}:", nodeIterator.getSize());
    			LOGGER.info("Mails de los editores {}", resultMails);
    			
    			
    		return resultMails;
		
	}
	
	

}
