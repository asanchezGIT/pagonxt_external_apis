package com.santander.advancedsearch;

import com.onehippo.cms7.search.frontend.ISearchContext;
import com.onehippo.cms7.search.frontend.constraints.IConstraintProvider;
import com.onehippo.cms7.search.frontend.constraints.YesNoNeither;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.hippoecm.frontend.plugin.IPluginContext;
import org.hippoecm.frontend.plugin.config.IPluginConfig;
import org.hippoecm.frontend.service.render.RenderPlugin;
import org.onehippo.cms7.services.search.query.QueryUtils;
import org.onehippo.cms7.services.search.query.constraint.Constraint;
import org.onehippo.cms7.services.search.query.constraint.TextConstraint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.ItemNotFoundException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class RejectedDocumentsPlugin extends RenderPlugin implements IConstraintProvider {

    private static final Logger log = LoggerFactory.getLogger(RejectedDocumentsPlugin.class);
    public static final String HIPPO_REQUEST_REVIEWWORKFLOW_ASSIGNTO = "../hippo:request/@reviewworkflow:assignto";
    public static final String REQUEST = "request";
    public static final String HIPPO_REQUEST_REVIEWWORKFLOW_STATE = "../hippo:request/@reviewworkflow:state";
    public static final String YES = "yes";
    public static final String NO = "no";
    public static final String FORM = "form";
    public static final String REVIEW_REJECTED_MINE = "rejectedToMe";

    private String rejectedToMe;

    public RejectedDocumentsPlugin(final IPluginContext context,
                                 final IPluginConfig config) {
        super(context, config);
        Form form = new Form(FORM, new CompoundPropertyModel(this));

        form.add(new YesNoNeither(REVIEW_REJECTED_MINE) {

            @Override
            protected void onModelChanged() {
                super.onModelChanged();
                updateSearchResults();
            }
        });
        add(form);
    }

    private void updateSearchResults() {
        ISearchContext searcher = getPluginContext().getService(
                ISearchContext.class.getName(),
                ISearchContext.class);
        searcher.updateSearchResults();
    }

    @Override
    public List<Constraint> getConstraints() {
        try {
            List<Constraint> constraints = new LinkedList<>();
            /*Documents in review*/


            /*Assigned to me or my group(s)*/


            /*Rejected assigned to me*/
            if (rejectedToMe != null) {
                TextConstraint rejectedToMeConstraint = QueryUtils.text(HIPPO_REQUEST_REVIEWWORKFLOW_ASSIGNTO)
                		.isEqualTo(getSession().getJcrSession().getUser().getId());
                if (YES.equals(rejectedToMe)) {
                    constraints.add(rejectedToMeConstraint);
                } else if (NO.equals(rejectedToMe)) {
                    constraints.add(QueryUtils.not(rejectedToMeConstraint));
                } else
                {
                	//Do nothing
                }
            }

            return constraints;
        } catch (
                ItemNotFoundException e) {
            log.error("User not found",e);
        }
        return Collections.emptyList();
    }

    @Override
    public void clearConstraints() {

        rejectedToMe = null;
        redraw();
    }
}
