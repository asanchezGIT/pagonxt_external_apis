package com.santander.security;

import org.apache.xmlbeans.SimpleValue;
import org.hippoecm.frontend.model.UserCredentials;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.NameID;
import org.opensaml.saml2.core.RequestAbstractType;
import org.opensaml.saml2.metadata.ServiceDescription;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.schema.impl.XSAnyImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.saml.SAMLCredential;

import com.santander.utils.Constants;

import javax.jcr.SimpleCredentials;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.santander.security.SSOUserState.SAML_GROUPS;

public class LoginSuccessFilter implements Filter {

    private static final Logger LOGGER = LoggerFactory.getLogger( LoginSuccessFilter.class );

    private static final String SSO_USER_STATE = SSOUserState.class.getName();

    private static ThreadLocal<SSOUserState> tlCurrentSSOUserState = new ThreadLocal<SSOUserState>();
    private static final String LOCALHOST = "localhost" ;
    public static final String GROUPS_BRXM_AD_DEV = Constants.GROUPS_BRXM_AD_DEV;
    public static final String GROUPS_BRXM_AD_PRE = Constants.GROUPS_BRXM_AD_PRE;
    public static final String GROUPS_BRXM_AD_PRO = Constants.GROUPS_BRXM_AD_PRO;


    @Override
    public void init(FilterConfig filterConfig) {
        //do nothing
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	
    	//AQUI ENTRA CUANDO RECIBE LA RESPUESTA DEL ACTIVE DIRECTORY 
        LOGGER.info("doFilter LoginSuccessFilter");

        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (!authentication.isAuthenticated()){
            LOGGER.debug("User not authenticated");
            chain.doFilter(request, response);
            return;
        }

        // Check if the user already has a SSO user state stored in HttpSession before.
        HttpSession session = ((HttpServletRequest) request).getSession();
        String requestUrl=((HttpServletRequest) request).getRequestURL().toString();
        LOGGER.info("VALOR requestUrl:"+requestUrl);

        SSOUserState userState = (SSOUserState) session.getAttribute(SSO_USER_STATE);

        if(userState == null || !userState.getSessionId().equals(session.getId())) //Si el usuario no está logado 
        {
        	LOGGER.debug("Usuario no logado");
            if (authentication.getCredentials() instanceof SAMLCredential) //Si es una identificación de SAML
            {
            	LOGGER.debug("Solicitud identificación SAML");
            	//Recoge la información de las credenciales del SAML
                SAMLCredential samlCredential = (SAMLCredential) authentication.getCredentials();
                //Obtenemos el nameID,nombre, apellidos y email
                final NameID nameID = samlCredential.getNameID();
                String nameUser = obtainNameUser(samlCredential.getAttribute("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname"));
                String lastName = obtainLastName(samlCredential.getAttribute("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname"));
                String email = obtainEmail(samlCredential.getAttribute("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"));


                //Grupos del usuario
                Attribute groups_attribute=samlCredential.getAttribute("http://schemas.microsoft.com/ws/2008/06/identity/claims/groups");
                
                List<String> groupsAD= new ArrayList<String>();
                List<String> groupsBrxm= new ArrayList<String>();
              
                if(groups_attribute!=null) {
                	LOGGER.debug("El usuario está asignado a grupos en el AD");
                	//recupera los valores del atributo(AD values)
                    groupsAD=getGroupsAD(groups_attribute);
                    //mapea los valores de AD con los de Brxm
                    groupsBrxm= getGroupsBrxm(groupsAD,requestUrl);

                }
                else {
                		LOGGER.debug("El usuario no tiene grupos asociados en AD");
                	
                }
                	 
                 
              //  if (nameID == null || groupsBrxm.size()==0) //Si no viene relleno el usuario en la informacion que llega del AD (Idp)
                if (nameID == null)
                {
                	
                    LOGGER.warn("nameID is null in SAML Credentials");
                    chain.doFilter(request, response);
                    return;
                }
                
                //Viene relleno el usuario y sus grupos.
                final String username = nameID.getValue();
                SimpleCredentials creds = new SimpleCredentials(username, "DUMMY".toCharArray()); 
                creds.setAttribute(SSOUserState.SAML_ID, username);
                creds.setAttribute("nameAttr", nameUser);
                creds.setAttribute("lastNameAttr", lastName);
                creds.setAttribute("email", email);

                // Se meten los grupos en el credentials y se asigna a la sesión y así recuperarlo en el
                LOGGER.debug("Se meten los grupos en el credentials");
                creds.setAttribute(SAML_GROUPS, groupsBrxm);
                userState = new SSOUserState(new UserCredentials(creds), session.getId());
                session.setAttribute(SSO_USER_STATE, userState);
                

            } else { // No es identificacón de SAML. Mostrará el formulario de Brxm
            	LOGGER.debug("Authenticated user credentials are not SAML credentials.");
                chain.doFilter(request, response);
                return;
            }

        }

        // If the user has a valid SSO user state, then
        // set a JCR Credentials as request attribute (named by FQCN of UserCredentials class).
        
        // Then the CMS application will use the JCR credentials passed through this request attribute.
        if (userState.getSessionId().equals(session.getId())) { //Pasa las credenciales por el request en el atributo UserCredentials.class.getName()
        	LOGGER.debug("Pasa las credenciales por el request");
          	request.setAttribute(UserCredentials.class.getName(), userState.getCredentials());
        }
       
        
        try {
        	LOGGER.debug("Redireccion al CustomDelegatingSecurityProvider");
            tlCurrentSSOUserState.set(userState);
            chain.doFilter(request, response);
        } 
        catch(Exception e) {
        	LOGGER.error("Error Redireccion al CustomDelegatingSecurityProvider");
            tlCurrentSSOUserState.remove();
        }

    }

    private String obtainEmail(Attribute emailAttr) {
        List<XMLObject> emailList = new ArrayList<XMLObject>();
        String email="";

        if(emailAttr!=null) {
            emailList=emailAttr.getAttributeValues();
            for (XMLObject xmlobject : emailList) {
                email = ((XSAnyImpl) xmlobject).getTextContent();
            }
        }
        return email;
    }

    private String obtainLastName(Attribute lastNameAttr) {
        List<XMLObject> lastNames = new ArrayList<XMLObject>();
        String last="";

        if(lastNameAttr!=null) {
            lastNames=lastNameAttr.getAttributeValues();
            for (XMLObject xmlobject : lastNames) {
                last = ((XSAnyImpl) xmlobject).getTextContent();
            }
        }
        return last;
    }

    private String obtainNameUser(Attribute nameAttr) {
        List<XMLObject> names = new ArrayList<XMLObject>();
        String nameUser="";

        if(nameAttr!=null) {
            names=nameAttr.getAttributeValues();
            for (XMLObject xmlobject : names) {
                nameUser = ((XSAnyImpl) xmlobject).getTextContent();
            }
        }
        return nameUser;

    }

    /**
     * Get current <code>SSOUserState</code> instance from the current thread local context.
     * @return
     */
    static SSOUserState getCurrentSSOUserState() {
        return tlCurrentSSOUserState.get();
    }

    @Override
    public void destroy() {
        //Do nothing

    }

    private List<String> getGroupsAD(Attribute groups_attribute){
    	//Recupera los valores de los grupos de AD que vienen del IPL
     	 LOGGER.debug("INICIO getGroupsAD");
         List<XMLObject> groups=new ArrayList<XMLObject>();
         List<String> groupsAD=new ArrayList<String>();
         
    	 if(groups_attribute!=null) {
         	groups=groups_attribute.getAttributeValues();
          	for (XMLObject xmlobject : groups) {
                 String valueGroup = ((XSAnyImpl) xmlobject).getTextContent();
                
                 if (valueGroup != null && !valueGroup.isEmpty()) {
                 	
                	 groupsAD.add(valueGroup);
                 }
     
         	}
         	
         }
    	 LOGGER.debug("GruposAD:"+groupsAD);
    	 
    	 return groupsAD;
    	
    }
    private List<String> getGroupsBrxm(List<String> groups_AD,String requestUrl ){
    	//Filtra del los grupos del AD los que son propios de BRXM y se queda con los nombres de los grupos en BRxm a los que pertenece el usuario
    	
   		LOGGER.debug("INICIO getGroupsBrxm");

        //Se coge la relacion entre los IDs del AzureAD y los grupos de BRXM
   		String groups_value="";
        HashMap<String, String> mapBrxmAD = new HashMap<String, String>();

        LOGGER.info("VALOR requestURL:"+requestUrl);
        if (requestUrl.contains(LOCALHOST) || requestUrl.contains("dev")) {
            LOGGER.info("DEV o Localhost:"+ mapBrxmAD);
            mapBrxmAD=getmapBrxmAD(GROUPS_BRXM_AD_DEV);
        }

        else if (requestUrl.contains("pre")){
            LOGGER.info("PRE");
            mapBrxmAD=getmapBrxmAD(GROUPS_BRXM_AD_PRE);
        }
        else { //pro
            LOGGER.info("PRO");
            mapBrxmAD=getmapBrxmAD(GROUPS_BRXM_AD_PRO);
        }
        LOGGER.debug("mapBrxmAD:"+ mapBrxmAD);
       


    	List<String> groupsBrxm=new ArrayList<String>();
        
   	 	if(groups_AD!=null) {
    		for (String  groupAD : groups_AD) {
    			if (mapBrxmAD.containsKey(groupAD)) {
    				
    				groupsBrxm.add(mapBrxmAD.get(groupAD));
    			}
             }
       	}
        	
    
   	 LOGGER.debug("gruposBRXM:"+groupsBrxm);
   	 
   	 return groupsBrxm;
   	
   }
    
    private HashMap<String,String> getmapBrxmAD(String groupsBrxmAD){
    	
    	//Guarda en un ashMap la relacion entre el id del AzureAD y el nombre del grupo. Esta informacion está configurada en una variable de contants.
    	
    	//groupsBrxmAD tiene el formato idAD1/gupoBrxm1,idAD2/gupoBrxm2,idAD3/gupoBrxm3 y se mete en un HashMap
    	
    	HashMap<String, String> mapBrxmAD = new HashMap<String, String>();
		
    	String[] splitGroupsBrxmAD =groupsBrxmAD.split(",");
		
		
        if (splitGroupsBrxmAD.length > 1) {
        	
        	for (String item: splitGroupsBrxmAD) {
            	//item idAD1/gupoBrxm1
        		String idAD=item.split("/")[0];
        		String groupName=item.split("/")[1];
        		
        		mapBrxmAD.put(idAD,groupName);
        		
        		
            }
        }

   	 return mapBrxmAD;
   	
   }
  
   
    
    
    
}