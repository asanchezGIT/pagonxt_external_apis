package  com.santander.security;

import java.io.Serializable;

import org.hippoecm.frontend.model.UserCredentials;

/**
 * SSO User State object which contains a pair of JSESSIONID and <code>UserCredentials</code>.
 */
class SSOUserState implements Serializable {

    private static final long serialVersionUID = 1L;

    static final String SAML_ID = SSOUserState.class.getName() + ".saml.id";

    private final UserCredentials credentials;
    private final String sessionId;
    public static final String SAML_GROUPS = SSOUserState.class.getName() + ".saml.groups";
    public static final String SAML_DISPLAY_NAME = SSOUserState.class.getName() + ".saml.displayName";
    public static final String SAML_GIVEN_NAME = SSOUserState.class.getName() + ".saml.givenName";
    public static final String SAML_SURNAME = SSOUserState.class.getName() + ".saml.surname";
    public static final String SAML_NAME = SSOUserState.class.getName() + ".saml.name";
    public static final String SAML_EMAIL = SSOUserState.class.getName() + ".saml.email";
    public static final String HIPPOSYS_MEMBERS = "hipposys:members";




    SSOUserState(final UserCredentials credentials, final String sessionId) {
        this.credentials = credentials;
        this.sessionId = sessionId;
    }

    UserCredentials getCredentials() {
        return credentials;
    }

    String getSessionId() {
        return sessionId;
    }

}