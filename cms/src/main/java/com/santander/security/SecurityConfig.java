package  com.santander.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.extensions.saml2.config.SAMLConfigurer;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

import static org.springframework.security.extensions.saml2.config.SAMLConfigurer.saml;

@EnableWebSecurity
@Configuration
@PropertySource("classpath:application.properties")
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private static final String LOCALHOST = "localhost" ;
    private static final String TRUE = "true";
    private static final String SSO_LOCAL = "SSO_LOCAL";

    @Value("${security.saml2.metadata-url}")
    String metadataUrl;

    @Value("${server.ssl.key-alias}")
    String keyAlias;

    @Value("${PASSWORD_STORE_SAML}")
    String password;

    @Value("${server.ssl.key-store}")
    String keyStoreFilePath;

    @Value("${BRX_HOSTNAME:localhost}")
    String host;

    @Override
    protected void configure(HttpSecurity http) throws Exception {


        http
                .csrf(AbstractHttpConfigurer::disable)
                .headers()
                .frameOptions()
                .sameOrigin();

        if (LOCALHOST.equalsIgnoreCase(this.host)) {

            if(TRUE.equals(System.getenv(SSO_LOCAL))){

                http

                        .authorizeRequests()
                        .antMatchers("/cms/saml/SSO", "/*.gif", "/*.jpg", "/*.jpeg", "/*.png", "/*.jsp", "/*.js", "/*.css", "/console*").permitAll()
                        .anyRequest().authenticated()
                        .and()
                        .addFilterAfter(new LoginSuccessFilter(), FilterSecurityInterceptor.class)
                        .addFilterBefore(new CustomFilterLogout(), FilterSecurityInterceptor.class);

                final SAMLConfigurer saml = http.apply(saml());

                saml
                        .serviceProvider()
                        .keyStore()
                        .storeFilePath(this.keyStoreFilePath)
                        .password(this.password)
                        .keyname(this.keyAlias)
                        .keyPassword(this.password)
                        .and()
                        .entityId("https://brx.marketplace.pagonxt.corp")
                        .protocol("http")
                        .hostname(String.format("%s:%s", "localhost", "8080"))
                        .basePath("/cms")
                        .and()
                        .identityProvider()
                        .metadataFilePath(this.metadataUrl);

            }else{

                http
                        .authorizeRequests()
                        .anyRequest()
                        .permitAll();
            }

        }else {

            http

                    .authorizeRequests()
                    .antMatchers("/cms/saml/SSO", "/*.gif", "/*.jpg", "/*.jpeg", "/*.png", "/*.jsp", "/*.js", "/*.css", "/console*").permitAll()
                    .anyRequest().authenticated()
                    .and()
                    .addFilterAfter(new LoginSuccessFilter(), FilterSecurityInterceptor.class)
                    .addFilterBefore(new CustomFilterLogout(), FilterSecurityInterceptor.class);

            final SAMLConfigurer saml = http.apply(saml());

            saml
                    .serviceProvider()
                    .keyStore()
                    .storeFilePath(this.keyStoreFilePath)
                    .password(this.password)
                    .keyname(this.keyAlias)
                    .keyPassword(this.password)
                    .and()
                    .entityId("https://brx.marketplace.pagonxt.corp")
                    .protocol("https")
                    .hostname(host)
                    .basePath("/cms")
                    .and()
                    .identityProvider()
                    .metadataFilePath(this.metadataUrl);
        }
    }
}