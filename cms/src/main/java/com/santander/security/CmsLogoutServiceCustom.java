package com.santander.security;

import org.hippoecm.frontend.logout.CmsLogoutService;
import org.hippoecm.frontend.plugin.IPluginContext;
import org.hippoecm.frontend.plugin.config.IPluginConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CmsLogoutServiceCustom extends CmsLogoutService {

    private static Logger log = LoggerFactory.getLogger(CmsLogoutServiceCustom.class);

    public CmsLogoutServiceCustom(IPluginContext context, IPluginConfig config) {
        super(context, config);
    }

    @Override
    public void logout() {

        log.debug("logout");
        super.logout();
    }

    @Override
    protected void redirectPage() {
        super.redirectPage();
    }
}
