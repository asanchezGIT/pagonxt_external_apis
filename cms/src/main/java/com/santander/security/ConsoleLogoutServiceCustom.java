package com.santander.security;

import org.hippoecm.frontend.plugin.IPluginContext;
import org.hippoecm.frontend.plugin.config.IPluginConfig;
import org.hippoecm.frontend.plugins.logout.ConsoleLogoutService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsoleLogoutServiceCustom extends ConsoleLogoutService {
    private static Logger log = LoggerFactory.getLogger(ConsoleLogoutServiceCustom.class);

    public ConsoleLogoutServiceCustom(IPluginContext context, IPluginConfig config) {
        super(context, config);
    }

    @Override
    public void logout() {
        //Pendiente hacer

        log.debug("logout");

        super.logout();
    }


}
