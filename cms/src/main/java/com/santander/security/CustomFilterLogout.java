package com.santander.security;

import javax.servlet.*;
import java.io.IOException;

public class CustomFilterLogout implements Filter {


    @Override
    public void init(FilterConfig filterConfig)  {
        //do nothing
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        final String logoutRedirect = request.getParameter("loginmessage");
        if ("UserLoggedOut".equals(logoutRedirect)) {
            request.getRequestDispatcher("/WEB-INF/redirect.jsp").forward(request, response);
            response.flushBuffer();

        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        //do nothing
    }
}
