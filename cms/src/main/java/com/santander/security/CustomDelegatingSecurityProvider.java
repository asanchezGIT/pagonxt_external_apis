package com.santander.security;

import org.apache.commons.lang.*;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.jackrabbit.value.*;
import org.hippoecm.repository.security.*;
import org.hippoecm.repository.security.group.GroupManager;
import org.hippoecm.repository.security.user.*;
import org.slf4j.*;
import javax.jcr.*;
import java.util.*;

import static com.santander.security.SSOUserState.HIPPOSYS_MEMBERS;
import static com.santander.security.SSOUserState.SAML_GROUPS;

/**
 * Custom <code>org.hippoecm.repository.security.SecurityProvider</code> implementation.
 * <p>
 * Hippo Repository allows to set a custom security provider for various reasons (e.g, SSO) for specific users.
 * If a user is associated with a custom security provider, then Hippo Repository invokes
 * the custom security provider to do authentication and authorization.
 * </P>
 */
public class CustomDelegatingSecurityProvider extends DelegatingSecurityProvider {

    private static Logger log = LoggerFactory.getLogger(CustomDelegatingSecurityProvider.class);

    private HippoUserManager userManager;

    /**
     * Constructs by creating the default <code>RepositorySecurityProvider</code> to delegate all the other calls
     * except of authentication calls.
     *
     * @throws RepositoryException
     */
    public CustomDelegatingSecurityProvider() throws RepositoryException {
        super(new RepositorySecurityProvider());
    }

    /**
     * Returns a custom (delegating) HippoUserManager to authenticate a user by SAML Assertion.
     */
    @Override
    public UserManager getUserManager() throws RepositoryException {
    	log.debug("INICIO getUserManager");

        if (userManager == null) 
        {
            userManager = new DelegatingHippoUserManager((HippoUserManager) super.getUserManager()) 
            {
                @Override
                public boolean authenticate(SimpleCredentials creds) throws RepositoryException {
                    if (validateAuthentication(creds)) {
                    	
                    	log.debug("-----*****METODO AUTHENTICATE******");
                        String userId = creds.getUserID();

                        List<String> groupsUserAD= (List<String>) creds.getAttribute(SAML_GROUPS);

                        if (!hasUser(userId)) {
                          	log.debug("-----El USUARIO "+userId+" NO EXISTE");
                        	setUserPropertiesAndGroups(createUser(userId), groupsUserAD, getGroupManager(), creds);

                        }
                        else { // aqui habria que lograr que actualice si el usuario ya está creado, pq puede tener nuevos roles en el AD
                        	log.debug("-----El USUARIO "+userId+" SI EXISTE");

                            if(groupsUserAD.size()==0){
                                //Usuario que le han quitado los permisos de AD pero existe en BRXM
                                //Habra que quitarle los permisos y elimina el usuario
                                deleteUser(creds, getUser(userId));
                            }else{
                                updateUserGroups(creds);
                            }
                        }
                        return true;
                    } else {
                    	//Si entra por aqui muestra el el logado de BRXM y NO el automatico por AD
                    	log.debug("LOGIN DE BRXM");
                        return false;
                    }
                }
            };
        }
        return userManager;
    }

    /**
     * Validates SAML SSO Assertion.
     * <p>
     * In this example, simply invokes SAML API (<code>AssertionHolder#getAssertion()</code>) to validate.
     * </P>
     *
     * @param creds
     * @return
     * @throws RepositoryException
     */
    protected boolean validateAuthentication(SimpleCredentials creds) throws RepositoryException {

        log.debug("CustomDelegatingSecurityProvider validating credentials: {}", creds);

        SSOUserState userState = LoginSuccessFilter.getCurrentSSOUserState();

        /*
         * If userState found in the current thread context, this authentication request came from
         * CMS application.
         * Otherwise, this authentication request came from SITE application (e.g, channel manager rest service).
         */

        if (userState != null)
            {
                log.debug("userState not null");
            // Asserting must have been done by the *AssertionValidationFilter* and the assertion thread local variable
            // must have been set by AssertionThreadLocalFilter already.
            // So, simply check if you have assertion object in the thread local.
            return StringUtils.isNotEmpty(userState.getCredentials().getUsername());

        } else {
        	log.debug("userState is null");
            String samlId = (String) creds.getAttribute(SSOUserState.SAML_ID);

            if (StringUtils.isNotBlank(samlId)) {
                log.info("Authentication allowed to: {}", samlId);
                return true;
            }
        }

        return false;
    }

    protected void setUserPropertiesAndGroups(final Node user, final List<String> groupsUserAD, GroupManager groupManager, SimpleCredentials creds)
            throws RepositoryException {
    	
    	log.debug("INICIO setUserPropertiesAndGroups");
        user.setProperty("hipposys:securityprovider", "saml");
        user.setProperty("hipposys:active", true);
        user.setProperty("hipposys:firstname",creds.getAttribute("nameAttr").toString());
        user.setProperty("hipposys:lastname",creds.getAttribute("lastNameAttr").toString());
        user.setProperty("hipposys:email",creds.getAttribute("email").toString());

        updateGroups(groupsUserAD, groupManager, user);

    }

    private void updateGroups(List<String> listGroups, GroupManager groupManager,  final Node user)
            throws RepositoryException {

        //updating group members
        for(int i = 0;i<listGroups.size();i++){
            log.debug("GRUPO en Brxm"+ listGroups.get(i));
            Node grupoNode = groupManager.getGroup(listGroups.get(i).trim());
            log.debug("ASIGNAR EL USUARIO AL GRUPO" );

            Value[] values = grupoNode.getProperties("hipposys:members").nextProperty().getValues();
            Value[] newValues = Arrays.copyOf(values, values.length + 1);
            newValues[values.length] = new StringValue(user.getName());
            grupoNode.setProperty("hipposys:members", newValues);
        }
    }

    private void updateUserGroups(SimpleCredentials credentials) throws RepositoryException {
        String userId = credentials.getUserID();
        ArrayList<String> groups = (ArrayList<String>)credentials.getAttribute(SAML_GROUPS);

        // remove user from old groups
        NodeIterator iterator = getGroupManager().getMemberships(userId);
        while (iterator.hasNext()) {
            Node group = iterator.nextNode();
            if (groups
                    .stream()
                    .noneMatch(name -> {
                        try {
                            return name.equals(group.getName());
                        } catch (RepositoryException e) {
                            return false;
                        }
                    })) {
                removeMembership(userId, group);
            }
        }

        // add user to new groups
        for (String groupName : groups) {
            Node group = getGroupManager().getGroup(groupName);
            if (Objects.nonNull(group)) {
                ensureMembership(userId, group);
            }
        }
    }

    protected void removeMembership(final String userId, final Node group)
            throws RepositoryException {
        final Value[] values = group
                .getProperties(HIPPOSYS_MEMBERS)
                .nextProperty()
                .getValues();

        final Value[] newValues = Arrays
                .stream(values)
                .filter(value -> {
                    try {
                        return !value
                                .getString()
                                .equals(userId);
                    } catch (RepositoryException e) {
                        return true;
                    }
                })
                .toArray(Value[]::new);

        if(newValues.length != values.length) {
            log.debug("removing user '{}' from group '{}' ...", userId, group.getName());
        }

        group.setProperty(HIPPOSYS_MEMBERS, newValues);
    }

    private void deleteUser(SimpleCredentials credentials, Node userNode)
            throws RepositoryException {

        String userId = credentials.getUserID();

        // NOTE: we know always will be an ArrayList<String>
        @SuppressWarnings("unchecked")
        ArrayList<String> groups = (ArrayList<String>)credentials.getAttribute(SAML_GROUPS);

        // remove user from old groups
        NodeIterator iterator = getGroupManager().getMemberships(userId);
        while (iterator.hasNext()) {
            Node group = iterator.nextNode();
            removeMembership(userId, group);
        }

        /**   //BORRAMOS EL USUARIO
         userNode.remove();
         userNode.getSession().save();**/

    }

    protected void ensureMembership(final String userId, final Node group)
            throws RepositoryException {
        final Value[] values = group
                .getProperties(HIPPOSYS_MEMBERS)
                .nextProperty()
                .getValues();

        final boolean isMember = Arrays
                .stream(values)
                .anyMatch(value -> {
                    try {
                        return value
                                .getString()
                                .equals(userId);
                    } catch (RepositoryException e) {
                        return false;
                    }
                });

        if (!isMember) {
            log.debug("adding '{}' to group '{}' ...", userId, group.getName());
            Value[] newValues = Arrays.copyOf(values, values.length + 1);
            newValues[values.length] = new StringValue(userId);
            group.setProperty(HIPPOSYS_MEMBERS, newValues);
        }
    }
}