package com.santander.reviewworkflow;

import org.bloomreach.forge.reviewworkflow.cms.reviewedactions.AssignableGroupsProvider;
import org.hippoecm.hst.container.RequestContextProvider;
import org.hippoecm.hst.core.request.HstRequestContext;
import org.hippoecm.repository.impl.SessionDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import com.santander.utils.Constants;

public class AssignableGroupsProviderImpl implements AssignableGroupsProvider {

	public static final String CONTENT_FOLDER = Constants.CONTENT_FOLDER;
	 public static final String TRANSLATOR= Constants.TRANSLATOR_PREFIX_GROUP;
	 public static final Map<String, String[]> mapping= Constants.MAPPING;
	  

    private static final Logger log = LoggerFactory.getLogger(AssignableGroupsProviderImpl.class);

 
    @Override
    public Set<String> provideGroups(String currentUserId, String docAbsolutePath) {
        try {
        	log.info("******INICIO AssignableGroupsProviderImpl.java*************");
           	Set<String> assignableGroups = new HashSet<>();
            HstRequestContext requestContext = RequestContextProvider.get();
            Session session = requestContext.getSession();

            for (String myGroup : ((SessionDecorator) session).getUser().getMemberships()) {
                log.debug("******My group is: {}" , myGroup);
                for (String key : mapping.keySet()) 
                {
                    processMyGroups(assignableGroups, myGroup, key);
                }
            }
            return assignableGroups;

        } catch (RepositoryException e) {
            log.error("Error getting groups: ", e);
        }
        return Collections.emptySet();
    }


	private void processMyGroups(Set<String> assignableGroups, String myGroup, String key){
		if (myGroup.contains(key)) {
		    String groupToAppend;
		    if (myGroup.indexOf('_') != -1) {
		        groupToAppend = myGroup.substring(myGroup.indexOf('_') + 1);
		    } else {
		        groupToAppend = myGroup;
		        
		    }
		    String[] groups = (mapping.get(key));
		                            
		    for (String group : groups) 
		    {
		        extractAssignableGroups(assignableGroups, groupToAppend, group);
		    }
		}
	}


	private void extractAssignableGroups(Set<String> assignableGroups, String groupToAppend,
			String group) {
		
		
		//En Pagonxt ya no existen los traductores de todos los locales. Sólo existe el trnaslator_superdigital y no es translator-es_Superdigital y el translator-en_Superdigital.
		//Por esta razón se trata como un autor_ y un editor_

			
			if (groupToAppend.equals("everybody") || groupToAppend.equals("editor")||groupToAppend
					.equals("webmaster")) { 
				//author,editor y admin. Default users Bloomreach
				if (!groupToAppend.equals(group)){
					assignableGroups.add(group);
					log.debug("******añadido a la lista de asignables: {}",group);
				}
			}
			else { 
				// para cuando tienes los grupos de seguridad
				//de un site establecidos "author_", "editor_", etc.
				assignableGroups.add(group + groupToAppend);
				log.info("******añadido a la lista de asignables: {} {}",group,  groupToAppend);
			}
		    
	}
}