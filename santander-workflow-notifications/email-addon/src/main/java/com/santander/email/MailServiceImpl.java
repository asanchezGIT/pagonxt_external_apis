package com.santander.email;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.Authenticator;
import javax.mail.Session;
import java.util.Properties;

public class MailServiceImpl implements MailService {
    private static Logger log = LoggerFactory.getLogger(MailServiceImpl.class);
    private String cmsRoot;
    public static final String MAIL_SESSION_NAME = "mail/Session";

    @Override
    public void sendMail(final String to, final String from, final String subject, final String html, final String text) throws EmailException {
        sendMail(new String[]{to}, from, subject, html, text);
    }

    @Override
    public void sendMail(final String[] to, final String from, final String subject, final String html, final String text) throws EmailException {
        final HtmlEmail email = new HtmlEmail();

        final Session session = getSession();
        if (session == null) {
            throw new EmailException("Unable to send mail; no mail session available");
        }

        email.setMailSession(session);
        email.addTo(to);
        email.setFrom(from);
        email.setSubject(subject);
        email.setHtmlMsg(html);
        email.setTextMsg(text);
        
        if (StringUtils.isEmpty(html) && StringUtils.isEmpty(text)) {
          throw new EmailException("No email template available");
        }

        if (StringUtils.isNotEmpty(html)) {
          email.setHtmlMsg(html);
        } else {
          log.warn("No HTML template available");
       }

        if (StringUtils.isNotEmpty(text)) {
            email.setTextMsg(text);
        }  else {
            log.warn("No text template available");
        }
        
      
        email.send();
    }

    protected Session getSession() {
    	
    	log.info("NUEVO********ENVIO MAIL");
    	      
    	String mailhost = System.getenv("SMTP_HOST");
    	String mailport = System.getenv("SMTP_PORT");
    	String username = System.getenv("SMTP_USER");
    	String password = System.getenv("SMTP_PASSWORD");

        Properties props = new Properties();
        props.put("mail.smtp.host", mailhost);
        props.put("mail.smtp.port", mailport);

        if (!"false".equals(username)) {
        	log.info("ENVIO DE EMAIL CON USUARIO/PASSWORD");
            props.put("mail.smtp.auth", "true");
            Authenticator authenticator = new DefaultAuthenticator(username, password);
            return Session.getInstance(props, authenticator);
        } else {
        	log.info("ENVIO DE EMAIL SIN  USUARIO/PASSWORD");
            props.put("mail.smtp.auth", "false");
            return Session.getInstance(props);
        }
    }

    public String getCmsRoot() {
        return cmsRoot;
    }

    @Override
    public void setCmsRoot(final String cmsRoot) {
        this.cmsRoot = cmsRoot;
    }
}
